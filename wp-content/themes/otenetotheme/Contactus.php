<?php 
	/* Template Name: Contactus */
?>
<?php get_header(); ?>

    <section>
        <div class="ContacArea">
            <div class="container">
                <div class="ContactInfo">
                    <ul>
                        <li>
                            <div class="ContactBox">
                                <figure><img src="<?php echo get_template_directory_uri(); ?>/images/Conatct-icon-1.png"></figure>
                                <h6>Our Address</h6>
                                <p><?php echo $address = get_field( "our_address" );?></p>
                            </div>
                        </li>
                        <li>
                            <div class="ContactBox">
                                <figure><img src="<?php echo get_template_directory_uri(); ?>/images/Conatct-icon-2.png"></figure>
                                <h6>Phone & Email</h6>
                                <p><?php echo $phone = get_field( "phone" );?></p>
                                <p><?php echo $email = get_field( "email" );?></p>
 
                            </div>
                        </li>
                        <li>
                            <div class="ContactBox">
                                <figure><img src="<?php echo get_template_directory_uri(); ?>/images/Conatct-icon-3.png"></figure>
                                <h6>Stay In Touch</h6>
                                <p>Also find us on social media</p>
                                <ol>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                </ol>
                            </div>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="ContactForm">  
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ContactLeft">
                                <h4>What is your question about?</h4>
                                <h5>Please feel free to contact us if you have queries, require more<br>
                                        information or have any other request.</h5>                                

                                <?php echo do_shortcode('[contact-form-7 id="38" title="Contact form 1"]');?>
                            </div>
                        </div> 

                        <div class="col-sm-6">
                            <div class="ContactFormRight">
                                <h4>Have You Any Question About Us?</h4>
                                
                                <?php
                                    while ( have_posts() ) :
                                        the_post();

                                        get_template_part( 'template-parts/content', 'page' );

                                        // If comments are open or we have at least one comment, load up the comment template.
                                        if ( comments_open() || get_comments_number() ) :
                                            comments_template();
                                        endif;

                                    endwhile; // End of the loop.
                                ?>
                                

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="PlaceTags">
            <div class="container">
                <figure><img src="<?php echo get_template_directory_uri(); ?>/images/Logo.png"></figure>
                <h4>The place for small business.</h4>
            </div>
        </div>

        <div class="LearnArea">
            <div class="container">
                <ul>
                    <li>
                        <h4><img src="<?php echo get_template_directory_uri(); ?>/images/Icon-1.png"> Learn</h4>
                        <p><?php echo $learn = get_field( "learn" );?></p>
                    </li>
                    <li>
                        <h4><img src="<?php echo get_template_directory_uri(); ?>/images/Icon-2.png"> Grow</h4>
                        <p><?php echo $grow = get_field( "grow" );?></p>
                    </li>
                    <li>
                        <h4><img src="<?php echo get_template_directory_uri(); ?>/images/Icon-3.png"> Promote</h4>
                        <p><?php echo $promote = get_field( "promote" );?></p>
                    </li>
                </ul>

                <div class="clear"></div>
            </div>
        </div>

    </section>

<?php get_footer(); ?>