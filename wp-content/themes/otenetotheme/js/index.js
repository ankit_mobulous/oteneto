$('#Company').owlCarousel({

    loop:true,
    margin:10,
    center:true,
    autoplay:true,
    autoplayTimeout:3000,
    smartSpeed: 1000,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:2
        }
    }
})