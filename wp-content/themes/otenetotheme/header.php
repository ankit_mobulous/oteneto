<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package otenetotheme
 */
global $post;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=0">
    <title></title>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,500,600,700,900&display=swap" rel="stylesheet">
    <style type="text/css">
        #post-31 .entry-header{display: none!important;}
        #post-33 .entry-header{display: none!important;}
        #post-25 .entry-header{display: none!important;}
        #post-27 .entry-header{display: none!important;}
        #post-109 .entry-header{display:none!important;}
        .titletexth1 h1{font-size: 4em!important;}
        .parahtext p{font-size: medium;}
        .metatext .entry-meta span{font-size: medium;}
        .single.single-post .FilterArea{padding: 153px 0 120px!important;}
        .search.search-results .FilterArea{padding: 153px 0 120px!important;}
        .singleblogs{margin-top: 5%;}
		#page #page-title-bar ul.insight_core_breadcrumb li.level-1 top a { color:#fff }
    </style>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div class="Header">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-8">
                    <ul>
                        <li><a href="tel:0704 000 9005"><i class="fa fa-phone"></i> 0704 000 9005</a></li>
                        <li><a href="mailto:contact@oteneto.com"><i class="fa fa-envelope"></i> contact@oteneto.com</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-xs-4">
                    <ol>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <header>     

        <div class="MenuSection">
            <div class="Menu">
                <span><a href="<?php echo site_url();?>"> <i class="fa fa-home" aria-hidden="true"></i> </a></span>
                <?php
                        if ( is_user_logged_in() ) {
                ?>
                            
                <?php
                            wp_nav_menu( array(
                                'theme_location' => 'afterlogin',
                                'menu_id' => 'secondary-menu',
                                'menu_class' => 'nav navbar-nav navbar-right',
                            ));
                        } else {
                            wp_nav_menu( array(
                                'theme_location' => 'menu-1',
                                'menu_id' => 'primary-menu',
                                'menu_class' => 'nav navbar-nav navbar-right',
                            ));
                        }
                    ?>
                    <div class="clear"></div>
            </div>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php the_custom_logo(); ?></a>
        </div>  

        <!-- <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Menu" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php the_custom_logo(); ?></a>
                </div>

                <div class="collapse navbar-collapse" id="Menu">
                    <?php
                        if ( is_user_logged_in() ) {
                            wp_nav_menu( array(
                                'theme_location' => 'afterlogin',
                                'menu_id' => 'secondary-menu',
                                'menu_class' => 'nav navbar-nav navbar-right',
                            ));
                        } else {
                            wp_nav_menu( array(
                                'theme_location' => 'menu-1',
                                'menu_id' => 'primary-menu',
                                'menu_class' => 'nav navbar-nav navbar-right',
                            ));
                        }
                    ?>
                </div>
            </div>
        </nav> -->

    </header>
<?php
if ( is_front_page() || is_home() ) {
?>
    <section>
        <div class="FilterArea">

            <div class="Search" style="margin-bottom: 30px;">
                <?php echo $banner = get_field( "banner" );?>
            </div>
            <?php echo do_shortcode('[slider_search]');?>            
        </div>
    </section>


    <section>
        <div class="FilterCategory">

            <ul style="position: relative;">
                <h3 style="position: absolute; top: -50px; width: 100%; color: #fff; margin: 0; font-weight: 500">Browse Our Top Categories </h3>
                <li>
                    <a href="<?php echo site_url();?>/directories/?property-category=restaurants">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/fork.png">
                        <p>Restaurants</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url();?>/directories/?property-category=automotive">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/car.png">
                        <p>Automotive</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url();?>/directories/?property-category=doctors">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/doct.png">
                        <p>Doctors</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url();?>/directories/?property-category=shopping">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/shoping.png">
                        <p>Shopping</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url();?>/directories/?property-category=advertise">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/advert.png">
                        <p>Advertise</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url();?>/directories/?property-category=partnership">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/partn.png">
                        <p>Partnership</p>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url();?>/directories">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/more.png">
                        <p>More</p>
                    </a>
                </li>

                </li>
            </ul>
        </div>
    </section>

<?php
} elseif($post->ID == 15 || $post->ID == 23) {
?>

<?php
} else {
?>
    <section>
        <div class="FilterArea FilterAreaone">
            <?php
                if($post->ID == 25) {
                    echo "<h3> About Us </h3>";        

                } else if($post->ID == 13) {
                    echo "<h3> Blogs </h3>";        
                
                } else if($post->ID == 27) {
                    echo "<h3> Contact Us </h3>";

                } else if($post->ID == 29) {
                    echo "<h3> Find a Business by category </h3>";

                } else if($post->ID == 31) {
                    echo "<h3> Terms of use </h3>";        
                
                } else if($post->ID == 33) {
                    echo "<h3> Privacy policy </h3>";
                    
                } else if($post->ID == 104) {
                    echo "<h3> Choose your plan </h3>";        
                }
            ?>
        </div>
    </section>
<?php
}
?>