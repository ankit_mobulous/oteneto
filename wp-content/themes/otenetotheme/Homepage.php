<?php 
	/* Template Name: Homepage */
?>
<?php get_header(); ?>

    <section>
        <div class="PromotArea">
            <div class="container-fluid">


                <div class="FeatureArea">

                    <h1 class="Title">Featured Businesses</h1>

                  <div class="row">

                    <?php
                    global $post,$wpdb;
                    $directory_url=get_option('_iv_directory_url'); 
                        $custom_query_args = array(
                          'post_type'  => 'directories',
                          'meta_key'   => '_is_ns_featured_post',
                          'meta_value' => 'yes',
                          );
                        $custom_query = new WP_Query( $custom_query_args );
                    ?>
                    <?php if ( $custom_query->have_posts() ) : ?>
                        <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                        <?php
                                $id1 = get_the_ID();
                                $currentCategory1 = wp_get_object_terms( $id1, $directory_url.'-category');
                                $cat_slug1 = '';
                                $cat_slug1 = $currentCategory1[0]->name;
                                $total_review_point=0;  
                                $one_review_total=0;
                                $two_review_total=0;
                                $three_review_total=0;
                                $four_review_total=0;
                                $five_review_total=0;

                                $post_type='dirpro_review';
                                $sql = "SELECT * FROM $wpdb->posts WHERE post_type ='".$post_type."' and post_author='".$id1."' and post_status='publish' ORDER BY ID DESC";

                                $author_reviews = $wpdb->get_results($sql);

                                $total_reviews=count($author_reviews);

                                if($total_reviews>0){

                                  foreach ( $author_reviews as $review ) {                       
                                     $review_val=(int)get_post_meta($review->ID,'review_value',true);
                                     $total_review_point=$total_review_point+ $review_val;                         
                                     if($review_val=='1'){
                                        $one_review_total=$one_review_total+1;
                                     }
                                     if($review_val=='2'){
                                        $two_review_total=$two_review_total+1;
                                     }
                                     if($review_val=='3'){
                                        $three_review_total=$three_review_total+1;
                                     }
                                     if($review_val=='4'){
                                        $four_review_total=$four_review_total+1;
                                     }
                                     if($review_val=='5'){
                                        $five_review_total=$five_review_total+1;
                                     }
                                  }  
                                }
                                $avg_review=0;
                                if($total_review_point>0) {
                                  $avg_review= $total_review_point/$total_reviews;
                                }
                        ?>
                        
                        <div class="col-sm-3">
                            <a href="<?php echo get_the_permalink($id1);?>">
                            <div class="FeatureBox">
                                <figure>
                                    <span>Featured</span>
                                    <span class="verified"><img src="<?php echo get_template_directory_uri();?>/images/Verfied.png">verified</span>
                                    <?php 
                                       if(has_post_thumbnail($id)){
                                          $fsrc= wp_get_attachment_image_src( get_post_thumbnail_id( $id1 ), 'large' );
                                             if($fsrc[0]!="") {
                                                $fsrc =$fsrc[0];
                                             }
                                    ?>
                                             <img src="<?php  echo $fsrc;?>">
                                    <?php
                                       } else {
                                    ?>
                                             <img src='<?php  echo wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";?>'>
                                    <?php
                                       }
                                    ?>
                                    <ul>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo get_post_meta($id1,'address',true);?> <?php echo get_post_meta($id1,'city',true);?> <?php echo get_post_meta($id1,'country',true);?></li>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i> <?php
                        if(get_post_meta($id,'phone',true)!='' ) {
                    ?>
                    <?php echo '<a style="text-decoration: none;" href="tel:'.get_post_meta($id1,'phone',true).'">'.get_post_meta($id1,'phone',true).'</a>' ;?>
                    <?php
                        }
                    ?> </li>
                                    </ul>
                                </figure> 
                                <figcaption>
                                    <h6><?php echo get_the_title($id1); ?></h6>
                                    <p><?php the_excerpt();?></p>
                                    <ul>
                                        <li>
                                            <span><?php echo $cat_slug1;?></span>
                                        </li>
                                        <li>
                                            <button type="button" class=" <?php echo ($avg_review>0?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                       
                                             <i class="fa fa-star 3x <?php echo ($avg_review>0?'white-star': 'black-star');?>"></i>
                                            </button>
                                            <button type="button" class=" <?php echo ($avg_review>=2?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                      
                                             <i class="fa fa-star 3x <?php echo ($avg_review>=2?'white-star': 'black-star');?>"></i>
                                            </button>
                                            <button type="button" class=" <?php echo ($avg_review>=3?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                      
                                             <i class="fa fa-star 3x <?php echo ($avg_review>=3?'white-star': 'black-star');?>"></i>
                                            </button>
                                            <button type="button" class=" <?php echo ($avg_review>=4?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                      
                                             <i class="fa fa-star 3x <?php echo ($avg_review>=4?'white-star': 'black-star');?>"></i>
                                            </button>                     
                                            <button type="button" class=" <?php echo ($avg_review>=5?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                      
                                             <i class="fa fa-star 3x <?php echo ($avg_review>=5?'white-star': 'black-star');?>"></i>
                                            </button>
                                        </li>
                                    </ul>
                                        <div class="clear"></div>
                                </figcaption>
                            </div>
                            </a>
                        </div>

                         <?php endwhile; ?>
                         <?php endif; ?>
                        <?php
                            // Reset postdata
                            wp_reset_postdata();
                        ?>

                        
                        <a href="<?php echo site_url();?>/directories" class="SeeArrow">
                          <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </a>
                        <!-- <div class="col-sm-12">
                            <a href="<?php echo site_url();?>/directories" class="SeeArrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            <div class="clear"></div>
                        </div> -->

                    </div>
                </div>



            </div>
        </div>
    </section>

    <section>
        <div class="NormalSlider">
            <div id="demos">
                <div class="owl-carousel owl-theme" id="Normal">
                    <?php
                $args1 = array(
                    'post_type'=> 'banner_slider',
                    'order'    => 'DESC'
                    );
                $the_query = new WP_Query( $args1 );
                $bid=1;
                if ( $the_query->have_posts() ) :
                while ( $the_query->have_posts() ) : $the_query->the_post();
                    $bannerid = get_the_ID();
                    $bannerLink = get_field( "link", $bannerid);

              ?>

                <div class="item <?php if($bid == 1){ echo "active";}?>">
                    <a href="<?php echo $bannerLink; ?>">
                        <div style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>');"> </div>
                    </a>
                </div>
                <?php $bid++; ?>
                <?php endwhile; ?>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    

    <section>
        <div class="BrowseArea">
            <h1 class="Title">Popular Cities </h1>

            <div class="container-fluid">
                <div class="row">
                <?php echo do_shortcode('[directorypro_cities cities="Abuja,Lagos"]');?>
                </div>

                <a href="<?php echo site_url();?>/directories" class="SeeArrow">
                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </a>

                <!-- <div class="col-sm-12">
                    <a href="<?php echo site_url();?>/directories" class="SeeArrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    <div class="clear"></div>
                </div> -->
            </div>



        </div>
    </section>

    <section>        
    <?php
      $args = array(
          'post_type' => 'sale_promotions',
          'post_status' => 'publish',
          'p' => '2041',   // id of the post you want to query
      );
      $my_posts = new WP_Query($args);  

      if($my_posts->have_posts()) :
      while ( $my_posts->have_posts() ) : $my_posts->the_post();
          
        if ( has_post_thumbnail() && ! post_password_required() ) : 
            $imgURL = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
        endif;
        
    ?>
        <div class="PromotBane" style="background-image: url(<?php echo $imgURL; ?>);">        
            <h4>Get Noticed On Oteneto.com</h4>
            <button type="button" class="btnbtn-primary" data-toggle="modal" data-target="#promotemodalone1" id="Promotes">
               <?php the_content(); ?>
            </button>

        </div>
    <?php
      endwhile;
      endif;
    ?>
    </section>

    <section>
        <div class="ClientArea">
            <h2>Lastest News</h2>
            <div class="container">
                <div id="demos">
                    <div class="owl-carousel owl-theme" id="Client">
                    <?php 
                    $the_query = new WP_Query( array(
                    'category_name' => 'blogs-and-articles',
                      'posts_per_page' => 4,
                    )); 
                ?>
                <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>    
                        
                        <div class="item">
                            <div class="ClientBox"> 
                                <h3><?php the_title(); ?></h3>
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php the_permalink(); ?>" class="btn-otn">Read More</a>
                            </div>
                        </div>

                <?php endwhile; ?>
                <?php endif; ?>
                    </div>
                </div>

                <!-- <div class="col-sm-12">
                    <a href="<?php //echo site_url();?>/blog" class="SeeArrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    <div class="clear"></div>
                </div> -->

            </div>
        </div>
    </section>


    <section>
        <div class="StartedArea">
            <h1 class="Title">Get Started</h1>
            <p>Add your business to Oteneto and make sure customers always find the right information about your business</p>
            <?php
                if ( is_user_logged_in() ) {
            ?>
                <a href="<?php echo site_url();?>/my-account/">My Account
                    <span>Update and enrich your business</span></a>
            <?php
                } else {
            ?>
                <a href="<?php echo site_url();?>/login/">Existing user
                    <span>Update and enrich your business</span></a>
        
                <a href="<?php echo site_url();?>/registration/">New user
                    <span>Get started and claim your business</span></a>
            <?php
                }
            ?>
        </div>
    </section>
<style type="text/css">
    .bdp-post-carousel-content .bdp-post-image-bg{display: none!important;}
    .PromoteModal{ position: relative; }
    .PromoteModal:before{     content: '';
    background-color: #00000082;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 999;}
    
    .PromoteModal .modal {
    overflow-x: hidden;
    overflow-y: auto;
} 

#promotemodalone1 .modal-header{}

#promotemodalone1 .modal-header h4{
    margin: 0;
    text-transform: capitalize;
    font-weight: 600;
    color: #292929;
    font-size: 20px;
    font-family: Roboto;
    display: inline-block;
}

#promotemodalone1 .modal-header a#CloseModal{
float: right;
    font-size: 25px;
    color: #b1b1b1;
    font-weight: 600;
    line-height: 20px;
    cursor: pointer;
}

#promotemodalone1 .form-control{
    outline: 0;
    height: 40px;
    padding: 6px 15px;
    box-shadow: none;
    border: 1px solid #c1c1c1;
}

#promotemodalone1 textarea.form-control{
  height: auto;
}

#promotemodalone1 input[type="submit"]{
    transition: 0.5s all ease-in-out;
    border: none;
    outline: 0;
    background-color: #26b060;
    color: #fff;
    padding: 11px 40px;
    font-size: 16px;
    text-transform: uppercase;
    border-radius: 5px;
    letter-spacing: 0.6px;
}

</style>

<div id="promotemodalone1" class="modal">
    <div class="modal-dialog">
        <div class="modal-content"> 
            <div class="modal-header">
                <a id="CloseModal" data-dismiss="modal">&times;</a>
                <h4 class="modal-title">Promote Your Business</h4>
            </div> 
            <div class="modal-body">
                <?php echo do_shortcode('[contact-form-7 id="38" title="Contact form 1"]');?>
            </div>  

        </div>
    </div>
</div>
 

<?php get_footer(); ?>
