<?php 
	/* Template Name: Signin */
?>
<?php get_header('login'); ?>

<style type="text/css">
    .FormArea{
        min-height: 100vh;
        background-color: #272727;
        padding: 50px 0 50px 0;
    }


    .FormArea .custom-logo-link{
    width: 160px;
    margin: 0 auto 30px;
    display: block;
}

.SigninForm #login-2 .content form h3 {
    padding: 0;
    margin: 0 0 20px 0;
}

.SigninForm #login-2 .content form h3:after{ content: none; }

.SigninForm #login-2 .content form .form-group .form-control {
    height: 40px; 
    border-radius: 0;
}

.SigninForm #login-2 .content form .form-actions .col-md-6 button{
    border-radius:  0px
}
#login-2 .uppercase {
    text-transform: uppercase;
    border-radius: 0;
}

</style>

	<section>


        <div class="FormArea"> <!-- style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/bannerhome.jpg')" -->
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php the_custom_logo(); ?></a>
            <!-- <h3>Let's get started now</h3>
            <h4>Generate Lorem Ipsum placeholder text for use in your graphic, print and web layouts, and discover <br> 
                plugins for your favorite writing, design and blogging tools.</h4> -->

            <div class="SigninForm">
                <?php echo do_shortcode('[iv_directories_login ]'); ?>
                <!--<h5>Sign In</h5>
                <form> 
                        <div class="form-group">
                            <label>Usrname<sup>*</sup></label>
                            <input type="text" name="" class="form-control" placeholder="Create your @username">
                        </div>
                        <div class="form-group">
                            <label>password<sup>*</sup></label>
                            <input type="text" name="" class="form-control" placeholder="Enter your password">
                        </div> 

                        <div class="CheckBox">
                            <button>Sign in</button>
                            <a href="#">Forgot your password?</a>
                            <label class="Checks">Remember me
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                </form>-->
                <?php //echo do_shortcode('[iv_directories_price_table ]'); ?>
                <form>
                    <div class="SignSocail">
                        <h5><span>or</span></h5> 
                        <p>Already have an account? <a href="<?php echo site_url();?>/registration/">Sign Up</a></p>
                    </div>
                </form>
            </div>
        </div>
    </section>

<?php get_footer('login'); ?>

