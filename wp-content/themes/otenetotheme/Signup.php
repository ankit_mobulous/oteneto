<?php 
	/* Template Name: Signup */
?>
<?php get_header('login'); ?>

<style type="text/css">

    .Fixed{ display: none; }
    .FormArea{ 
        background-color: #272727;
        padding: 50px 0 50px 0;
    }


    .FormArea .custom-logo-link{
    width: 160px;
    margin: 0 auto 30px;
    display: block;
}

.SignupForm .bootstrap-wrapper div.sign-up-wizard #iv_directories_registration h2 {
    padding: 0;
    margin: 0 0 20px 0;
    font-size: 21px;
}

.SignupForm .bootstrap-wrapper div.sign-up-wizard #iv_directories_registration h2:after{ content: none; }

.SignupForm .bootstrap-wrapper div.sign-up-wizard #selected-column-1 .form-group {
    margin: 0 0 15px !important;
}

.FormArea .SignupForm form .form-group .form-control {
    height: 40px; 
    border-radius: 0;
}

select#package_sel {
    width: 75%;
    height: 35px; 
    border-radius: 0; 
}

#submit_iv_directories_payment{
width: 60%;
    border-radius: 0;
    margin: 20px 0 10px 0;
}

</style>

	<section>
        <div class="FormArea"> <!-- style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/bannerhome.jpg')" -->


            <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php the_custom_logo(); ?></a>


            <!-- <h3>Let's get started now</h3>
            <h4>Generate Lorem Ipsum placeholder text for use in your graphic, print and web layouts, and discover <br> 
                plugins for your favorite writing, design and blogging tools.</h4> -->

            <div class="SignupForm">
                <?php echo do_shortcode('[iv_directories_form_wizard ]'); ?>

                <!--<h5>Sign up</h5>
                <form>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>First Name<sup>*</sup></label>
                            <input type="text" name="" class="form-control" placeholder="First Name">
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Last Name<sup>*</sup></label>
                            <input type="text" name="" class="form-control" placeholder="Last Name">
                        </div>
                        <div class="form-group col-sm-6">
                            <label>E-mail<sup>*</sup></label>
                            <input type="text" name="" class="form-control" placeholder="info@mobulous.com">
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Usrname<sup>*</sup></label>
                            <input type="text" name="" class="form-control" placeholder="Create your @username">
                        </div>
                        <div class="form-group col-sm-6">
                            <label>password<sup>*</sup></label>
                            <input type="text" name="" class="form-control" placeholder="Enter your password">
                        </div>
                        <div class="form-group col-sm-6">
                            <label>confirm password<sup>*</sup></label>
                            <input type="text" name="" class="form-control" placeholder="Confirm password">
                        </div>

                        <div class="col-sm-12">
                            <div class="Categories">
                                <label class="Radio">Sign in as user
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="Radio">Service provider
                                    <input type="radio" name="radio" checked>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <button>create account</button>
                        </div>

                    </div>
                </form>-->
                
                <form>
                    <div class="SignSocail">
                        <h5><span>or</span></h5>
                        <!--<h6>Sign up with your social network</h6>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                        </ul>-->
                        <p>Already have an account? <a href="<?php echo site_url();?>/login/">Sign In</a></p>
                    </div>

                </form>
            </div>
        </div>
    </section>
<script type="text/javascript">
    $(document).ready(function(){
        $(".ihs-country-code").attr("readonly", false);
        $(".ihs-country-code").attr("placeholder", "+234");
    });
</script>
<?php get_footer('login'); ?>