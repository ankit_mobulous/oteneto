<?php 
	/* Template Name: Aboutus */
?>
<?php get_header(); ?>

    <section>
        <div class="WhatdoArea">
            <div class="container">
                <!--<h2>What we do</h2>-->

                <?php
                    while ( have_posts() ) :
                        the_post();

                        get_template_part( 'template-parts/content', 'page' );

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                ?>
            </div>
        </div>
    </section>

    <section>
        <div class="ServiceArea">
            <div class="container">

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#Service1">
                            <div class="Service">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Service-1.png">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#Service2">
                            <div class="Service">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Service-2.png">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#Service3">
                            <div class="Service">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Service-3.png">
                            </div>
                        </a>
                    </li>
                    <!-- <li>
                        <a data-toggle="tab" href="#Service4">
                            <div class="Service">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Service-4.png">
                            </div>
                        </a>
                    </li> -->
                </ul>

                <div  class="tab-content">
                    <div id="Service1" class="tab-pane fade in active">
                        <h3>Our Services</h3>
                        <p> <?php echo $value = get_field( "service" );?> </p>
                    </div>

                    <div id="Service2" class="tab-pane fade">
                        <h3>Subscription Plans</h3>
                        <p> <?php echo $value = get_field( "subscription_plan" );?> </p>
                    </div>

                    <div id="Service3" class="tab-pane fade">
                        <h3>Our Mission</h3>
                        <p> <?php echo $value = get_field( "our_vision" );?> </p>
                    </div> 
                        
                    </div>
                </div>

                <!-- <ul>
                    <li>
                        <div class="Box">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/Service-1.png">
                            <a href="#">Explore</a>
                        </div>
                    </li>
                    <li>
                        <div class="Box">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/Service-2.png">
                            <a href="#">Explore</a>
                        </div>
                    </li>
                    <li>
                        <div class="Box">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/Service-3.png">
                            <a href="#">Explore</a>
                        </div>
                    </li>
                    <div class="clear"></div>
                </ul> -->

                
            </div>
        </div>
    </section>

    <section>
        <div class="PromoteArea">
            <p>
                <?php echo $value = get_field( "promote" );?>
            </p>
            <a href="#">promote your business</a>
        </div>
    </section>

    <section>
        <div class="TalkingArea">

            <h3>People Talking About Company</h3>

            <div class="owl-carousel" id="Company">
            <?php
                $talking = get_posts([
                  'post_type' => 'people_talking',
                  'post_status' => 'publish',
                  'numberposts' => -1,
                  'order'    => 'DESC'
                ]);
                // echo  "<PRE>";
                // var_dump($talking);
                foreach($talking as $talk) {
            ?>
                <div class="item">
                    <div class="TalkingBox">
                        <aside>
                            <figure><?php echo get_the_post_thumbnail($talk->ID); ?></figure>
                            <h5><?php echo $talk->post_title; ?></h5>
                            <h6><?php echo $profile = get_field( "profile", $talk->ID);?></h6>
                        </aside>
                        <figcaption>
                            <p><?php echo $talk->post_content; ?></p>
                        </figcaption>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php
                }
            ?> 

            </div>
        </div>
    </section>

    <section>
        <div class="PartnerArea">
            <h3>Our Partner's</h3>
            <ul>
            <?php
                $partners = get_posts([
                  'post_type' => 'our_partner',
                  'post_status' => 'publish',
                  'numberposts' => -1,
                  'order'    => 'DESC'
                ]);
                foreach($partners as $partner) {
            ?>
                <li><?php echo get_the_post_thumbnail($partner->ID); ?></li>
            <?php
                }
            ?>
                <div class="clear"></div>
            </ul>
        </div>
    </section>

<?php get_footer(); ?>