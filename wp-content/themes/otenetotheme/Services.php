<?php 
	/* Template Name: Services */
?>
<?php get_header(); ?>

<section>
    <div class="BusinessArea">
        <div class="container-fluid">
            <div class="BusinessHead">
                <?php
                    while ( have_posts() ) :
                        the_post();

                        get_template_part( 'template-parts/content', 'page' );

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                ?>
                

                <!--<form>
                    <input type="" name="" placeholder="Search">
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>-->
                <div class="clear"></div>
            </div>

            <div class="Businessbody">
                
                <ul class="Pagination">
                    <li class="active"><a data-toggle="tab" href="#see">see All</a></li>
                    <li><a data-toggle="tab" href="#a">a</a></li>
                    <li><a data-toggle="tab" href="#b">b</a></li>
                    <li><a data-toggle="tab" href="#c">c</a></li>
                    <li><a data-toggle="tab" href="#d">d</a></li>
                    <li><a data-toggle="tab" href="#e">e</a></li>
                    <li><a data-toggle="tab" href="#f">f</a></li>
                    <li><a data-toggle="tab" href="#g">g</a></li>
                    <li><a data-toggle="tab" href="#h">h</a></li>
                    <li><a data-toggle="tab" href="#i">i</a></li>
                    <li><a data-toggle="tab" href="#j">j</a></li>
                    <li><a data-toggle="tab" href="#k">k</a></li>
                    <li><a data-toggle="tab" href="#l">l</a></li>
                    <li><a data-toggle="tab" href="#m">m</a></li>
                    <li><a data-toggle="tab" href="#n">n</a></li>
                    <li><a data-toggle="tab" href="#o">o</a></li>
                    <li><a data-toggle="tab" href="#p">p</a></li>
                    <li><a data-toggle="tab" href="#q">q</a></li>
                    <li><a data-toggle="tab" href="#r">r</a></li>
                    <li><a data-toggle="tab" href="#s">s</a></li>
                    <li><a data-toggle="tab" href="#t">t</a></li>
                    <li><a data-toggle="tab" href="#u">u</a></li>
                    <li><a data-toggle="tab" href="#v">v</a></li>
                    <li><a data-toggle="tab" href="#w">w</a></li>
                    <li><a data-toggle="tab" href="#x">x</a></li>
                    <li><a data-toggle="tab" href="#y">y</a></li>
                    <li><a data-toggle="tab" href="#z">z</a></li> 
                </ul>

                <div class="Services">

                    <div class="tab-content">
                        <div id="see" class="tab-pane fade in active">
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'child_of'            => 0,
                                    'parent'              => 0,
                                    'current_category'    => 0,
                                    'depth'               => 0,
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                            ?>
                                        <h4><?php echo $term->name;?> :</h4>
                                        <ul>
                                        <?php
                                            $taxonomies = array( 
                                                $directory_url.'-category',
                                            );
                                            $args = array(
                                                'parent' => $term->term_id,
                                            );
                                            $subcategories = get_terms($taxonomies, $args);

                                            foreach($subcategories as $subcategory) { 

                                        ?>
                                                <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $subcategory->name;?> <?php echo $subcategory->count;?></a></li>
                                        <?php
                                            }
                                        ?>
                                            
                                        </ul>

                            <?php
                                    }
                                }
                            ?>
                        </div>

                        <div id="a" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "a" || $checkalpha == "A") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>   
                        </div>

                        <div id="b" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "b" || $checkalpha == "B") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>    
                        </div>

                        <div id="c" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "c" || $checkalpha == "C") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>    
                        </div>

                        <div id="d" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "d" || $checkalpha == "D") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>    
                        </div>

                        <div id="e" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "e" || $checkalpha == "E") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>    
                        </div>

                        <div id="f" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "f" || $checkalpha == "F") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>    
                        </div>

                        <div id="g" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "g" || $checkalpha == "G") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>    
                        </div>

                        <div id="h" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "h" || $checkalpha == "H") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>    
                        </div>

                        <div id="i" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "i" || $checkalpha == "I") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="j" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "j" || $checkalpha == "J") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="k" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "k" || $checkalpha == "K") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="l" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "l" || $checkalpha == "L") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="m" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "m" || $checkalpha == "M") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="n" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "n" || $checkalpha == "N") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="o" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "o" || $checkalpha == "O") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="p" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "p" || $checkalpha == "P") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="q" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "q" || $checkalpha == "Q") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="r" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "r" || $checkalpha == "R") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="s" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "s" || $checkalpha == "S") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="t" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "t" || $checkalpha == "T") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="u" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "u" || $checkalpha == "U") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="v" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "v" || $checkalpha == "V") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="w" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "w" || $checkalpha == "W") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="x" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "x" || $checkalpha == "X") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="y" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "y" || $checkalpha == "Y") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                        <div id="z" class="tab-pane fade">
                            <ul>
                            <?php
                                $directory_url=get_option('_iv_directory_url');
                                if($directory_url==""){$directory_url='directories';}

                                $argscat = array(
                                    'type'                     => $directory_url,
                                    'orderby'                  => 'name',
                                    'order'                    => 'ASC',
                                    'hide_empty'               => true,
                                    'hierarchical'             => 1,
                                    'exclude'                  => '',
                                    'include'                  => '',
                                    'number'                   => '',
                                    'taxonomy'                 => $directory_url.'-category',
                                    'pad_counts'               => false
                                );

                                $categories = get_categories( $argscat );

                                if($categories && !is_wp_error( $categories )) {
                                    
                                    foreach ( $categories as $term ) {
                                        $checkalpha = substr($term->name, 0, 1);
                                        
                                        if($checkalpha == "z" || $checkalpha == "Z") {
                            ?>
                                            <li><a href="<?php echo site_url();?>/directories/?property-category=<?php echo $subcategory->slug;?>"><?php echo $term->name;?> <?php echo $term->count;?></a></li>

                            <?php
                                        }
                                    }
                                }
                            ?>
                            </ul>
                        </div>

                    </div>

                </div>

            </div>


        </div>
    </div>
</section>
<style type="text/css">
    .entry-title{display: none!important;}
</style>
<?php get_footer(); ?>