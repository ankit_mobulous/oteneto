<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package otenetotheme
 */

?>

    <footer id="Footer">
        <div>
            <!--<img src="<?php //echo get_template_directory_uri(); ?>/images/Footer-Icon.png">-->
            <ul>
                <li><a href="<?php echo site_url();?>/about-us/">About us</a></li>
                <li><a href="<?php echo site_url();?>/contact-us/">Contact us</a></li>
                <li><a href="<?php echo site_url();?>/services/">Services</a></li>
                <li><a href="<?php echo site_url();?>/blog/">Blogs</a></li>
                <li><a href="<?php echo site_url();?>/terms-of-use/">Terms of use</a></li>
                <li><a href="<?php echo site_url();?>/privacy-policy/">Privacy policy</a></li>
            </ul>
            <?php //dynamic_sidebar('footer-3') ?>
            <p>oteneto © First 9japages. 2019 - All rights reserved.</p>
        </div>
    </footer>

    


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/index.js"></script>

    <script type="text/javascript">
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 150) {
                $("header").addClass("Fixed");
            } else {
                $("header").removeClass("Fixed");
            }
        });
    </script>

    <script type="text/javascript"> 
        $(document).ready(function(){
            $("#Promotes").click(function(){
                $("#promotemodalone1").addClass("show");
                $("#promotemodalone1").addClass("in"); 
                $("body").addClass("PromoteModal"); 
            });

            $("#CloseModal").click(function(){
                $("#promotemodalone1").removeClass("show");
                $("#promotemodalone1").removeClass("in"); 
                $("body").removeClass("PromoteModal"); 
            });
        }); 
        
    </script>

    <script type="text/javascript">
        $('#myCarousel').carousel({
            interval: 3000,
            cycle: true
        }); 
    </script>

    <script type="text/javascript">
        $('#Client').owlCarousel({
            loop:true,
            margin:0,
            center:true,
            autoplay:true,
            autoplayTimeout:6000,
            smartSpeed: 1000,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:3
                }
            }
        });
    </script>

    <script type="text/javascript">
        $('#Normal').owlCarousel({
            loop:true,
            margin:0,
            center:true,
            autoplay:true,
            autoplayTimeout:6000,
            smartSpeed: 1000,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>

    
 
 

<?php wp_footer(); ?>

</body>

</html>