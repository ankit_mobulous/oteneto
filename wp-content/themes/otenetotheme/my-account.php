<?php 
ob_start();
error_reporting(0);
    /* Template Name: my-account */
?>
<?php get_header(); ?>
<?php
	if ( is_user_logged_in() ) {
	} else {
		header('Location: http://mobulous.co.in/Oteneto/sign-in/');
		exit();
	}
?>
    <section>
        <div class="WhatdoArea">
            <div class="container">
                    <?php echo do_shortcode('[iv_directories_profile_template]'); ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>