<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package otenetotheme
 */

get_header();
?>


	<section id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<?php if ( have_posts() ) : ?>
					<div class="row">
						<div class="col-md-12">
								<h1 class="page-title">
									<?php
									/* translators: %s: search query. */
									printf( esc_html__( 'Search Results for: %s', 'otenetotheme' ), '<span>' . get_search_query() . '</span>' );
									?>
								</h1>
						</div>
					</div>

					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
					?>
						<div class="row">
							<div class="col-md-12">
								<?php get_template_part( 'template-parts/content', 'search' ); ?>
							</div>
						</div>
					<?php
					endwhile;
					?>
						<div class="row">
							<div class="col-md-12">
								<?php the_posts_navigation(); ?>
							</div>
						</div>
					<?php

				else :
				?>
				<div class="row">
					<div class="col-md-12">
						<?php get_template_part( 'template-parts/content', 'none' );?>
					</div>
				</div>
				<?php
				endif;
				?>
			</div>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
