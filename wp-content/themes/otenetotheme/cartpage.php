<?php 
    /* Template Name: cartpage */
?>
<?php get_header(); ?>
<style type="text/css">
    .entry-title{display:none;}
</style>
    <section style="min-height: 320px;">
        <div class="WhatdoArea">
            <div class="container">
                    <?php
                        while ( have_posts() ) :
                            the_post();

                            get_template_part( 'template-parts/content', 'page' );

                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) :
                                comments_template();
                            endif;

                        endwhile; // End of the loop.
                    ?>
            </div>
        </div>
    </section>

<?php get_footer(); ?>