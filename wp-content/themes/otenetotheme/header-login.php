<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package otenetotheme
 */
global $post;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <title></title>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,500,600,700,900&display=swap" rel="stylesheet">
    <style type="text/css">
        #post-31 .entry-header{display: none!important;}
        #post-33 .entry-header{display: none!important;}
        #post-25 .entry-header{display: none!important;}
        #post-27 .entry-header{display: none!important;}
        #post-109 .entry-header{display:none!important;}
        .titletexth1 h1{font-size: 4em!important;}
        .parahtext p{font-size: medium;}
        .metatext .entry-meta span{font-size: medium;}
        .single.single-post .FilterArea{padding: 153px 0 120px!important;}
        .search.search-results .FilterArea{padding: 153px 0 120px!important;}
        .singleblogs{margin-top: 5%;}
    </style>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <header>
        
    </header>
