<?php 
    /* Template Name: profile-public */
?>
<?php get_header(); ?>

    <section>
        <div class="WhatdoArea">
            <div class="container">
                 <?php echo do_shortcode('[iv_directories_profile_public]'); ?>
            </div>
        </div>
    </section>

<?php get_footer(); ?>