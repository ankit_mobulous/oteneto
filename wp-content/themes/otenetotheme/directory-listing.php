<?php 
    /* Template Name: directory-listing */
?>
<?php get_header(); ?>

    <section>
        <div class="WhatdoArea">
            <div class="container">
                 <?php echo do_shortcode('[listing_layout_style_1]'); ?>
            </div>
        </div>
    </section>

<?php get_footer(); ?>