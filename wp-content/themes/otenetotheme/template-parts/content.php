<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package otenetotheme
 */

?>

    <section class="singleblogs">
    	<div class="Blog-Details">
    		<div class="container">
    		
	    		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	    			<div class="row">
						<div class="col-md-12">
							<center>
								<?php otenetotheme_post_thumbnail(); ?>
							</center>
						</div>
					</div>

	    			<div class="row">
		    			<div class="col-md-12 titletexth1 text-center">
			    			<?php
								if ( is_singular() ) :
									the_title( '<h1 class="entry-title">', '</h1>' );
								else :
									the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
								endif;
							?>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 parahtext">
							<p>
							<?php
								the_content( sprintf(
									wp_kses(
										/* translators: %s: Name of current post. Only visible to screen readers */
										__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'otenetotheme' ),
										array(
											'span' => array(
												'class' => array(),
											),
										)
									),
									get_the_title()
								) );
							?>
							</p>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
						<?php
							wp_link_pages( array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'otenetotheme' ),
									'after'  => '</div>',
								) );
						?>
						</div>
					</div>

	    		</article>
    		</div>
    	</div>
    </section>

