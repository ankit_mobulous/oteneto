<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package otenetotheme
 */

?>

<section class="singleblogs">
    <div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="row">
					<div class="col-md-12">
						<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
					</div>
				</div>

				<?php if ( 'post' === get_post_type() ) : ?>
				<div class="row">
					<div class="col-md-12 metatext">
						<div class="entry-meta">
							<?php
							otenetotheme_posted_on();
							otenetotheme_posted_by();
							?>
						</div><!-- .entry-meta -->
					</div>
				</div>

				<?php endif; ?>
			
			<div class="row">
				<div class="col-md-6">
					<?php otenetotheme_post_thumbnail(); ?>
				</div>

				<div class="col-md-6 parahtext">
					<?php the_excerpt(); ?>
				</div>
			</div>

		</article><!-- #post-<?php the_ID(); ?> -->
	</div>
</section>
