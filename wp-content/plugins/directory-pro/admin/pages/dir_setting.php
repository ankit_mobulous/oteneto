<style>
.bs-callout {
    margin: 20px 0;
    padding: 15px 30px 15px 15px;
    border-left: 5px solid #eee;
}
.bs-callout-info {
    background-color: #E4F1FE;
    border-color: #22A7F0;
}
.html-active .switch-html, .tmce-active .switch-tmce {
	height: 28px!important;
	}
	.wp-switch-editor {
		height: 28px!important;
	}
</style>	
	

		<h3  class=""><?php _e('Directory Setting','ivdirectories');  ?><small></small>	
		</h3>
	
		<br/>
		<div id="update_message"> </div>		 
					
			<form class="form-horizontal" role="form"  name='directory_settings' id='directory_settings'>											
					<?php
				
					$new_badge_day=get_option('_iv_new_badge_day');
					//$bid_start_amount=get_option('_bid_start_amount');
					$dir_approve_publish =get_option('_dir_approve_publish');
					$dir_archive=get_option('_dir_archive_page');	
					if($dir_approve_publish==""){$dir_approve_publish='no';}	
					
					$dir_claim_show=get_option('_dir_claim_show');	
					if($dir_claim_show==""){$dir_claim_show='yes';}
					
					$search_button_show=get_option('_search_button_show');	
					if($search_button_show==""){$search_button_show='yes';}
					
					$dir_searchbar_show=get_option('_dir_searchbar_show');	
					if($dir_searchbar_show==""){$dir_searchbar_show='no';}
					
					$dir_map_show=get_option('_dir_map_show');	
					if($dir_map_show==""){$dir_map_show='no';}
					
					$dir_social_show=get_option('_dir_social_show');	
					if($dir_social_show==""){$dir_social_show='yes';}
					
					$dir_tag_show=get_option('_dir_tag_show');	
					if($dir_tag_show==""){$dir_tag_show='yes';}
					
					$dir_contact_show=get_option('_dir_contact_show');	
					if($dir_contact_show==""){$dir_contact_show='yes';}
					
					$dir_load_listing_all=get_option('_dir_load_listing_all');	
					if($dir_load_listing_all==""){$dir_load_listing_all='yes';}
				
					?>
					<h4><?php _e('Archive Page','ivdirectories');  ?> </h4>
					<hr>
					<?php
					$active_filter=get_option('active_filter');	
					if($active_filter==""){$active_filter='category';}
					?>
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Style 4 top Filter','ivdirectories');  ?></label>
					
						<div class="col-md-2">
							<label>												
							<input type="radio" name="active_filter" id="active_filter" value='category' <?php echo ($active_filter=='category' ? 'checked':'' ); ?> ><?php _e('Categories Filter','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="active_filter" id="active_filter" value='tag' <?php echo ($active_filter=='tag' ? 'checked':'' );  ?> > <?php _e('Amenities/Tags Filter','ivdirectories');  ?>
							</label>
						</div>	
					</div>
					<!--
					<?php
					echo $active_sort=get_option('dirp_active_sort');	
					if($active_sort==""){$active_sort='date';}
					?>
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Style 4 default Sort','ivdirectories');  ?></label>
					
						<div class="col-md-2">
							<label>												
							<input type="radio" name="active_sort" id="active_sort" value='date' <?php echo ($active_sort=='date' ? 'checked':'' ); ?> ><?php _e('Date','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-2">	
							<label>											
							<input type="radio"  name="active_sort" id="active_sort" value='title' <?php echo ($active_sort=='title' ? 'checked':'' );  ?> > <?php _e('Title','ivdirectories');  ?>
							</label>
						</div>
						<div class="col-md-2">	
							<label>											
							<input type="radio"  name="active_sort" id="active_sort" value='review' <?php echo ($active_sort=='review' ? 'checked':'' );  ?> > <?php _e('Review','ivdirectories');  ?>
							</label>
						</div>		
						
					</div>
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Listing Page toggle button[Search + Map] ','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="search_button_show" id="search_button_show" value='yes' <?php echo ($search_button_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="search_button_show" id="search_button_show" value='no' <?php echo ($search_button_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide','ivdirectories');  ?>
							</label>
						</div>	
					</div>
					-->
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Listing Page Top Map','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_map_show" id="dir_map_show" value='yes' <?php echo ($dir_map_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show  Top Map','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_map_show" id="dir_map_show" value='no' <?php echo ($dir_map_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Top Map','ivdirectories');  ?>
							</label>
						</div>	
					</div>
					
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Listing Page Search Bar','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_searchbar_show" id="dir_searchbar_show" value='yes' <?php echo ($dir_searchbar_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show  Search Bar','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_searchbar_show" id="dir_searchbar_show" value='no' <?php echo ($dir_searchbar_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Search Bar','ivdirectories');  ?>
							</label>
						</div>	
					</div>
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Map Zoom','ivdirectories');  ?></label>
					<?php
						
						$dir_map_zoom=get_option('_dir_map_zoom');	
						if($dir_map_zoom==""){$dir_map_zoom='7';}	
					?>
						<div class="col-md-2">
							<label>												
							<input  type="input" name="dir_map_zoom" id="dir_map_zoom" value='<?php echo $dir_map_zoom; ?>' >
							</label>	
						</div>
						<div class="col-md-2">
							<label>												
							<?php _e('20 is more Zoom, 1 is less zoom','ivdirectories');  ?> 
							</label>	
						</div>
							
					</div>
					
					
					<h4><?php _e('Single Page','ivdirectories');  ?> </h4>
					<hr>
					<?php
						
						$single_slider_height=get_option('_single_slider_height');	
						if($single_slider_height==""){$single_slider_height='450px';}	
					?>
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Top Slider Height','ivdirectories');  ?></label>
					
					<div class="col-md-4">
																	
							<input type="text" name="single_slider_height" id="single_slider_height" value='<?php echo $single_slider_height;?>' >
							
						</div>
						
					</div>
					<?php
					$dir_share_show=get_option('_dir_share_show');	
					if($dir_share_show==""){$dir_share_show='yes';}
					
					?>
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Social Share','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_share_show" id="dir_share_show" value='yes' <?php echo ($dir_share_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Social Share','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_share_show" id="dir_share_show" value='no' <?php echo ($dir_share_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Social Share','ivdirectories');  ?>
							</label>
						</div>	
					</div>
					
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Social Profile','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_social_show" id="dir_social_show" value='yes' <?php echo ($dir_social_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Social Profile','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_social_show" id="dir_social_show" value='no' <?php echo ($dir_social_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Social Profile','ivdirectories');  ?>
							</label>
						</div>	
					</div>
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Amenities/Tag','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_tag_show" id="dir_tag_show" value='yes' <?php echo ($dir_tag_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Amenities/Tag','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_tag_show" id="dir_tag_show" value='no' <?php echo ($dir_tag_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Amenities/Tag','ivdirectories');  ?>
							</label>
						</div>	
					</div>
					
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Contact Us','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_contact_show" id="dir_contact_show" value='yes' <?php echo ($dir_contact_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Contact Us','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_contact_show" id="dir_contact_show" value='no' <?php echo ($dir_contact_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Contact Us','ivdirectories');  ?>
							</label>
						</div>	
					</div>

					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Claim','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_claim_show" id="dir_claim_show" value='yes' <?php echo ($dir_claim_show=='yes' ? 'checked':'' ); ?> ><?php _e('Show Claim','ivdirectories');  ?>
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_claim_show" id="dir_claim_show" value='no' <?php echo ($dir_claim_show=='no' ? 'checked':'' );  ?> > <?php _e('Hide Claim','ivdirectories');  ?>
							</label>
						</div>	
					</div>
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Days #','ivdirectories');  ?></label>
						<div class="col-md-2">						
							<input type="text" class="form-control" name="iv_new_badge_day" id="iv_new_badge_day" value="<?php echo $new_badge_day;?>" placeholder="Enter Days">
							
						</div>
						<div class="col-md-7">
							<img  width="40px" src="<?php echo  wp_iv_directories_URLPATH."/assets/images/newicon-big.png";?>">	
							<?php _e('The new item badge will show for the days','ivdirectories');  ?>
						</div>	
					</div>
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Directory Publish By User','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_approve_publish" id="dir_approve_publish" value='yes' <?php echo ($dir_approve_publish=='yes' ? 'checked':'' ); ?> > Admin Will Approve  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_approve_publish" id="dir_approve_publish" value='no' <?php echo ($dir_approve_publish=='no' ? 'checked':'' );  ?> > User Can Publish
							</label>
						</div>	
					</div>
					<?php
					$dir_single_map_show=get_option('_dir_single_map_show');	
					if($dir_single_map_show==""){$dir_single_map_show='yes';}
					?>
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Detail Listing Map','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_single_map_show" id="dir_single_map_show" value='yes' <?php echo ($dir_single_map_show=='yes' ? 'checked':'' ); ?> > Show  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_single_map_show" id="dir_single_map_show" value='no' <?php echo ($dir_single_map_show=='no' ? 'checked':'' );  ?> > Hide
							</label>
						</div>	
					</div>
					<?php
					$dir_single_review_show=get_option('_dir_single_review_show');	
					if($dir_single_review_show==""){$dir_single_review_show='yes';}
					?>
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Detail Listing Review','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_single_review_show" id="dir_single_review_show" value='yes' <?php echo ($dir_single_review_show=='yes' ? 'checked':'' ); ?> > Show  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_single_review_show" id="dir_single_review_show" value='no' <?php echo ($dir_single_review_show=='no' ? 'checked':'' );  ?> > Hide
							</label>
						</div>	
					</div>
					
					
					
					<?php
					$dir_search_redius=get_option('_dir_search_redius');	
					if($dir_search_redius==""){$dir_search_redius='Km';}	
					?>
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Directory Radius','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_redius" id="dir_search_redius" value='Km' <?php echo ($dir_search_redius=='Km' ? 'checked':'' ); ?> > Km  
							</label>	
						</div>
						<div class="col-md-2">	
							<label>											
							<input type="radio"  name="dir_search_redius" id="dir_search_redius" value='Miles' <?php echo ($dir_search_redius=='Miles' ? 'checked':'' );  ?> > Miles
							</label>
						</div>	
					</div>
					
					
					
					<h4><?php _e('Search Options','ivdirectories');  ?> </h4>
					<hr>
					<?php
					$dir_search_keyword=get_option('_dir_search_keyword');	
					if($dir_search_keyword==""){$dir_search_keyword='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Keyword','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_keyword" id="dir_search_keyword" value='yes' <?php echo ($dir_search_keyword=='yes' ? 'checked':'' ); ?> > Show Keyword  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_keyword" id="dir_search_keyword" value='no' <?php echo ($dir_search_keyword=='no' ? 'checked':'' );  ?> > Hide Keyword  
							</label>
						</div>	
					</div>
					
					<?php
					$dir_search_city=get_option('_dir_search_city');	
					if($dir_search_city==""){$dir_search_city='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('City','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_city" id="dir_search_city" value='yes' <?php echo ($dir_search_city=='yes' ? 'checked':'' ); ?> > Show City  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_city" id="dir_search_city" value='no' <?php echo ($dir_search_city=='no' ? 'checked':'' );  ?> > Hide City   
							</label>
						</div>	
					</div>
					
					<?php
					$dir_search_country=get_option('_dir_search_country');	
					if($dir_search_country==""){$dir_search_country='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Country','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_country" id="dir_search_country" value='yes' <?php echo ($dir_search_country=='yes' ? 'checked':'' ); ?> > Show Country  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_country" id="dir_search_country" value='no' <?php echo ($dir_search_country=='no' ? 'checked':'' );  ?> > Hide Country   
							</label>
						</div>	
					</div>
					
					
				
					<?php
					$dir_search_category=get_option('_dir_search_category');	
					if($dir_search_category==""){$dir_search_category='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Category','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_category" id="dir_search_category" value='yes' <?php echo ($dir_search_category=='yes' ? 'checked':'' ); ?> > Show Category  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_category" id="dir_search_category" value='no' <?php echo ($dir_search_category=='no' ? 'checked':'' );  ?> > Hide Category   
							</label>
						</div>	
					</div>
					
					<?php
					 $dir_search_location=get_option('_dir_search_location');	
					if($dir_search_location==""){$dir_search_location='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Location + Radius','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_location" id="dir_search_location" value='yes' <?php echo ($dir_search_location=='yes' ? 'checked':'' ); ?> > Show Location  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_location" id="dir_search_location" value='no' <?php echo ($dir_search_location=='no' ? 'checked':'' );  ?> > Hide Location   
							</label>
						</div>	
					</div>					
					
					<?php
					$dir_search_zipcode=get_option('_dir_search_zipcode');	
					if($dir_search_zipcode==""){$dir_search_zipcode='yes';}	
					
					?>				
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Zipcode','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_search_zipcode" id="dir_search_zipcode" value='yes' <?php echo ($dir_search_zipcode=='yes' ? 'checked':'' ); ?> > Show Zipcode  
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_search_zipcode" id="dir_search_zipcode" value='no' <?php echo ($dir_search_zipcode=='no' ? 'checked':'' );  ?> > Hide Zipcode   
							</label>
						</div>	
					</div>
					
					
					
					<h4><?php _e('Other Options','ivdirectories');  ?> </h4>
					<hr>
					
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('VIP image','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<div id="current_vip">	
								<?php
								
								if(get_option('vip_image_attachment_id')!=''){
									$vip_img= wp_get_attachment_image_src(get_option('vip_image_attachment_id'));
									if(isset($vip_img[0])){									
										$vip_image='<img style="width:40px;"   src="'.$vip_img[0] .'">';
									}
								}else{
									$vip_image='<img style="width:40px;"   src="'. wp_iv_directories_URLPATH."/assets/images/vipicon.png".'">';
								}
								echo $vip_image;
								?>
								
							</div>	
						</div>
						<div class="col-md-3">	
							<label>											
							<button type="button" onclick="change_vip_image();"  class="btn btn-success btn-xs"><?php _e('Change Image','ivdirectories');  ?></button>
					
							</label>
						</div>	
					</div>										
					<?php
					$dir_tags=get_option('_dir_tags');	
					if($dir_tags==""){$dir_tags='yes';}						
					?>
					<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Tags','ivdirectories');  ?></label>
					
					<div class="col-md-2">
							<label>												
							<input type="radio" name="dir_tags" id="dir_tags" value='yes' <?php echo ($dir_tags=='yes' ? 'checked':'' ); ?> > Directories Tags
							</label>	
						</div>
						<div class="col-md-3">	
							<label>											
							<input type="radio"  name="dir_tags" id="dir_tags" value='no' <?php echo ($dir_tags=='no' ? 'checked':'' );  ?> > Post Tags    
							</label>
						</div>	
					</div>
					
					<?php
					$dir_map_api=get_option('_dir_map_api');	
					if($dir_map_api==""){$dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';}	
					?>
					<div class="form-group">
						<label  class="col-md-3 control-label"> <?php _e('Google Map API Key','medical-directory');  ?></label>
					
							<div class="col-md-5">																		
								<input class="col-md-12" type="text" name="dir_map_api" id="dir_map_api" value='<?php echo $dir_map_api; ?>' >						
								
						</div>
						<div class="col-md-4">
							<label>												
							 <b> <a href="https://developers.google.com/maps/documentation/geocoding/get-api-key">Get your API key here </a></b>
							
							</label>	
						</div>
						
												
					</div>
					
				<div class="form-group">
					<label  class="col-md-3 control-label"> <?php _e('Cron Job URL','ivdirectories');  ?>						 
					
					</label>
					
						<div class="col-md-6">
							<label>												
							 <b> <a href="<?php echo admin_url('admin-ajax.php'); ?>?action=iv_directories_cron_job"><?php echo admin_url('admin-ajax.php'); ?>?action=iv_directories_cron_job </a></b>
							
							</label>	
						</div>
						<div class="col-md-3">
							Cron JOB Detail : Auto Bidding Renew update, Hide Listing( Package setting),Subscription Remainder email.
						</div>		
							
					</div>
					
					
				
					<div class="form-group">
					<label  class="col-md-3 control-label"> </label>
					<div class="col-md-8">
						
						<button type="button" onclick="return  iv_update_dir_setting();" class="btn btn-success">Update</button>
					</div>
				</div>
						
			</form>
								

	
<script>
 function change_vip_image(){	
				var image_gallery_frame;

               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: '<?php _e( 'VIP Image', 'easy-image-gallery' ); ?>',
                    button: {
                        text: '<?php _e( 'VIP Image', 'easy-image-gallery' ); ?>',
                    },
                    multiple: false,
                    displayUserSettings: true,
                });                
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {							
							var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
							var search_params = {
								"action": 	"iv_directories_update_vip_image",
								"attachment_id": attachment.id,								
							};
                             jQuery.ajax({
										url: ajaxurl,
										dataType: "json",
										type: "post",
										data: search_params,
										success: function(response) {   
											if(response=='success'){					
												
												jQuery('#current_vip').html('<img width="40px" src="'+attachment.url+'">');                              
						

											}
											
										}
							});									
                              
						}
					});
                   
                });               
				image_gallery_frame.open(); 
				
	}
function iv_update_dir_setting(){
var search_params={
		"action"  : 	"iv_update_dir_setting",	
		"form_data":	jQuery("#directory_settings").serialize(), 
	};
	jQuery.ajax({					
		url : ajaxurl,					 
		dataType : "json",
		type : "post",
		data : search_params,
		success : function(response){
			jQuery('#update_message').html('<div class="alert alert-info alert-dismissable"><a class="panel-close close" data-dismiss="alert">x</a>'+response.msg +'.</div>');
			
		}
	})

}

	

</script>
