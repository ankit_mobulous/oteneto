<style>
.bs-callout {
    margin: 20px 0;
    padding: 15px 30px 15px 15px;
    border-left: 5px solid #eee;
}
.bs-callout-info {
    background-color: #E4F1FE;
    border-color: #22A7F0;
}

</style>
			<?php
			global $wpdb;
			global $current_user;
			$ii=1;
			$directory_url=get_option('_iv_directory_url');					
			if($directory_url==""){$directory_url='directories';}
			
			?>
			
		
	<div class="row">
		<div class="col-md-6 ">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class=""><?php _e('Demo Import','ivdirectories');?></h3>                    
                </div>
                <div class="panel-body">					  
							<div class="row" style="margin-top:10px">	
							<img class="img-responsive" src="<?php echo  wp_iv_directories_URLPATH; ?>assets/images/demo-inport01.png">
							</div>
							<div class="row" style="margin-top:10px">	
								<img class="img-responsive" src="<?php echo  wp_iv_directories_URLPATH; ?>assets/images/demo-inport02.png">
							</div>
							<div class="row" style="margin-top:10px">	
								<img class="img-responsive" src="<?php echo  wp_iv_directories_URLPATH; ?>assets/images/demo-inport03.png">
							</div>
							<div class="row" style="margin-top:10px">					
								<div class="col-md-12">		
									<br/>
										<a  class="btn btn-info btn-xs" href="<?php echo  wp_iv_directories_URLPATH; ?>assets/demo-data.xml" download >Download Demo XML File</a>
									
								</div>	
								<div class="col-md-12">		
									<h3>
										You can check listing page after importing demo data: 
									<a   class="btn btn-info btn-xs" href="<?php echo get_post_type_archive_link( $directory_url) ; ?>" target="_blank"><?php _e('Listing Archive Page','ivdirectories');?>  </a>
								</h3>
								</div>							
							</div>	
                </div>
            </div>
        </div>
		<div class="col-md-6 ">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 ><?php _e('Some Important shortcode','ivdirectories');?>
					<a class="btn btn-info btn-xs" href="http://help.eplug-ins.com/dirprodoc/#all-shortcode" target="_blank"><?php _e('All Shortcodes','ivdirectories');?>  </a>
					</h3> 
						
                </div>
                <div class="panel-body">
					<div class="tab-content">
							<div class="row">
									<div class="col-md-6">	
									Listing Filter ( you can use any parameter e.g. [listing_filter ] )
									</div>
										<div class="col-md-6">	
										[listing_filter  category="test" dir_city="test" zipcode="10001" background_color="#EFEFEF" post_limit="3"]
									</div>
								</div>	
								<hr/>
								<div class="row">
									<div class="col-md-6">	
									 Slider Search bar(You can use without slider too)
									</div>
										<div class="col-md-6">	
										[slider_search]
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-6">	
									 Listing Carousel
									</div>
										<div class="col-md-6">	
										[listing_carousel  category="test" dir_city="test" zipcode="10001" post_limit="10"]
									</div>
								</div>	
								<hr/>
								<div class="row">
									<div class="col-md-6">	
									All Listings
									</div>
										<div class="col-md-6">	
										[listing_layout_style_4]
									</div>
								</div>	
								
					</div>			
                </div>
            </div>
        </div>
		<div class="col-md-6 ">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3><?php _e('Importing CSV Data ','ivdirectories');?></h3>                    
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						   <div class="row">
								<div class="col-xs-12">	
									You can use the plugin/any CSV importing plugin for importing  data CSV. <strong><a href="https://wordpress.org/plugins/wp-csv/"> CSV Importing Plugin</a></strong>
								</div>
								<div class="col-xs-12">	
								Sample CSV file : <strong><a class="btn btn-info btn-xs" href="<?php echo  wp_iv_directories_URLPATH; ?>assets/csv-sample.csv" download >CSV Sample file</a> </strong>
								</div>						
								
							</div>						
                       
                    </div>
                </div>
            </div>
        </div>
		<div class="col-md-6 ">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3><?php _e('Home Page Content','ivdirectories');?></h3>  
						<small><?php _e('Create a full width page and paste the code','ivdirectories');?> </small>
						<p><a class="btn btn-info btn-xs" href="<?php echo  wp_iv_directories_URLPATH; ?>assets/directory-pro-slider.zip" download ><?php _e('Download & import the top Revolution Slider','ivdirectories');?>  </a> </p>
						
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                       <code>
					    [rev_slider alias="directory-pro-slider"][/rev_slider]  

						&nbsp;
						<h2 style="text-align: center;">Recent Listing</h2>
						&nbsp;
						<p style="text-align: center;">[listing_carousel post_limit="10"]</p>

						<h2 style="text-align: center;">Listing in New York</h2>
						&nbsp;
						<p style="text-align: center;">[listing_filter  post_limit="3"]</p>

						<h2 style="text-align: center;">Find a Listing That Fits Your Comfort</h2>
						<p style="text-align: center;">[directorypro_categories slugs="hotel,food" post_limit="3"]</p>

						<h2 style="text-align: center;">Browse Listings in these Cities</h2>						
						<p style="text-align: center;">[directorypro_cities cities="london,new york,FLORIDA,California"]</p>
												
                       </code>
                    </div>
                </div>
            </div>
        </div>
		
		
	</div>



			
		

