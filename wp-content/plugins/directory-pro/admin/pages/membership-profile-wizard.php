<div class="bootstrap-wrapper">
	<div class="welcome-panel container-fluid">
		
		<div class="row">
			<div class="col-md-12">
				
				<h3 class="page-header" ><?php _e('My Account','ivdirectories');?> <small>  </small> </h3>
				
				
				
			</div>
		</div>
		
		
		<div class="form-group col-md-12 row">
			
			<div class="row ">
				<label for="text" class="col-md-2 control-label"><?php _e('Short Code','ivdirectories');?> </label>
				<div class="col-md-4" >
					[iv_directories_profile_template]
				</div>
				<div class="col-md-4" id="success_message">
				</div>
			</div>
			<div class="row ">
				<label for="text" class="col-md-2 control-label"><?php _e('Php Code','ivdirectories');?> </label>
				<div class="col-md-10" >
							<p>
										&lt;?php
										echo do_shortcode('[iv_directories_profile_template]');
										?&gt;</p>	
				</div>
			</div>
			<div class="row ">
				<label for="text" class="col-md-2 control-label"> <?php _e('My Account Page','ivdirectories');?> </label>
				<div class="col-md-10" >
					<!--
					 get_option('_iv_directories_registration','73');
					-->
					<?php 
					$form_wizard=get_option('_iv_directories_profile_page');
							//echo get_the_title( $form_wizard );  ?>
					<a class="btn btn-info btn-xs " href="<?php echo get_permalink( $form_wizard ); ?>" target="blank"><?php _e('View Page','ivdirectories');?> </a>
				
					
				</div>
			</div>
			<?php
			include('profile-fields.php');
			?>
						
		</div>
		
			
		
	</div>
</div>

