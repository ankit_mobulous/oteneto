(function($, window, document, undefined) {
    'use strict';

    // init cubeportfolio
    $('#js-grid-meet-the-team').cubeportfolio({
        filters: '#js-filters-meet-the-team',
        layoutMode: 'grid',
        defaultFilter: '*',
        animationType: 'quicksand',
        gapHorizontal: 30,
        gapVertical: 30,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 5,
        }, {
            width: 1100,
            cols: 4,
        }, {
            width: 768,
            cols: 4,
        } 
		, {
            width: 480,
            cols: 2,
        }, 
		
		{ width: 375,
            cols: 1,
            options: {
                caption: '',
                gapHorizontal: 30,
                gapVertical: 15,
            }
        }],
        caption: 'fadeIn',        
		displayType: 'lazyLoading',
        displayTypeSpeed: 50,
       
		 plugins: {
            sort: {
                element: '#js-sort-juicy-projects',
             }
        },
    });
})(jQuery, window, document);