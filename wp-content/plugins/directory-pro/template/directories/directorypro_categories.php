<?php

    wp_enqueue_style('iv_property-style-110', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap-4.css');
    wp_enqueue_style('iv_property-style-111', wp_iv_directories_URLPATH . 'admin/files/css/styles.css');
	wp_enqueue_script('iv_property-ar-script-24', wp_iv_directories_URLPATH . 'admin/files/js/bootstrap.min-4.js');
	wp_enqueue_script('iv_property-ar-script-25', wp_iv_directories_URLPATH . 'admin/files/js/popper.min.js');


global $post,$wpdb,$tag;
$directory_url=get_option('_iv_directory_url');					
if($directory_url==""){$directory_url='directories';}
$post_limit='9999';
if(isset($atts['post_limit']) and $atts['post_limit']!="" ){
 $post_limit=$atts['post_limit'];
}

?>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Quicksand:300&display=swap" rel="stylesheet">


<section id="destination" style="background: transparent !important;">
	<section class="bootstrap-wrapper" style="background: transparent !important;">
		<div class="container dynamic-bg">
			<div class="row mt-5 cities-sec-row">

			<?php
						if( isset($_POST['submit'])){
							$selected = $_POST['directories-category'];
						}
							//property
							$taxonomy = $directory_url.'-category';
							$args = array(
								'orderby'           => 'name',
								'order'             => 'ASC',
								'hide_empty'        => true,
								'exclude'           => array(),
								'exclude_tree'      => array(),
								'include'           => array(),
								'number'            => $post_limit,
								'fields'            => 'all',
								'slug'              => '',
								//'parent'            => '0',
								'hierarchical'      => true,
								'child_of'          => 0,
								'childless'         => false,
								'show_count'        => '1',

							);
				$terms = get_terms($taxonomy,$args); // Get all terms of a taxonomy
				if ( $terms && !is_wp_error( $terms ) ) :
						$i=0;
						foreach ( $terms as $term_parent ) {

							if($term_parent->count>0){

								$feature_img_id = get_option('_cate_main_image_'.$term_parent->term_id);
								$feature_img='';
								$feature_image = wp_get_attachment_image_src( $feature_img_id, 'large' );
								if($feature_image[0]!=""){
									$feature_img=$feature_image[0];
									$feature_img_width=$feature_image[1];
									$feature_img_height=$feature_image[2];

								}
								if($feature_img==""){
									$feature_img=wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";
								}
								 $cat_link= get_term_link($term_parent , $directory_url.'-category');
							?>
									<div class="col-md-6 col-lg-4 mb-5">
										<div class="img_overlay_container">
                      <div class="img_overlay rounded "></div>
										<a href="<?php echo $cat_link; ?>">
											<img   src="<?php echo $feature_img;?>" class="rounded w-100 img-fluid cities_img">
										</a>
											<h6 class="cities_title text-center text-white"><?php echo $term_parent->name; ?> <small class="text-white text-break">( <?php echo $term_parent->count; ?> )</small></h6>
										</div>
									</div>

						<?php
							}
						}
				endif;
				?>
			</div>


		</div>
	</section>
</section>
