<?php
	$features = array(
		'relation' => 'AND',
			array(
				'key'     => 'dirpro_featured',
				'value'   => 'featured',
				'compare' => 'LIKE'
			),
		);
	$feature_listing_all['posts_per_page']='-1';
	
	$feature_listing_all['meta_query'] = array(
			$city_mq, $country_mq, $zip_mq,$features,
		);
		
	
$feature_listing = new WP_GeoQuery( $feature_listing_all ); 


 if ( $feature_listing->have_posts() ) : 
	
	while ( $feature_listing->have_posts() ) : $feature_listing->the_post();
				
				
				
				$id = get_the_ID();
				$gallery_ids=get_post_meta($id ,'image_gallery_ids',true);
				$gallery_ids_array = array_filter(explode(",", $gallery_ids));
				
				$dir_data['link']=get_post_permalink();
				$dir_data['title']=$post->post_title; 				
				$dir_data['lat']=get_post_meta($id,'latitude',true);
				$dir_data['lng']=get_post_meta($id,'longitude',true);
				if(get_post_meta($id,'latitude',true)!=''){$ins_lat=get_post_meta($id,'latitude',true);}
				if(get_post_meta($id,'longitude',true)!=''){$ins_lng=get_post_meta($id,'longitude',true);}
				$dir_data['address']=get_post_meta($id,'address',true); 
				$dir_data['image']= '';
				$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'thumbnail' ); 
				if($feature_image[0]!=""){ 					
					$dir_data['image']=  $feature_image[0];
				}
				$dir_data['marker_icon']=wp_iv_directories_URLPATH."/assets/images/map-marker/map-marker.png";				
				$currentCategoryId='';
				$terms =get_the_terms($id, $directory_url."-category");				
				if($terms!=""){
					foreach ($terms as $termid) {  
						if(isset($termid->term_id)){
							 $currentCategoryId= $termid->term_id; 
						}					  
					} 
				}
				$marker = get_option('_cat_map_marker_'.$currentCategoryId,true);
				if($marker!=''){
					$image_attributes = wp_get_attachment_image_src( $marker ); // returns an array
					if( $image_attributes ) {
					
						$dir_data['marker_icon']= $image_attributes[0];
					}							
				}
				if($dir_data['lat']!='' AND $dir_data['lng']!='' ){				
					array_push( $dirs_data, $dir_data );
				}
				
				
							
					$feature_img='';
					if(has_post_thumbnail()){ 
						$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' ); 
						if($feature_image[0]!=""){ 							
							$feature_img =$feature_image[0];
						}					
					}else{
						$feature_img= wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";					
					
					}
					if($active_filter=="tag"){
						$currentCategory=wp_get_object_terms( $id, $directory_url.'_tag');
					}else{
						$currentCategory=wp_get_object_terms( $id, $directory_url.'-category');
					}
					
					$cat_link='';$cat_name='';$cat_slug='';
					if(isset($currentCategory[0]->slug)){										
						$cat_slug = $currentCategory[0]->slug;
						$cat_name = $currentCategory[0]->name;
						$cc=0;
						foreach($currentCategory as $c){		
								if($cc==0){
									$cat_name =$c->name;
									$cat_slug =$c->slug;
								}else{
									$cat_name = $cat_name .', '.$c->name;
									$cat_slug = $cat_slug .' '.$c->slug;
								}															
							$cc++;
						}					
					}
					
					$currentCategory=wp_get_object_terms( $id, $directory_url.'-category');
					$cat_name2='';
					if(isset($currentCategory[0]->slug)){
						$cat_name2 = $currentCategory[0]->name;
						$cc=0;
						foreach($currentCategory as $c){		
								if($cc==0){
									$cat_name2 =$c->name;								
								}else{
									$cat_name2 = $cat_name2 .', '.$c->name;									
								}															
							$cc++;
						}					
					}
					
					
					// VIP image***************	
					$vip_image='';	$have_vip_badge='';
					$post_author_id= $post->post_author;
					$author_package_id=get_user_meta($post_author_id, 'iv_directories_package_id', true); 
					$have_vip_badge= get_post_meta($author_package_id,'iv_directories_package_vip_badge',true);
					$exprie_date= strtotime (get_user_meta($post_author_id, 'iv_directories_exprie_date', true));	
					$current_date=time();
					if($have_vip_badge=='yes'){
						if($exprie_date >= $current_date){ 	
							if(get_option('vip_image_attachment_id')!=""){
									$vip_img= wp_get_attachment_image_src(get_option('vip_image_attachment_id'));
									if(isset($vip_img[0])){									
										$vip_image='<img style="width:30px;"   src="'.$vip_img[0] .'">';
									}							
							}else{
								$vip_image='<img style="width:35px;"   src="'. wp_iv_directories_URLPATH."/assets/images/vipicon.png".'">';
							}
											
						}	
					}								
										
					?>
					

				<div class="item-inside cbp-item  <?php echo esc_attr($cat_slug); ?>">
					<article>						
						<div class="img-sec">							
						  <img src="<?php echo $feature_img;?>" alt="" /> 	
							
						  <a href="<?php echo get_the_permalink($id); ?>" class="over-hover"><?php esc_html_e('VIEW DETAIL', 'ivdirectories' ); ?></a> 
						</div>
						<div class="overlay_content1">
							<?php 
							if(get_post_meta($id,'dirpro_featured',true)=="featured"){ ?>
									<p><?php _e('Featured', 'ivdirectories' ); ?></p>
							<?php	
							}
							?>
						
						</div>
						<div class="overlay_content2">
							<?php echo $vip_image;?>
						</div>
						<div class="des-info">
							<a  class="cbp-l-grid-projects-title" href="<?php echo get_the_permalink($id); ?>"><?php echo$post->post_title;?> </a>							
							<div class="epcategory">						
								<?php echo $cat_name2;?>							
							</div>	
							
							<?php //echo $vip_image;?>
	
						<ul>
								<?php
								if(get_post_meta($id,'city',true)!=""){
								?>
								<li><i class="fa fa-map-marker"></i> <?php echo get_post_meta($id,'city',true);?></li>
							  <?php
								}
							  ?>
							  
							  <?php
								if(get_post_meta($id,'phone',true)!=""){
								?>
								 <li><i class="fa fa-phone"></i> <?php echo get_post_meta($id,'phone',true);?></li>
							  <?php
								}
							  ?>
							   <?php
								if(get_post_meta($id,'contact-email',true)!=""){
								?>
								 <li><i class="fa fa-envelope"></i><?php 
									
									
									$estring = get_post_meta($id,'contact-email',true) ;
									 if (strlen($estring) >= 20) {
											echo substr($estring, 0, 18). ".. " . substr($estring, -5);
										}
										else {
											echo $estring;
										}									 
									 ?></li>
							  <?php
								}
							  ?>
						 
						</ul>
						 <?php
							$dir_single_review_show=get_option('_dir_single_review_show');	
							if($dir_single_review_show==""){$dir_single_review_show='yes';}
							if($dir_single_review_show=='yes'){
						
									$total_reviews_point = $wpdb->get_col("SELECT SUM(pm.meta_value) FROM {$wpdb->postmeta} pm
									 INNER JOIN {$wpdb->posts} p ON p.ID = pm.post_id
									 WHERE pm.meta_key = 'review_value' 
									 AND p.post_status = 'publish' 
									 AND p.post_type = 'dirpro_review' AND p.post_author = '".$id."'");
									 
									 
									$argsreviw = array( 'post_type' => 'dirpro_review','author'=>$id,'post_status'=>'publish' );
									$ratings = new WP_Query( $argsreviw );

									$total_review_post = $ratings->post_count;							
									
									$avg_review=0;
									if(isset($total_reviews_point[0])){
										$avg_review= (int)$total_reviews_point[0]/(int)$total_review_post;
									}		
								?>
								<div>
									<i class="fa fa-star <?php echo ($avg_review>0?'black-star': 'white-star ');?>"></i>
									<i class="fa fa-star <?php echo ($avg_review>=2?'black-star': 'white-star');?>"></i>
									<i class="fa fa-star <?php echo ($avg_review>=3?'black-star': 'white-star');?>"></i>
									<i class="fa fa-star <?php echo ($avg_review>=4?'black-star': 'white-star');?>"></i>
									<i class="fa fa-star <?php echo ($avg_review>=5?'black-star': 'white-star');?>"></i>
								 </div>
								 <div class="cbp-l-grid-projects-review" style="display:none"><?php echo $avg_review;?></div>
								<?php
							}
							
						 ?>
						<div class="cbp-l-grid-projects-date" style="display:none"><?php echo strtotime($post->post_date);?></div>						
					  </div>
					</article>
				</div>
      
      
      <?php
		$i++;
	endwhile; 	
 endif; ?>