<?php

wp_enqueue_style('iv_property-style-110', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap-4.css');

wp_enqueue_style('iv_property-style-111', wp_iv_directories_URLPATH . 'admin/files/css/styles.css');

wp_enqueue_script('iv_property-ar-script-24', wp_iv_directories_URLPATH . 'admin/files/js/bootstrap.min-4.js');

wp_enqueue_script('iv_property-ar-script-25', wp_iv_directories_URLPATH . 'admin/files/js/popper.min.js');



wp_enqueue_style('iv_directories-css-queryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css');

wp_enqueue_script('iv_directories-jqueryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');

global $post,$wpdb;

$directory_url=get_option('_iv_directory_url');

if($directory_url==""){$directory_url='directories';}

$main_class = new wp_iv_directories;



?>

<link href="https://fonts.googleapis.com/css?family=Quicksand:300&display=swap" rel="stylesheet">





<section id="slider-search" style="background: transparent; margin: 0; padding:0;">

  <div class="bootstrap-wrapper" style="background: transparent;">

    <div class="container" style="background: transparent;">

      <div class="row my-0 py-0">

        <div class="col-md-12 my-0 py-0">

          <form class="p-0 m-0" action="<?php echo get_post_type_archive_link( $directory_url ) ; ?>" >

            <div class="form-row" style="line-height: 0px !important;">
            <!-- <div class="form-group col-md-1 my-0 py-0" style="line-height: 0px !important;"> </div> -->

          <div class="SearchForm">
            <div class="Looking">
            	<h6>What Are You Looking For?</h6>
               <input type="text" class="form-control" id="keyword" name="keyword"  placeholder="<?php _e( 'Looking for', 'ivdirectories' ); ?>">
			</div>

         	<div class="Location">
         		<h6>locations</h6>
			  <?php
			  // City
				$args_citys = array(
					'post_type'  => $directory_url,
					'posts_per_page' => -1,
					'meta_query' => array(
						array(
							'key'     => 'city',
							'orderby' => 'meta_value',
							'order' => 'ASC',
						),
					),
				);

				$citys = new WP_Query( $args_citys );
				$citys_all = $citys->posts;
				$get_cityies =array();
				foreach ( $citys_all as $term ) {
					$new_city="";
					$new_city=get_post_meta($term->ID,'city',true);
					if (!in_array($new_city, $get_cityies)) {
						$get_cityies[]=$new_city;
					}
				}
				// City
			  ?>

			  	<div class="custom-select" style="width: 100%">
	                <select class="form-control form-control-sm" name="dir_city">
	                   <option   value="" style="line-height: 0px !important;"><?php esc_html_e('Choose a City','ivdirectories'); ?></option>
					   <?php
						if(count($get_cityies)) {
							asort($get_cityies);
						  foreach($get_cityies as $row1) {
							  if($row1!=''){
							  ?>
							<option   value="<?php echo $row1; ?>"><?php echo $row1; ?></option>
							<?php
							}
							}
						}
						?>
	                </select>
	            </div>
              </div>

             

              <!--<div class="form-group col-md-3 my-0 py-0" style="line-height: 0px !important;">

                <select name="property-category" class="form-control form-control-sm" style="height: 40px !important;  font-size: 14px !important;">

      //             <option value=""><?php //_e('Any Category','ivdirectories');?></option>

				  // <?php



						// 				$taxonomy = $directory_url.'-category';

						// 				$args = array(

						// 					'orderby'           => 'name',

						// 					'order'             => 'ASC',

						// 					'hide_empty'        => true,

						// 					'exclude'           => array(),

						// 					'exclude_tree'      => array(),

						// 					'include'           => array(),

						// 					'number'            => '',

						// 					'fields'            => 'all',

						// 					'slug'              => '',

						// 					'parent'            => '0',

						// 					'hierarchical'      => true,

						// 					'child_of'          => 0,

						// 					'childless'         => false,

						// 					'get'               => '',



						// 				);

						// 	$terms = get_terms($taxonomy,$args); // Get all terms of a taxonomy

						// 	if ( $terms && !is_wp_error( $terms ) ) :

						// 		$i=0;

						// 		foreach ( $terms as $term_parent ) {



						// 				echo '<option  value="'.$term_parent->slug.'" ><strong>'.$term_parent->name.'<strong></option>';

						// 				?>

						// 					<?php



						// 					$args2 = array(

						// 						'type'                     => $directory_url,

						// 						'parent'                   => $term_parent->term_id,

						// 						'orderby'                  => 'name',

						// 						'order'                    => 'ASC',

						// 						'hide_empty'               => 1,

						// 						'hierarchical'             => 1,

						// 						'exclude'                  => '',

						// 						'include'                  => '',

						// 						'number'                   => '',

						// 						'taxonomy'                 => $directory_url.'-category',

						// 						'pad_counts'               => false



						// 					);

						// 					$categories = get_categories( $args2 );

						// 					if ( $categories && !is_wp_error( $categories ) ) :





						// 						foreach ( $categories as $term ) {

						// 							echo '<option  value="'.$term->slug.'">-'.$term->name.'</option>';

						// 						}



						// 					endif;

						// 			$i++;

						// 		}

						// 	endif;

						?>

                </select>

              </div>-->

              <div class="Button">
                <button type="submit" class="btn btn-sm btn-block btn-secondary text-center"><?php _e('Search','ivdirectories');?></button>
              </div>
            </div>
            </div>

          </form>

        </div>

      </div>

    </div>

  </div>

</section>

<script>
        var x, i, j, selElmnt, a, b, c; 
        x = document.getElementsByClassName("custom-select");
        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0]; 
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a); 
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 1; j < selElmnt.length; j++) { 
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function(e) { 
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function(e) { 
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }

        function closeAllSelect(elmnt) { 
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        } 
        document.addEventListener("click", closeAllSelect);
    </script>

<?php

$pos = $main_class->get_unique_keyword_values('keyword',$directory_url);



//$pos2=implode('","',$pos);

//$pos2 =json_encode($pos);



wp_enqueue_script('iv_property-ar-script-27', wp_iv_directories_URLPATH . 'admin/files/js/slider-search.js');

wp_localize_script('iv_property-ar-script-27', 'dirpro_data', array(			

		'pos'	=>$pos,

		) );

?>