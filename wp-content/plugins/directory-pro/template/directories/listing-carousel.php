<?php
    wp_enqueue_style('iv_property-style-110', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap-4.css');
    wp_enqueue_style('iv_property-style-111', wp_iv_directories_URLPATH . 'admin/files/css/styles.css');
	wp_enqueue_script('iv_property-ar-script-25', wp_iv_directories_URLPATH . 'admin/files/js/popper.min.js');
	wp_enqueue_script('iv_property-ar-script-24', wp_iv_directories_URLPATH . 'admin/files/js/bootstrap.min-4.js');


	// slick-------


	$dir_background_color=get_option('dir_background_color');
	if($dir_background_color==""){$dir_background_color='#EFEFEF';}
$directory_url=get_option('_iv_directory_url');
if($directory_url==""){$directory_url='directories';}





$post_limit='10';
if(isset($atts['post_limit']) and $atts['post_limit']!="" ){
 $post_limit=$atts['post_limit'];
}

	$dirs_data =array();
	$tag_arr= array();
	$paged =1;
	$args = array(
		'post_type' => $directory_url, // enter your custom post type
		'paged' => $paged,
		'post_status' => 'publish',
		//'fields' => 'all',
		//'orderby' => 'ASC',
		'orderby'	=> 'rand',
		'posts_per_page'=> $post_limit,  // overrides posts per page in theme settings
	);

	$lat='';$long='';$keyword_post='';$address='';$postcats ='';$selected='';
	// Add new shortcode only category

	if(isset($atts['category']) and $atts['category']!="" ){
			$postcats = $atts['category'];
			$args[$directory_url.'-category']=$postcats;
	}




	// Meta Query***********************
$city_mq ='';
if(isset($atts['dir_city']) AND $atts['dir_city']!=''){
		$city_mq = array(
		'relation' => 'AND',
			array(
				'key'     => 'city',
				'value'   => $atts['dir_city'],
				'compare' => 'LIKE'
			),
		);
}

$zip_mq='';
if(isset($atts['zipcode']) AND $atts['zipcode']!=''){
	$zip_mq = array(
		'relation' => 'AND',
			array(
				'key'     => 'postcode',
				'value'   => $atts['zipcode'],
				'compare' => 'LIKE'
			),
		);
}



$args['meta_query'] = array(
	$city_mq, $zip_mq,
);


global $wpdb;
$rand_div=rand(10,100);

 $properties = new WP_Query( $args );


$dir_background_color=get_option('dir_background_color');
if($dir_background_color==""){$dir_background_color='#EFEFEF';}

if(isset($atts['background_color']) and $atts['background_color']!="" ){
 $dir_background_color=$atts['background_color'];
}
?>


<style>
.main-container{
    background:<?php echo $dir_background_color;?>;
}
</style>

<style>
.next1,.previous1{
    background:<?php echo "#bdc3c7";?>;
}
</style>

<!-- add font-awesome for using icon -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<!-- font -->
<link href="https://fonts.googleapis.com/css?family=Quicksand:300&display=swap" rel="stylesheet">

<!-- slick slider -->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<!-- <link rel="stylesheet" type="text/css" href="http://kenwheeler.github.io/slick/slick/slick-theme.css"/> -->



<!-- slick slider -->
<section class="property-carousel" style="background:<?php echo $dir_background_color;?> !important;">
  <div class="bootstrap-wrapper" style="background: transparent !important;">
    <div class="container main-container py-3" style="background: transparent !important;">
      <div class="slick-controls<?php echo $rand_div;?>">
          <p class="next1" id="next1<?php echo $rand_div;?>"><i class="fas fa-angle-right"></i></p>
          <p class="previous1" id="previous1<?php echo $rand_div;?>"><i class="fas fa-angle-left"></i></p>
      </div>
      <div class="row multiple-items mb-2" id="multiple-items<?php echo $rand_div;?>">

  			<?php
  				$i=0;
  				 if ( $properties->have_posts() ) :
  					while ( $properties->have_posts() ) : $properties->the_post();
  					$id = get_the_ID();
  			?>
  				   <div class="col-md-4">
  					     <div class="card rounded">
                     <a href="<?php echo get_the_permalink($id);?>">
         								<?php	if(has_post_thumbnail($id)){
         										$fsrc= wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' );
         												 if($fsrc[0]!=""){
         													$fsrc =$fsrc[0];
         												}
         											 ?>

         											<img src="<?php  echo $fsrc;?>" class="rounded-top card-img-top w-100" style="height:200px !important;">
         										<?php
         										}else{	?>
         											<img src="<?php  echo wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";?>" class="rounded-top w-100 card-img-top" style="height:200px !important;">
         										<?php
         									}
         								?>
         						</a>
         					  <div class="card-body card-body-min-height pt-1 pb-0 text-justify">
         							<a href="<?php echo get_the_permalink($id);?>"><p class="title m-0 text-center"><?php echo get_the_title($id); ?></p></a>
         						 <p class="address mt-1"><?php echo get_post_meta($id,'address',true);?> <?php echo get_post_meta($id,'city',true);?> <?php echo get_post_meta($id,'zipcode',true);?> <?php echo get_post_meta($id,'country',true);?></p>
         					  </div>
                 </div>
  					</div>
  			<?php
  				$i++;
  			endwhile;
  		endif;
  			?>


       </div>
    </div>
  </div>
</section>
<!--
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
-->
<!-- slic slider js -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script type="text/javascript">
  jQuery('#multiple-items<?php echo $rand_div;?>').slick({
    arrows: true,
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    nextArrow: '#next1<?php echo $rand_div;?>',
    prevArrow: '#previous1<?php echo $rand_div;?>',
    responsive: [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false
        }
    }, {
        breakpoint: 770,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    }, {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }]
  });
</script>
