<?php get_header(); ?>
<?php
global $post,$wpdb;
$directory_url=get_option('_iv_directory_url');                 

if($directory_url=="") {$directory_url='directories';}
   $id = get_the_ID();
   $listingid = get_the_ID();
   $post_id_1 = get_post($id);
   $post_id_1->post_title;

   if (!defined('wp_iv_directories_URLPATH'))define('wp_iv_directories_URLPATH', trailingslashit(WP_PLUGIN_URL . '/' . plugin_basename(dirname(__FILE__))));
      
   $wp_iv_directories_URLPATH=wp_iv_directories_URLPATH;
   wp_enqueue_style('iv_directories-style-111', wp_iv_directories_URLPATH . 'admin/files/css/single-listing.css');

   $slider_height=get_option('_single_slider_height');                 

   if($slider_height=="") {$slider_height='450px';}
?>
      <style>
         .cbp-item-wrapper {max-height:<?php echo $slider_height;?>!important;
   }
      .single-featured-image-header {
         display: none;
      }
      </style>
   
   <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
   <!-- Feature Image -->
<?php
$wp_directory = new wp_iv_directories();

while ( have_posts() ) : the_post();    
      
   if(has_post_thumbnail()){ 
      $feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' ); 
      if($feature_image[0]!="") { $feature_img =$feature_image[0];}              
   }else{
      $feature_img= wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";              
   }

   $currentCategory = wp_get_object_terms( $id, $directory_url.'-category');
   $cat_link='';$cat_name='';$cat_slug='';
      
   if(isset($currentCategory[0]->slug)) {
      $cat_slug = $currentCategory[0]->slug;
      $i=0;
      
      foreach($currentCategory as $c) {    
            
        if($i==0) {
           $cat_name =$c->name;
        } else {
           $cat_name = $cat_name .', '.$c->name;
        }                                            
        $i++;
      }
      $cat_link= get_term_link($currentCategory[0], $directory_url.'-category');    
   }

  if ( is_user_logged_in() ) {

    global $current_user;
    $current_user = wp_get_current_user();
    
    if(get_user_meta($current_user->ID,'address',true)!='') {

      if(get_post_meta($id,'address',true)!='') {

          $dir_map_api=get_option('_dir_map_api');
          if($dir_map_api=="") {
            $dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';
          }

          //Post
          $post_add = get_post_meta($id,'address',true);

          if(get_post_meta($id,'latitude',true)!=''){$ins_lat=get_post_meta($id,'latitude',true);}
          if(get_post_meta($id,'longitude',true)!=''){$ins_lng=get_post_meta($id,'longitude',true);}

          if( get_user_meta($current_user->ID,'latitude',true)!=''){ $user_lat = get_user_meta($current_user->ID,'latitude',true);}
          if( get_user_meta($current_user->ID,'longitude',true)!=''){ $user_lng = get_user_meta($current_user->ID,'longitude',true);}

          if(($ins_lat == $user_lat) && ($ins_lng == $user_lng)) {
              $discal = "0 Km";

          } else {
              $theta = $ins_lng - $user_lng;
              $dist = sin(deg2rad($ins_lat)) * sin(deg2rad($user_lat)) +  cos(deg2rad($ins_lat)) * cos(deg2rad($user_lat)) * cos(deg2rad($theta));
              $dist = acos($dist);
              $dist = rad2deg($dist);
              $miles = $dist * 60 * 1.1515;
              $kmdistance = $miles * 1.609344;
              $kmdistance = round($kmdistance,2);
              $discal = $kmdistance." Km";
          }

      } else {
        $discal = " ";
      }
    } else {
      $discal = " ";
    }
  } else {
      $discal = " ";
  }

?>
<section>
   <div class="DetailsArea">      
      <div class="container">

        <div class="col-sm-12">
          <div class="DetailsSearch">
            <?php echo do_shortcode('[slider_search]');?>
          </div>
        </div>

        <div class="DetailsCard">
          <div class="col-sm-7 padding_none">
            <div class="DetailsCardLeft">
              <figure>
              <?php
                if(has_post_thumbnail()) {
              ?>
                <?php echo the_post_thumbnail();?>
              <?php
                } else {
              ?>
                  <img width="1242" height="606" src="https://oteneto.com/wp-content/uploads/2019/10/IMG_0447-1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" sizes="(max-width: 1242px) 100vw, 1242px">
              <?php
                }
              ?>

              </figure>
              <figcaption>
                <h3>
                  <?php the_title(); ?> 
                  <span class="Featured" id="fav_dir<?php echo $id; ?>" >
                    <span class="verified">
                      <img src="<?php echo get_template_directory_uri();?>/images/Verfied.png">verified / Featured / </span>
                        <?php
                        $user_ID = get_current_user_id();                      
                          if($user_ID > 0) {
                            $my_favorite = get_post_meta($id,'_favorites',true);
                            $all_users = explode(",", $my_favorite);                        
                              if (in_array($user_ID, $all_users)) {
                            ?>
                            <a style="text-decoration: none;" data-toggle="tooltip" data-placement="bottom" title="<?php _e('Added to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_unfavorite('<?php echo $id; ?>')" > <span class="hide-sm"><i class="fa fa-heart  red-heart fa-lg"></i></span></a>
                          <?php               
                            } else { 
                          ?>
                            <a style="text-decoration: none;" data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_favorite('<?php echo $id; ?>')" > <span class="hide-sm"><i class="fa fa-heart fa-lg"></i> </span> </a>
                          <?php 
                          } 
                          } else { 
                        ?>
                            <a style="text-decoration: none;" data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_favorite('<?php echo $id; ?>')" > <span class="hide-sm"><i class="fa fa-heart fa-lg"></i> </span> </a>
                        <?php             
                      }
                    ?>
                    </span>  
                </h3>
                
                <h5>
                    <?php
                        $dir_single_review_show=get_option('_dir_single_review_show');
                        if($dir_single_review_show==""){$dir_single_review_show='yes';}
                        if($dir_single_review_show=='yes'){

                        $total_reviews_point = $wpdb->get_col("SELECT SUM(pm.meta_value) FROM {$wpdb->postmeta} pm
                         INNER JOIN {$wpdb->posts} p ON p.ID = pm.post_id
                         WHERE pm.meta_key = 'review_value'
                         AND p.post_status = 'publish'
                         AND p.post_type = 'dirpro_review' AND p.post_author = '".$id."'");


                        $argsreviw = array( 'post_type' => 'dirpro_review','author'=>$id,'post_status'=>'publish' );
                        $ratings = new WP_Query( $argsreviw );

                        $total_review_post = $ratings->post_count;

                        $avg_review=0;
                        
                        if(isset($total_reviews_point[0])){
                          $avg_review= (int)$total_reviews_point[0]/(int)$total_review_post;
                          $avg_review = (int)$avg_review;
                        }
                      ?>
                          <span class="Rats">
                              <i class="fa fa-star <?php echo ($avg_review>0?'black-star': 'white-star ');?>"></i>
                              <i class="fa fa-star <?php echo ($avg_review>=2?'black-star': 'white-star');?>"></i>
                              <i class="fa fa-star <?php echo ($avg_review>=3?'black-star': 'white-star');?>"></i>
                              <i class="fa fa-star <?php echo ($avg_review>=4?'black-star': 'white-star');?>"></i>
                              <i class="fa fa-star <?php echo ($avg_review>=5?'black-star': 'white-star');?>"></i>
                              <?php echo $total_review_post;?>
                              <a href="#">(<?php echo $avg_review;?> review)</a>
                          </span>
                      <?php
                        }
                    ?>
                </h5>
                <p><span><i class="fa fa-map-marker"></i></span>
                    <?php
                        if(get_post_meta($id,'address',true)!='') {
                           $lat = get_post_meta($id,'latitude',true);
                           $lng=get_post_meta($id,'longitude',true);

                           echo ' <a style="text-decoration: none;" href="http://maps.google.com/maps?saddr=Current+Location&amp;daddr='.get_post_meta($id,'address',true).'+'.get_post_meta($id,'state',true).'+'.get_post_meta($id,'postcode',true).'+'.get_post_meta($id,'country',true).'" target="_blank">'.get_post_meta($id,'address',true).'</a>';
                        }
                     ?>
                </p>
                <?php if(strlen($discal) > 0) { ?>
                  <p><span> <i class="fa fa-map-marker"></i></span> Distance  <?php echo $discal;?></p>
                <?php }?>
                <p><span><i class="fa fa-phone"></i></span>
                    <?php
                        if(get_post_meta($id,'phone',true)!='' ) {
                    ?>
                    <?php echo '<a style="text-decoration: none;" href="tel:'.get_post_meta($id,'phone',true).'">'.get_post_meta($id,'phone',true).'</a>' ;?>
                    <?php
                        }
                    ?>
                </p>

              <ul>
                <li>
                  <figure><img src="<?php echo get_template_directory_uri();?>/images/Details-Icon-6.png"></figure> <?php if(get_post_meta($id, 'whatsapp', true)!='') {?> <a href=" https://api.whatsapp.com/send?phone=<?php echo get_post_meta($id, 'whatsapp', true); ?>" target="__blank"><?php echo get_post_meta($id, 'whatsapp', true); ?> </a> <?php }?>
                </li>

                <?php
                  if(get_post_meta($id,'contact_web',true)!='' ){
                ?>
                
                <li>
                  <figure><img src="<?php echo get_template_directory_uri();?>/images/Details-Icon-8.png"></figure> 
                    <?php
                    if(get_post_meta($id,'contact-email',true)!='' ) {
                      ?>
                      <?php echo '<a style="text-decoration: none;" href="mailto:'. get_post_meta($id,'contact-email',true) .'">'.get_post_meta($id,'contact-email',true).'&nbsp;</a>';?>
                      <?php
                    }
                    ?>
                </li>

                <?php
                  }
                ?>

                <?php
                  if(get_post_meta($id,'contact_web',true)!='' ){
                ?>

                <li>
                  <figure><img src="<?php echo get_template_directory_uri();?>/images/Details-Icon-9.png"></figure>
                <?php
                  if(get_post_meta($id,'contact_web',true)!='' ){
                      $contact_web=get_post_meta($id,'contact_web',true);
                      $contact_web=str_replace('https://','',$contact_web);
                      $contact_web=str_replace('http://','',$contact_web);
                      
                      echo '<a style="text-decoration: none;" href="http://'. $contact_web.'" target="_blank">'. get_post_meta($id,'contact_web',true).'&nbsp; </a>';
                  }
                ?>
                </li>

                <?php
                  }
                ?>

                <?php
                  if(get_post_meta($id,'business_type',true)!='' ){
                ?>
                <li><figure><img src="<?php echo get_template_directory_uri();?>/images/Details-Icon-1.png"></figure> <?php echo ' '.get_post_meta($id, 'business_type', true); ?> </li>                     
                <?php
                  }
                ?>

                <?php
                  if(get_post_meta($id,'main_products',true)!='' ){
                ?>
                <li><figure><img src="<?php echo get_template_directory_uri();?>/images/Details-Icon-3.png"></figure> <?php echo ' '.get_post_meta($id, 'main_products', true); ?> </li>                    
                <?php
                  }
                ?>

                <?php
                  if(get_post_meta($id,'number_of_employees',true)!='' ){
                ?>
                <li><figure><img src="<?php echo get_template_directory_uri();?>/images/Details-Icon-5.png"></figure> <?php echo ' '.get_post_meta($id, 'number_of_employees', true); ?> Employees</li>

                <?php
                  }
                ?>

                <?php
                  if(get_post_meta($id,'company_timing',true)!='' ){
                ?>
                  <li><figure><img src="<?php echo get_template_directory_uri();?>/images/Details-Icon-5.png"></figure> Opening Time : <?php echo ' '.get_post_meta($id, 'company_timing', true); ?> </li>
                <?php
                  }
                ?>
              </ul>

                <h6>
                  <a href="#TabsEnquiry">review</a>
                  <a href="#TabsEnquiry">Enquiry</a>
                </h6>

              </figcaption>
            </div>
          </div>
          <div class="col-sm-5 padding_none"> 
            <?php
              $address=get_post_meta($id,'address',true).'+'.get_post_meta($id,'city',true).'+'.get_post_meta($id,'postcode',true).'+'.get_post_meta($id,'country',true);
            ?>
          <iframe width="100%" height="325" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=<?php echo $address; ?>&amp;ie=UTF8&amp;&amp;output=embed"></iframe>

          </div>
          <div class="clear"></div>
        </div>

         <div class="col-sm-12">
            <div class="DetailsHead">
            <?php
              $dir_share_show=get_option('_dir_share_show');  
              
              if($dir_share_show==""){$dir_share_show='yes';}
                
              if($dir_share_show=='yes'){
            ?>
                <div class="cbp-l-project-details-title">
                  <?php _e('Share :','ivdirectories'); ?>
                  <?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
                <!--<a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php //_e('Share On Facebook','ivdirectories'); ?>" href="https://www.facebook.com/sharer/sharer.php?u=<?php //the_permalink();  ?>"><i class="fa fa-facebook-square fa-2x"></i></a> <a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php //_e('Share On Twitter','ivdirectories'); ?>" href="https://twitter.com/home?status=<?php //the_permalink();  ?>"><i class="fa fa-twitter fa-2x"></i></a> <a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php //_e('Share On linkedin','ivdirectories'); ?>" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php //the_permalink();  ?>&title=<?php //the_title(); ?>&summary=&source="><i class="fa fa-linkedin-square fa-2x"></i></a> <a data-toggle="tooltip" class="icon-blue" data-placement="bottom" title="<?php //_e('Share On google+','ivdirectories'); ?>" href="https://plus.google.com/share?url=<?php //the_permalink();  ?>"><i class="fa fa-google-plus-square fa-2x"></i></a>-->

                </div>
            <?php
                }
            ?> 
            </div>
         </div>

         <div class="col-sm-9">
            <div class="DetailsLeft">
<?php
            if($wp_directory->check_reading_access('image',$id)) {
               $gallery_ids=get_post_meta($id ,'image_gallery_ids',true);
               $gallery_ids_array = array_filter(explode(",", $gallery_ids));

               if(sizeof($gallery_ids_array)>0) {
?>
               <div class="Details-Gallery">
                  <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                     <div class="carousel-inner">
                        <span class="verified"><img src="<?php echo get_template_directory_uri();?>/images/Verfied.png">verified</span>
<?php
                     $i=1;
                     foreach($gallery_ids_array as $slide) {
                        
                        if($slide!='') { 
?>
                           <div class="item <?php if($i == 1){ echo 'active';}?>">
                               <img src="<?php echo wp_get_attachment_url( $slide ); ?>">
                           </div>
<?php
                        $i++;
                        }
                     }
?>
                     </div>

                     <ol class="carousel-indicators">
<?php
                     $ii=1;
                     foreach($gallery_ids_array as $slide) {
                        
                        if($slide!='') { 
?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $ii;?>" class="<?php if($ii == 1){ echo 'active';}?>">
                            <img src="<?php echo wp_get_attachment_url( $slide ); ?>">
                        </li>
<?php
                        $ii++;
                        }
                     }
?>
                        <div class="clear"></div>
                     </ol>
 
                     <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-menu-left"></span>
                        <span class="sr-only">Previous</span>
                     </a>
                     <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-menu-right"></span>
                        <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
<?php
               }
            } else { 
               echo get_option('_iv_visibility_login_message');
            }  
?>
               <div class="Details-Content">
                  

                  <h4>Description</h4>

<?php
                  if($wp_directory->check_reading_access('description',$id)) {
?>
                  <p>
<?php
                     $my_postid = $id;//This is page id or post id
                     $content_post = get_post($my_postid);
                     $content = $content_post->post_content;
                     $content = apply_filters('the_content', $content);
                     $content = str_replace(']]>', ']]&gt;', $content);
                     echo $content;
?>
                  </p>
<?php
                  } else { 
                     echo get_option('_iv_visibility_login_message');      
                  }
?>
               </div>

               <div class="Details-Content">
                  <!--<h4>Video</h4>-->

  <?php
        if($wp_directory->check_reading_access('video',$id)) {
          $video_vimeo_id= get_post_meta($id,'vimeo',true);
          $video_youtube_id=get_post_meta($id,'youtube',true);
          
          if($video_vimeo_id!='' || $video_youtube_id!='') {
?>
              <div class="cbp-2-project-desc br-tme">
            <div class="cbp-l-project-desc-title">
              <?php _e('Video','ivdirectories'); ?>
            </div>
            <div class="cbp-l-project-desc-text">
<?php
              $v=0;
              $video_vimeo_id= get_post_meta($id,'vimeo',true);
              
              if($video_vimeo_id!="") { $v=$v+1;
?>
                <div class="video-container">
                  <iframe src="https://player.vimeo.com/video/<?php echo $video_vimeo_id; ?>" width="100%" height="315px" frameborder="0"></iframe>
                </div>
<?php
              }
?>
<?php
              $video_youtube_id=get_post_meta($id,'youtube',true);
              
              if($video_youtube_id!="") {
                echo($v==1?'<hr>':''); 
?>
                    <div class="video-container">
                      <iframe width="100%" height="315px" src="https://www.youtube.com/embed/<?php echo $video_youtube_id; ?>" frameborder="0" allowfullscreen></iframe>
                    </div>
<?php   
              }
?>
                </div>
              </div>
<?php
          }
        }
?>
                 

               </div>

               <div class="Details-Content"  id="TabsEnquiry">
                  <h4>Ratings & Reviews</h4>

                  <ul class="Ratings" id="myList">
                  <?php
                     $user_id=$id;
                     $post_type='dirpro_review';
                     
                     $sql="SELECT * FROM $wpdb->posts WHERE post_type ='".$post_type."'  and post_author='".$user_id."'    and post_status='publish' ORDER BY ID DESC";

                     $author_reviews = $wpdb->get_results($sql);

                     foreach ( $author_reviews as $review ) {  
                        $user_review_val=0;
                        $review_submitter=get_post_meta($review->ID, 'review_submitter', true);    
                        $user_review_val=get_post_meta($review->ID, 'review_value', true);

                        $user_image_path=get_user_meta($review_submitter, 'iv_profile_pic_url',true);

                        if($user_image_path=='') {
                           $user_image_path=wp_iv_directories_URLPATH.'assets/images/Blank-Profile.jpg';
                        }
                        $userreview = get_user_by( 'id', $review_submitter );    
                        $name_display=get_user_meta($review_submitter,'first_name',true).' '.get_user_meta($review_submitter,'last_name',true);
                        
                        $profile_public=get_option('_iv_directories_profile_public_page');
                        $reg_page_u= get_permalink( $profile_public); 
                        
                        $reg_page_u= $reg_page_u.'?&id='.$review_submitter;
                  ?>    
                       <li>
                           <aside>
                              <figure><img src="<?php echo $user_image_path;?>"></figure>
                              <h6><?php echo (trim($name_display)!=""? $name_display : $userreview->display_name ); ?>
                              </h6>

                               <h5>
                                    <button type="button" class=" <?php echo ($user_review_val>0?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
                                    <i class="fa fa-star <?php echo ($user_review_val>0?'white-star': 'black-star');?>"></i>
                                    </button>
                                    <button type="button" class=" <?php echo ($user_review_val>1?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
                                     <i class="fa fa-star <?php echo ($user_review_val>1?'white-star': 'black-star');?>"></i>
                                    </button>
                                    <button type="button" class=" <?php echo ($user_review_val>2?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
                                     <i class="fa fa-star <?php echo ($user_review_val>2?'white-star': 'black-star');?>"></i>
                                    </button>
                                    <button type="button" class=" <?php echo ($user_review_val>3?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
                                     <i class="fa fa-star <?php echo ($user_review_val>3?'white-star': 'black-star');?>"></i>
                                    </button>
                                    <button type="button" class=" <?php echo ($user_review_val>4?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
                                    <i class="fa fa-star <?php echo ($user_review_val>4?'white-star': 'black-star');?>"></i>
                                    </button>
                               </h5>
                           </aside>
                           <p><?php echo $review->post_title; ?></p>
                           <p><?php echo $review->post_content; ?></p>
                           <time><?php echo date('M d, Y',strtotime($review->post_date)); ?></time>
                       </li>
                  <?php
                     }
                  ?>
                  </ul>
                  <div id="loadMore">Load more</div>
                  <div id="showLess" style="display: none;">Show less</div>

                   <div class="Tabing">
                       <ul class="nav nav-tabs">
                           <li class="active"><a data-toggle="tab" href="#review">Write a review</a></li>
                           <li><a data-toggle="tab" href="#Enquiry">Send Enquiry</a></li>
                           <li><a data-toggle="tab" href="#Quality">Q & A</a></li>
                       </ul>

                       <div class="tab-content">
                           <div id="review" class="tab-pane fade in active">
                              <form id="iv_review_form" name="iv_review_form" class="" role="form" onsubmit="return false;">
                                 <div class="row">
                                    <div class="col-sm-12">
                                       <div class="row"><div class="col-sm-12"><h4><?php  _e('Submit your review', 'ivdirectories'); ?></h4></div></div>
                                       <div class="row form-group">
                                          <div class="col-sm-3">
                                             <label><?php  _e('Subject', 'ivdirectories'); ?></label>
                                          </div>   
                                          <div class="col-sm-9">
                                             <input type="text" class="form-control" name="review_subject"   value="" placeholder="<?php  _e('Enter review title', 'ivdirectories'); ?>"> 
                                          </div>
                                       </div>
                                       <div class="row form-group">
                                          <div class="col-sm-3">
                                             <label><?php  _e('Rating', 'ivdirectories'); ?></label>
                                          </div>   
                                          <div class="col-sm-9">
                                             <div class="stars">   
                                                <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>
                                                <label class="star star-5" for="star-5"></label> 
                                                <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>
                                                <label class="star star-4" for="star-4"></label>
                                                <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
                                                <label class="star star-3" for="star-3"></label>
                                                <input class="star star-2" id="star-2" type="radio" name="star" value="2" />
                                                <label class="star star-2" for="star-2"></label>
                                                <input class="star star-1" id="star-1" type="radio" name="star" value="1" />
                                                <label class="star star-1" for="star-1"></label> 
                                             </div>
                                          </div> 
                                       </div>
                                       <div class="row form-group">
                                          <div class="col-sm-3">
                                             <label><?php  _e('Comments', 'ivdirectories'); ?></label>
                                          </div>   
                                          <div class="col-sm-9">
                                             <textarea class="form-control" cols="50"  name="review_comment" id="review_comment" placeholder="<?php  _e('Enter review comments', 'ivdirectories'); ?>" rows="5"></textarea>
                                          </div>
                                       </div>   
                                       
                                       <div class="row">
                                          <div class="col-sm-9 col-sm-offset-3">
                                             <button type="button" class="Submit_review" onclick="return iv_submit_review();">
                                                <?php  _e('Submit', 'ivdirectories'); ?> 
                                             </button>
                                             <input type="hidden" name="listingid" id="listingid" value="<?php echo $listingid; ?>">
                                             <div id="rmessage"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                           
                           <div id="Enquiry" class="tab-pane fade in">
                              <form action="" id="message-pop" name="message-pop"  method="POST" role="form">
                                <h6>Send an Email Enquiry to ChooseNget</h6>
                                <div class="row">
                                   <div class="form-group cbp-l-grid-projects-desc col-sm-6">
                                      <input id="subject" name ="subject" type="text" placeholder="<?php _e( 'Enter Subject', 'ivdirectories' ); ?>" class="form-control cbp-search-input">
                                   </div>
                                   <div class="form-group cbp-l-grid-projects-desc col-sm-6">
                                      <input name ="email_address" id="email_address" type="text" placeholder="<?php _e( 'Enter Email', 'ivdirectories' ); ?>" class="form-control cbp-search-input">
                                   </div>
                                   <div class="form-group cbp-l-grid-projects-desc col-sm-12">
                                      <textarea name="message-content" id="message-content"  class="form-control cbp-search-"  cols="54" rows="4" title="<?php _e( 'Please Enter Message', 'ivdirectories' ); ?>"  placeholder="<?php _e( 'Enter Message', 'ivdirectories' ); ?>"  ></textarea>
                                   </div>
                                   
                                   <div class="form-group col-sm-12">
                                     <input type="hidden" name="dir_id" id="dir_id" value="<?php echo $id; ?>">
                                     <a onclick="send_message_iv();" class="Submit">
                                        <?php _e( 'Send Message', 'ivdirectories' ); ?>
                                     </a>
                                     <div id="update_message_popup"></div>
                                    </div>
                                </div>
                              </form>
                           </div>

                           <div id="Quality" class="tab-pane fade in">
                              <?php echo do_shortcode('[dwqa-submit-question-form]');?>
                           </div>
                       </div>
                   </div>

               </div>
            </div>
         </div>

         <div class="col-sm-3">
            <div class="DetailsRight">
               <div class="DetailsProperty">
                  <h4>Related Companies</h4>
                  <?php
                    
                     $post_limit='30';
                     $args = array(
                        'post_type' => $directory_url, // enter your custom post type
                        'post_status' => 'publish',
                        'orderby'   => 'rand',
                        'posts_per_page'=> $post_limit,
                     );
                     $properties = new WP_Query( $args );
                     $i=0;
                     
                     if ( $properties->have_posts() ) :
                        while ( $properties->have_posts() ) : $properties->the_post();
                          $id1 = get_the_ID();
                          $currentCategory1 = wp_get_object_terms( $id1, $directory_url.'-category');
                          $cat_slug1 = '';
                          $cat_slug1 = $currentCategory1[0]->slug;
                           
                          if($cat_slug1 == $cat_slug && $i < 6 ) {

                           $total_review_point=0;  
                           $one_review_total=0;
                           $two_review_total=0;
                           $three_review_total=0;
                           $four_review_total=0;
                           $five_review_total=0;

                           $post_type='dirpro_review';
                           $sql = "SELECT * FROM $wpdb->posts WHERE post_type ='".$post_type."' and post_author='".$id1."' and post_status='publish' ORDER BY ID DESC";

                           $author_reviews = $wpdb->get_results($sql);
                     
                           $total_reviews=count($author_reviews);
                     
                           if($total_reviews>0){
                     
                              foreach ( $author_reviews as $review ) {                       
                                 $review_val=(int)get_post_meta($review->ID,'review_value',true);
                                 $total_review_point=$total_review_point+ $review_val;                         
                                 if($review_val=='1'){
                                    $one_review_total=$one_review_total+1;
                                 }
                                 if($review_val=='2'){
                                    $two_review_total=$two_review_total+1;
                                 }
                                 if($review_val=='3'){
                                    $three_review_total=$three_review_total+1;
                                 }
                                 if($review_val=='4'){
                                    $four_review_total=$four_review_total+1;
                                 }
                                 if($review_val=='5'){
                                    $five_review_total=$five_review_total+1;
                                 }
                              }  
                           }
                           $avg_review=0;
                           if($total_review_point>0) {
                              $avg_review= $total_review_point/$total_reviews;
                           }
                  ?>
                  <div class="PropertyBox">
                     <figure>
                      <span class="verified"><img src="<?php echo get_template_directory_uri();?>/images/Verfied.png">verified</span>
                      <a href="<?php echo get_the_permalink($id1);?>">
                        <!--<span class="verified"><img src="<?php //echo get_template_directory_uri();?>/images/Verfied.png">verified</span>
                        <span class="Premium">Premium</span>-->
                        <?php 
                           if(has_post_thumbnail($id)){
                              $fsrc= wp_get_attachment_image_src( get_post_thumbnail_id( $id1 ), 'large' );
                                 if($fsrc[0]!="") {
                                    $fsrc =$fsrc[0];
                                 }
                        ?>
                                 <img src="<?php  echo $fsrc;?>">
                        <?php
                           } else {
                        ?>
                                 <img src='<?php  echo wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";?>'>
                        <?php
                           }
                        ?>
                        <aside>
                           <h4><?php echo get_the_title($id1); ?></h4>
                           <p><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo get_post_meta($id1,'address',true);?> <?php echo get_post_meta($id1,'city',true);?> <?php echo get_post_meta($id1,'country',true);?><!--<span>5.2km</span>--></p>
                        </aside>
                      </a>
                     </figure>
                     <figcaption>
                        <h5>
                           <button type="button" class=" <?php echo ($avg_review>0?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                       
                             <i class="fa fa-star 3x <?php echo ($avg_review>0?'white-star': 'black-star');?>"></i>
                           </button>
                           <button type="button" class=" <?php echo ($avg_review>=2?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                      
                             <i class="fa fa-star 3x <?php echo ($avg_review>=2?'white-star': 'black-star');?>"></i>
                           </button>
                           <button type="button" class=" <?php echo ($avg_review>=3?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                      
                             <i class="fa fa-star 3x <?php echo ($avg_review>=3?'white-star': 'black-star');?>"></i>
                           </button>
                           <button type="button" class=" <?php echo ($avg_review>=4?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                      
                             <i class="fa fa-star 3x <?php echo ($avg_review>=4?'white-star': 'black-star');?>"></i>
                           </button>                     
                           <button type="button" class=" <?php echo ($avg_review>=5?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">                      
                             <i class="fa fa-star 3x <?php echo ($avg_review>=5?'white-star': 'black-star');?>"></i>
                           </button>
                        </h5>
                        <p> <?php the_excerpt();?> </p>
                        <ul>
                            <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-picture-o" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-globe" aria-hidden="true"></i></a></li>
                        </ul>
                        <div class="clear"></div>
                     </figcaption>
                  </div>

                  <?php
                          $i++;
                       }
                     endwhile;
                     endif;
                  ?>
               </div>

               <!--<div class="DetailsKeywords">
                   <h4>Keywords</h4>
                   <ul>
                       <li><a href="#">Lorem impsum</a></li>
                       <li><a href="#">Lorem impsu</a></li>
                       <li><a href="#">Lorem impsum</a></li>
                       <li><a href="#">Lorem imp</a></li>
                       <li><a href="#">Lorem imp</a></li>
                   </ul>
               </div>-->
            </div>
         </div>
      </div>
   </div>
</section>

<?php
endwhile;
?>

<section>
   <div class="container">
    <?php
      $args = array(
          'post_type' => 'sale_promotions',
          'post_status' => 'publish',
          'p' => '2043',   // id of the post you want to query
      );
      $my_posts = new WP_Query($args);  

      if($my_posts->have_posts()) :
      while ( $my_posts->have_posts() ) : $my_posts->the_post();
          
        if ( has_post_thumbnail() && ! post_password_required() ) : 
            $imgURL = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
        endif;
        
    ?>
      <div class="PromotationArea">
        <div class="PromotationImages" style="background-image: url(<?php echo $imgURL; ?>);">
        </div>
         <div class="Promotationconetnt">
            <h4><?php the_content(); ?></h4>
            <button type="button" class="btnbtn-primary" data-toggle="modal" data-target="#promotemodalone1" id="Promotes"> <i class="fa fa-plus" aria-hidden="true"></i> </button>
         </div>
         <div class="clear"></div>
      </div>
    <?php
      endwhile;
      endif;
    ?>
   </div>
</section>

<script src="<?php echo get_template_directory_uri();?>/js/owl.carousel.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/index.js"></script>

<script type="text/javascript">

  function save_favorite(id) {       
    
      var isLogged ="<?php echo get_current_user_id();?>";
                               
                if (isLogged=="0") {                   
                     alert("<?php _e('Please login to add favorite','ivdirectories'); ?>");
                } else { 
            
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';            
            var search_params={
              "action"  :   "iv_directories_save_favorite", 
              "data": "id=" + id,
            };
            
            jQuery.ajax({         
              url : ajaxurl,           
              dataType : "json",
              type : "post",
              data : search_params,
              success : function(response){ 
                jQuery("#fav_dir"+id).html('<a data-toggle="tooltip" data-placement="bottom" title="<?php _e('Added to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_unfavorite('+id+')" ><span class="hide-sm"><i class="fa fa-heart  red-heart fa-lg"></i>&nbsp;&nbsp; </span></a>');
              
                
              }
            });
            
        }  
        
    }
    
  function save_unfavorite(id) {
      var isLogged ="<?php echo get_current_user_id();?>";
                               
                if (isLogged=="0") {                   
                     alert("<?php _e('Please login to remove favorite','ivdirectories'); ?>");
                } else { 
            
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';           
            var search_params={
              "action"  :   "iv_directories_save_un_favorite",  
              "data": "id=" + id,
            };
            
            jQuery.ajax({         
              url : ajaxurl,           
              dataType : "json",
              type : "post",
              data : search_params,
              success : function(response){ 
                jQuery("#fav_dir"+id).html('<a data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" onclick="save_favorite('+id+')" ><span class="hide-sm"><i class="fa fa-heart fa-lg "></i>&nbsp;&nbsp; </span></a>');
              
                
              }
            });
            
        }  
        
    }



   function iv_submit_review(){
      var isLogged ="<?php echo get_current_user_id();?>";

        if (isLogged=="0") {
             alert("<?php _e('Please login to add review','ivdirectories'); ?>");
        } else {
         var form = jQuery("#iv_review_form");
         
         if (jQuery.trim(jQuery("#review_comment", form).val()) == "") {
              alert("<?php _e('Please put your comment','ivdirectories'); ?>");
         } else {
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
               var loader_image = '<img style="width:100px" src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
               jQuery('#rmessage').html(loader_image);
               var search_params={
               "action"  :  "iv_directories_save_user_review",
               "form_data": jQuery("#iv_review_form").serialize(),
               };
               jQuery.ajax({
               url : ajaxurl,
               dataType : "json",
               type : "post",
               data : search_params,
               success : function(response){
                  jQuery('#rmessage').html('<div class="col-sm-7 alert alert-info alert-dismissable"><a class="panel-close close" data-dismiss="alert">x</a>'+response.msg +'.</div>');
                  jQuery("#iv_review_form")[0].reset();
               }
            });
         }    
      }
   }

   function send_message_iv() {
      var formc = jQuery("#message-pop");
      
      if (jQuery.trim(jQuery("#message-content",formc).val()) == "") {
         alert("Please put your message");
      } else {    
         var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
         var loader_image = '<img style="width:60px" src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
         jQuery('#update_message_popup').html(loader_image); 
         var search_params={
            "action"  :    "iv_directories_message_send",   
            "form_data":   jQuery("#message-pop").serialize(),                
         };          
         jQuery.ajax({              
            url : ajaxurl,              
            dataType : "json",
            type : "post",
            data : search_params,
            success : function(response){
                  jQuery('#update_message_popup').html(response.msg );
                  jQuery("#message-pop").trigger('reset');                 
            }
         });
      }  
   }

   function isValidEmailAddress(emailAddress) {
      var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&"\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&"\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
      return pattern.test(emailAddress);
   }


   $(document).ready(function () {
    size_li = $("#myList li").size();
    x=3;
    $('#myList li:lt('+x+')').show();
    
    $('#loadMore').click(function () {
        x= (x+5 <= size_li) ? x+5 : size_li;
        $('#myList li:lt('+x+')').show();

        if(x == size_li) {
            $('#loadMore').css('display','none');
            $('#showLess').css('display','block');
        }
    });
    
    $('#showLess').click(function () {
        x=(x-5<0) ? 3 : x-5;
        $('#myList li').not(':lt('+x+')').hide();

        if(x < size_li) {
            $('#loadMore').css('display','block');
            $('#showLess').css('display','none');
        }
    });
});
</script>
<style type="text/css">
#loadMore {
    width: 150px;
    line-height: 40px;
    border-radius: 2px;
    margin: 0 auto;
    background: #219150;
    color: #fff;
    cursor: pointer;
    text-align: center;
}
#showLess {
    width: 150px;
    line-height: 40px;
    border-radius: 2px;
    margin: 0 auto;
    background: #219150;
    color: #fff;
    cursor: pointer;
    text-align: center;
}

.btn-black{
 background-color: #000000;

}
         
div.stars {
  width: 100%;
  display: inline-block;
}

input.star { display: none; }

label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}

input.star:checked ~ label.star:before {
  content: '\f005';
  color: #FD4;
  transition: all .25s;
}

input.star-5:checked ~ label.star:before {
  color: #FE7;
  text-shadow: 0 0 20px #952;
}

input.star-1:checked ~ label.star:before { color: #F62; }

label.star:hover { transform: rotate(-15deg) scale(1.3); }

label.star:before {
  content: '\f006';
  font-family: FontAwesome;
}

.stars
{
    margin: 0px 0;
    font-size: 24px;
    color: #d17581;
}  
.black-star{
   color: #2c3e50;
}  

.white-star {
   color: #f4f7f8;
}
.bootstrap-wrapper .btn-warning {    
    border-color: #0099e5!important;
}  
   
.btn-grey{
    background-color:#D8D8D8!important;
   color:#FFF!important;
}
</style>
<?php
get_footer();
?>
