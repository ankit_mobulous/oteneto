<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<div class="dproreviews" >
  			
				<div class="row">
					<div class="col-md-6">
						<div class="rating-block">
							<?php
							global $wpdb;
							$user_id=$id;
							$total_review_point=0;	
							$one_review_total=0;
							$two_review_total=0;
							$three_review_total=0;
							$four_review_total=0;
							$five_review_total=0;
							
							$post_type='dirpro_review';
							$sql="SELECT * FROM $wpdb->posts WHERE post_type ='".$post_type."'  and post_author='".$user_id."' 	and post_status='publish' ORDER BY ID DESC";
							$reg_page_user='';
							$iv_redirect_user = get_option( '_ep_ivdirectories_profile_public_page');
							if($iv_redirect_user!='defult'){
								$reg_page_user= get_permalink( $iv_redirect_user) ;
							}
							$listing_author_link=get_option('listing_author_link');	
							if($listing_author_link==""){$listing_author_link='author';}		
							$author_reviews = $wpdb->get_results($sql);
							
							$total_reviews=count($author_reviews);
							
							if($total_reviews>0){
							
							foreach ( $author_reviews as $review )								
								{								
									$review_val=(int)get_post_meta($review->ID,'review_value',true);
									$total_review_point=$total_review_point+ $review_val;									
									if($review_val=='1'){
										$one_review_total=$one_review_total+1;
									}
									if($review_val=='2'){
										$two_review_total=$two_review_total+1;
									}
									if($review_val=='3'){
										$three_review_total=$three_review_total+1;
									}
									if($review_val=='4'){
										$four_review_total=$four_review_total+1;
									}
									if($review_val=='5'){
										$five_review_total=$five_review_total+1;
									}		
								}	
							}
								$avg_review=0;
								if($total_review_point>0){
									$avg_review= $total_review_point/$total_reviews;
								}
								
							?>
							
							<h4><?php  _e('Average rating', 'ivdirectories'); ?></h4>
							<h2 class="bold padding-bottom-7"><?php echo number_format($avg_review,1,'.',''); ?> / <?php  _e('5', 'ivdirectories'); ?></h2>
							
							<button type="button" class=" <?php echo ($avg_review>0?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">							  
							  <i class="fa fa-star 3x <?php echo ($avg_review>0?'white-star': 'black-star');?>"></i>
							</button>
							<button type="button" class=" <?php echo ($avg_review>=2?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">							  
							  <i class="fa fa-star 3x <?php echo ($avg_review>=2?'white-star': 'black-star');?>"></i>
							</button>
							<button type="button" class=" <?php echo ($avg_review>=3?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">							  
							  <i class="fa fa-star 3x <?php echo ($avg_review>=3?'white-star': 'black-star');?>"></i>
							</button>
							<button type="button" class=" <?php echo ($avg_review>=4?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">							  
							  <i class="fa fa-star 3x <?php echo ($avg_review>=4?'white-star': 'black-star');?>"></i>
							</button>							
							<button type="button" class=" <?php echo ($avg_review>=5?'btn-black': 'btn-default btn-grey');?>  btn-sm" aria-label="Left Align">							  
							  <i class="fa fa-star 3x <?php echo ($avg_review>=5?'white-star': 'black-star');?>"></i>
							</button>
							
						</div>
					</div>
					<div class="col-md-6">
						
						<h4><?php  _e('Rating breakdown', 'ivdirectories'); ?> </h4>
						<div class="pull-left">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">5 <i class="fa fa-star 3x blue-star"></i></div>
							</div>
							<div class="pull-left" style="width:180px;">
								<div class="progress" style="height:9px; margin:8px 0;">
									<?php $bar_value=0; if($total_reviews>0){
									$bar_value=($five_review_total/$total_reviews)*100;
										} ?>
								  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width:<?php echo $bar_value;?>%">
									
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;"><?php echo $five_review_total;?></div>
						</div>
						<div class="pull-left">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">4 <i class="fa fa-star 3x blue-star"></i></div>
							</div>
							<div class="pull-left" style="width:180px;">
								<div class="progress" style="height:9px; margin:8px 0;">
									<?php $bar_value=0; 
									if($total_reviews>0){
									$bar_value=($four_review_total/$total_reviews)*100;
										}
									 ?>
								  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: <?php echo $bar_value;?>%">
									
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;"><?php echo $four_review_total;?></div>
						</div>
						<div class="pull-left">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">3 <i class="fa fa-star 3x blue-star"></i></div>
							</div>
							<div class="pull-left" style="width:180px;">
								<div class="progress" style="height:9px; margin:8px 0;">
									<?php $bar_value=0; 
									if($total_reviews>0){
									$bar_value=($three_review_total/$total_reviews)*100;
										}
									 ?>
								  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: <?php echo $bar_value;?>%">
									
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;"><?php echo $three_review_total;?></div>
						</div>
						<div class="pull-left">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">2 <i class="fa fa-star 3x blue-star"></i></div>
							</div>
							<div class="pull-left" style="width:180px;">
								<div class="progress" style="height:9px; margin:8px 0;">
									<?php $bar_value=0; 
									if($total_reviews>0){
									$bar_value=($two_review_total/$total_reviews)*100;
										}
									?>
								  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: <?php echo $bar_value;?>%">
									
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;"><?php echo $two_review_total;?></div>
						</div>

						<div class="pull-left">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">1 <i class="fa fa-star 3x blue-star"></i></div>
							</div>
							<div class="pull-left" style="width:180px;">
								<div class="progress" style="height:9px; margin:8px 0;">
									<?php $bar_value=0; 
									if($total_reviews>0){
									$bar_value=($one_review_total/$total_reviews)*100;
										}
									 ?>
								  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: <?php echo $bar_value;?>%">
									
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;"><?php echo $one_review_total;?></div>
						</div>
					</div>			
				</div>			
				
				<div class="row">
					<div class="col-sm-12">
						<hr/>
						<div class="review-block">
							<?php
							foreach ( $author_reviews as $review )								
								{	
								$user_review_val=0;
								$review_submitter=get_post_meta($review->ID, 'review_submitter', true); 	
								$user_review_val=get_post_meta($review->ID, 'review_value', true); 		
							?>
							<div class="row">
								<div class="col-sm-3">
									<?php 
									$user_image_path=get_user_meta($review_submitter, 'iv_profile_pic_url',true);
									if($user_image_path==''){
										$user_image_path=wp_iv_directories_URLPATH.'assets/images/Blank-Profile.jpg';
									}
												
									?>
									 <?php	
										   $userreview = get_user_by( 'id', $review_submitter );	   
										   $name_display=get_user_meta($review_submitter,'first_name',true).' '.get_user_meta($review_submitter,'last_name',true);
										   
										   $profile_public=get_option('_iv_directories_profile_public_page');
										   $reg_page_u= get_permalink( $profile_public); 
										   
										  
											 $reg_page_u= $reg_page_u.'?&id='.$review_submitter;
										   ?>
									<a href="<?php echo $reg_page_u;?>"><img src="<?php echo $user_image_path;?>" class="img-rounded" width="60px;"></a>
									<div class="review-block-name">
										
										<a href="<?php echo $reg_page_u;?>">
										 <?php   
										  echo (trim($name_display)!=""? $name_display : $userreview->display_name );
										   ?>
										</a></div>
									<div class="review-block-date"><?php echo date('M d, Y',strtotime($review->post_date)); ?></div>
								</div>
								<div class="col-sm-9">
									<div class="review-block-rate">
																				
										<button type="button" class=" <?php echo ($user_review_val>0?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
										 <i class="fa fa-star <?php echo ($user_review_val>0?'white-star': 'black-star');?>"></i>
										</button>
										<button type="button" class=" <?php echo ($user_review_val>1?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
										 <i class="fa fa-star <?php echo ($user_review_val>1?'white-star': 'black-star');?>"></i>
										</button>
										<button type="button" class=" <?php echo ($user_review_val>2?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
										 <i class="fa fa-star <?php echo ($user_review_val>2?'white-star': 'black-star');?>"></i>
										</button>
										<button type="button" class=" <?php echo ($user_review_val>3?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
										 <i class="fa fa-star <?php echo ($user_review_val>3?'white-star': 'black-star');?>"></i>
										</button>
										<button type="button" class=" <?php echo ($user_review_val>4?'btn-black': 'btn-default btn-grey');?> btn-xs" aria-label="Left Align">
										 <i class="fa fa-star <?php echo ($user_review_val>4?'white-star': 'black-star');?>"></i>
										</button>
										
									</div>
									<div class="review-block-title"><?php echo $review->post_title; ?></div>
									<div class="review-block-description "><?php echo $review->post_content; ?></div>
								</div>
							</div>
							<hr>
							<?php
								}
							?>
						
						</div>
					</div>
				</div>	
					
				<form id="iv_review_form" name="iv_review_form" class="" role="form" onsubmit="return false;">
							<div class="row">
								<div class="col-sm-12">
									<div class="row"><div class="col-sm-12"><h4><?php  _e('Submit your review', 'ivdirectories'); ?></h4></div></div><hr>
									<div class="row">
											<div class="col-sm-3">
												<?php  _e('Subject', 'ivdirectories'); ?>
											</div>	
											<div class="col-sm-9">
												<input type="text" class="form-control" name="review_subject"   value="" placeholder="<?php  _e('Enter review title', 'ivdirectories'); ?>"> 
											</div>	
									</div>	
									<div class="row">
											<div class="col-sm-3">
												<?php  _e('Rating', 'ivdirectories'); ?>
											</div>	
											<div class="col-sm-9">
													<div class="stars">		
														<input class="star star-5" id="star-5" type="radio" name="star" value="5"/>
														<label class="star star-5" for="star-5"></label>
														<input class="star star-4" id="star-4" type="radio" name="star" value="4"/>
														<label class="star star-4" for="star-4"></label>
														<input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
														<label class="star star-3" for="star-3"></label>
														<input class="star star-2" id="star-2" type="radio" name="star" value="2" />
														<label class="star star-2" for="star-2"></label>
														<input class="star star-1" id="star-1" type="radio" name="star" value="1" />
														<label class="star star-1" for="star-1"></label>
													</div>												
											</div>	
									</div>	
									<div class="row">
											<div class="col-sm-3">
												<?php  _e('Comments', 'ivdirectories'); ?>
											</div>	
											<div class="col-sm-9">
												<textarea class="form-control" cols="50"  name="review_comment" id="review_comment" placeholder="<?php  _e('Enter review comments', 'ivdirectories'); ?>" rows="5"></textarea>
											</div>	
									</div>	
									
										<div class="row">
											<div class="col-sm-12 text-right">
													<button type="button" class="btn btn-default " onclick="return iv_submit_review();">
														<?php  _e('Submit', 'ivdirectories'); ?> 
													</button>
													<input type="hidden" name="listingid" id="listingid" value="<?php echo $listingid; ?>">
													<div id="rmessage"></div>
											</div>												
										</div>	
								</div>
							</div>	
						
					 
					 </form>
					
			
</div>


<script>
	function iv_submit_review(){
		var isLogged ="<?php echo get_current_user_id();?>";

        if (isLogged=="0") {
             alert("<?php _e('Please login to add review','ivdirectories'); ?>");
        } else {
			var form = jQuery("#iv_review_form");
			
			if (jQuery.trim(jQuery("#review_comment", form).val()) == "") {
				  alert("<?php _e('Please put your comment','ivdirectories'); ?>");
			} else {
			 	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
			   	var loader_image = '<img style="width:100px" src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
			   	jQuery('#rmessage').html(loader_image);
			   	var search_params={
				 	"action"  :  "iv_directories_save_user_review",
				 	"form_data": jQuery("#iv_review_form").serialize(),
			   	};
			   	jQuery.ajax({
					url : ajaxurl,
				 	dataType : "json",
				 	type : "post",
				 	data : search_params,
				 	success : function(response){
				  		jQuery('#rmessage').html('<div class="col-sm-7 alert alert-info alert-dismissable"><a class="panel-close close" data-dismiss="alert">x</a>'+response.msg +'.</div>');
				  		jQuery("#iv_review_form")[0].reset();
					}
			  	});
			}	  
		}
	}
</script>
<style>
		

.btn-black{
 background-color: #000000;

}
			
div.stars {
  width: 100%;
  display: inline-block;
}

input.star { display: none; }

label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}

input.star:checked ~ label.star:before {
  content: '\f005';
  color: #FD4;
  transition: all .25s;
}

input.star-5:checked ~ label.star:before {
  color: #FE7;
  text-shadow: 0 0 20px #952;
}

input.star-1:checked ~ label.star:before { color: #F62; }

label.star:hover { transform: rotate(-15deg) scale(1.3); }

label.star:before {
  content: '\f006';
  font-family: FontAwesome;
}

.stars
{
    margin: 0px 0;
    font-size: 24px;
    color: #d17581;
}	
.black-star{
	color: #2c3e50;
}	

.white-star {
	color: #f4f7f8;
}
.bootstrap-wrapper .btn-warning {    
    border-color: #0099e5!important;
}	
	
.btn-grey{
    background-color:#D8D8D8!important;
	color:#FFF!important;
}
.rating-block{
	background-color:#FAFAFA!important;
	border:1px solid #EFEFEF!important;
	padding:15px 15px 20px 15px!important;
	border-radius:3px!important;
}
.bold{
	font-weight:700!important;
	font-size:18px!important;
}
.padding-bottom-7{
	padding-bottom:7px!important;
}

.review-block{
	background-color:#FAFAFA!important;
	border:1px solid #EFEFEF!important;
	padding:15px!important;
	border-radius:3px!important;
	margin-bottom:15px!important;
}
.review-block-name{
	font-size:12px;
	margin:10px 0;
}
.review-block-date{
	font-size:12px;
}
.review-block-rate{
	font-size:13px;
	margin-bottom:15px;
}
.review-block-title{
	font-size:15px;
	font-weight:700;
	margin-bottom:10px;
}
.review-block-description{
	font-size:13px;
}
</style>	
