<?php
global $post,$wpdb,$tag;
wp_enqueue_style('iv_directories-style-1109', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap.css');
wp_enqueue_style('iv_directories-style-110', wp_iv_directories_URLPATH . 'admin/files/css/listing_style_4.css');
wp_enqueue_style('iv_directories-style-64', wp_iv_directories_URLPATH . 'assets/cube/css/cubeportfolio.min.css');
wp_enqueue_style('iv_directories-style-111', wp_iv_directories_URLPATH . 'admin/files/css/new-version.css');
wp_enqueue_script('iv_directories-script-12', wp_iv_directories_URLPATH . 'admin/files/js/markerclusterer.js');

wp_enqueue_style('iv_directories-css-queryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css');
wp_enqueue_script('iv_directories-jqueryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');

$directory_url=get_option('_iv_directory_url');

if($directory_url==""){$directory_url='directories';}
$current_post_type=$directory_url;
$form_action='';

if ( is_front_page() ) {
  $form_action='action="'.get_post_type_archive_link($current_post_type).'"';
}

$dir_map_api=get_option('_dir_map_api');

if($dir_map_api==""){$dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';}

if(isset($_POST['latitude'])){
	$ins_lat=$_POST['latitude'];
}else{
	$ins_lat='37.4419';
}

if(isset($_POST['longitude'])){
	$ins_lng=$_POST['longitude'];
}else{
	$ins_lng='-122.1419';
}

$search_show=0;
$map_show=0;

$dir_searchbar_show=get_option('_dir_searchbar_show');

if($dir_searchbar_show=="yes"){$search_show=1;}
$dir_map_show=get_option('_dir_map_show');

if($dir_map_show=="yes"){$map_show=1;}

	$dirs_data =array();
	$tag_arr= array();

	$args = array(
		'post_type' => $directory_url, // enter your custom post type
		'post_status' => 'publish',
	);

	$lat='';$long='';$keyword_post='';$address='';$postcats ='';$selected='';

	// Add new shortcode only category
	if(isset($atts['category']) and $atts['category']!="" ){
			$postcats = $atts['category'];
			$args[$directory_url.'-category']=$postcats;
			//$args['posts_per_page']='9999';

	}

	if(get_query_var($directory_url.'-category')!=''){
			$postcats = get_query_var($directory_url.'-category');
			$args[$directory_url.'-category']=$postcats;
			$selected=$postcats;
			$search_show=1;

	}

	if( isset($_POST[$directory_url.'-category'])){
		
		if($_POST[$directory_url.'-category']!=''){
			$postcats = $_POST[$directory_url.'-category'];
			$args[$directory_url.'-category']=$postcats;
			$selected=$postcats;
			$search_show=1;
		}
	}


	$radius=get_option('_iv_radius');
	
	if( isset($_REQUEST['range_value'])){
		$radius = $_REQUEST['range_value'];
	}
	
	if($radius==''){$radius='50';}

	if( isset($_REQUEST['address'])) {
		
		if($_POST['address']!="") {
			$lat =  $_REQUEST['latitude'];
			$long = $_REQUEST['longitude'];
			$address=trim($_REQUEST['address']);

				if($lat=='' || $long=='') {
					$latitude='';$longitude='';

					$prepAddr = str_replace(' ','+',$address);
					$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
					$output= json_decode($geocode);
					if(isset( $output->results[0]->geometry->location->lat)){
						$latitude = $output->results[0]->geometry->location->lat;
					}
					if(isset($output->results[0]->geometry->location->lng)){
						$longitude = $output->results[0]->geometry->location->lng;
					}
					$lat=$latitude;
					$long=$longitude;
					$args['distance']='50';
				} else {
					$args['distance']=$radius;
				}

			$args['lat']=$lat;
			$args['lng']=$long;

			$search_show=1;

		}
	}

	if( isset($_REQUEST['keyword'])) {
		if($_REQUEST['keyword']!=""){
			$args['s']= $_REQUEST['keyword'];
			$keyword_post=$_REQUEST['keyword'];
			$search_show=1;
		}
	}

	if( isset($tag)){
		if($tag!=""){
			if(!isset($_REQUEST['keyword'])){
				$args['tag']= $tag;
			}
		}
	}
	if( isset($_REQUEST['tag_arr'])){
		if($_REQUEST['tag_arr']!=""){
			$tag_arr= $_REQUEST['tag_arr'];
			//$tag_arr= get_query_var('tag_arr');
			$tags_string= implode("+", $tag_arr);
			$args['tag']= $tags_string;
		}
	}
		// Meta Query***********************
		$city_mq ='';
		if(isset($_REQUEST['dir_city']) AND $_REQUEST['dir_city']!=''){
				$city_mq = array(
				'relation' => 'AND',
					array(
						'key'     => 'city',
						'value'   => $_REQUEST['dir_city'],
						'compare' => 'LIKE'
					),
				);
		}
		$country_mq='';
		if(isset($_REQUEST['dir_country']) AND $_REQUEST['dir_country']!=''){
			$country_mq = array(
				'relation' => 'AND',
					array(
						'key'     => 'country',
						'value'   => $_REQUEST['dir_country'],
						'compare' => 'LIKE'
					),
				);
		}
		$zip_mq='';
		if(isset($_REQUEST['zipcode']) AND $_REQUEST['zipcode']!=''){
			$zip_mq = array(
				'relation' => 'AND',
					array(
						'key'     => 'postcode',
						'value'   => $_REQUEST['zipcode'],
						'compare' => 'LIKE'
					),
				);
		}


		// For featrue listing***********
		$feature_listing_all =array();
		$feature_listing_all =$args;




		$args['meta_query'] = array(
			$city_mq, $country_mq, $zip_mq,
		);

		if(isset($atts['category']) and $atts['category']!="" ){
			$args['posts_per_page']='999';
		}else{
			$args['posts_per_page']=get_option('posts_per_page');
		}



	$the_query = new WP_GeoQuery( $args );


$main_class = new wp_iv_directories;

?>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,700%7CRoboto:400,500,700%7CRoboto+Condensed%7CLato:400,700" rel="stylesheet" type="text/css">

<!-- Map**************-->
<div id="top-map" style="<?php echo ($map_show==1 ? '': 'display: none;'); ?>">
  <div id="map"  style="width:100%; height: 450px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden;"> </div>
</div>

<div class="wrap-pro">
  <div class="bootstrap-wrapper">
    <div id="top-search" class="navbar-default navbar dir-style-search" style="<?php //echo ($search_show==1 ? '': 'display: none;'); ?>">
      <div class="text-center dir-style-4">
        <form class="form-inline" id="dirprosearch" name="dirprosearch" method="POST" <?php echo $form_action;?> onkeypress="return event.keyCode != 13;">
          <?php
						$dir_search_keyword=get_option('_dir_search_keyword');
						if($dir_search_keyword==""){$dir_search_keyword='yes';}
						if($dir_search_keyword=='yes'){
						?>
          <div class="form-group">
            <input type="text" class="form-control " id="keyword" name="keyword"  placeholder="<?php _e( 'Keyword', 'ivdirectories' ); ?>" value="<?php echo $keyword_post; ?>">
            <?php $pos = $main_class->get_unique_keyword_values('keyword',$current_post_type);
			?>
			<script>
				jQuery(function() {
				var availableTags = [ "<?php echo  implode('","',$pos); ?>" ];
				jQuery( "#keyword" ).autocomplete({source: availableTags});
			  });
			</script>
          </div>
          <?php
			}
						$dir_search_city=get_option('_dir_search_city');
						if($dir_search_city==""){$dir_search_city='yes';}
						if($dir_search_city=='yes'){
						// City
						$args_citys = array(
							'post_type'  => $current_post_type,
							'posts_per_page' => -1,
							'meta_query' => array(
								array(
									'key'     => 'city',
									'orderby' => 'meta_value',
									'order' => 'ASC',
								),

							),
						);
						$citys = new WP_Query( $args_citys );
						$citys_all = $citys->posts;
						$get_cityies =array();
						foreach ( $citys_all as $term ) {
							$new_city="";
							$new_city=ucfirst(trim(get_post_meta($term->ID,'city',true)));
							if (!in_array($new_city, $get_cityies)) {
								$get_cityies[]=ucfirst($new_city);

							}
						}

					// City


						?>
          <div class="form-group" >
            <select name="dir_city"  id="dir_city" class="form-control" >
              <option   value="">
              <?php esc_html_e('Choose a City   ','ivdirectories'); ?>
              </option>
              <?php
						$selected_city= (isset($_REQUEST['dir_city'])?$_REQUEST['dir_city']:'' );
						if(count($get_cityies)) {
								asort($get_cityies);
						  foreach($get_cityies as $row1) {
							  if($row1!=''){
							  ?>
								<option   value="<?php echo $row1; ?>" <?php echo ($selected_city==$row1?'selected':''); ?>><?php echo $row1; ?></option>
							<?php
							}

							}

						}

						?>
            </select>
          </div>
          <?php
							}
						?>
          <?php
						$dir_search_country=get_option('_dir_search_country');
						if($dir_search_country==""){$dir_search_country='yes';}
						if($dir_search_country=='yes'){
						// Country
							$args_country = array(
								'post_type'  => $current_post_type,
								'posts_per_page' => -1,
								'meta_query' => array(
									array(
										'key'     => 'country',
										'orderby' => 'meta_value',
										'order' => 'ASC',
									),

								),
							);
							$country = new WP_Query( $args_country );
							$country_all = $country->posts;
							$get_country =array();
							foreach ( $country_all as $term ) {
								$new_country="";
								$new_country=ucfirst(trim(get_post_meta($term->ID,'country',true)));

								if (!in_array($new_country, $get_country)) {
									$get_country[]=ucfirst($new_country);

								}
							}

							//Country
							?>
          <div class="form-group" >
            <select name="dir_country"  id="dir_country" class="form-control" >
              <option   value="">
              <?php esc_html_e('Country  ','ivdirectories'); ?>
              </option>
              <?php

						$selected_country= (isset($_REQUEST['dir_country'])?$_REQUEST['dir_country']:'' );
										if(count($get_country)) {
											asort($get_country);
										  foreach($get_country as $row1) {
											  if($row1!=''){
											  ?>
				<option   value="<?php echo $row1; ?>" <?php echo ($selected_country==$row1?'selected':''); ?> ><?php echo $row1; ?></option>
              <?php
											}

											}

										}

										?>
            </select>
          </div>
          <?php
							}
						?>
          <?php
						 $_dir_search_zipcode=get_option('_dir_search_zipcode');
						if($_dir_search_zipcode==""){$dir_search_location='yes';}
						if($_dir_search_zipcode=='yes'){
							$zipcode=(isset($_REQUEST['zipcode'])?$_REQUEST['zipcode']:'' )
						?>
          <div class="form-group" >
            <input type="text"  id="zipcode" name="zipcode" class="form-control" placeholder="<?php _e( 'Zipcode', 'ivdirectories' ); ?>"
								value="<?php echo trim($zipcode); ?>">
            <?php $pos = $main_class->get_unique_post_meta_values('postcode',$current_post_type);
									//print_r($pos);
								?>
            <script>
								  jQuery(function() {
									var availableTags = [ "<?php echo  implode('","',$pos); ?>" ];
									jQuery( "#zipcode" ).autocomplete({source: availableTags});
								  });
								</script>
          </div>
          <?php
						}
						?>
          <div class="form-group" >
            <?php
								echo '<select name="'.$directory_url.'-category" class="form-control">';
								echo'	<option selected="'.$selected.'" value="">'.__('Any Category   ','ivdirectories').'</option>';


										if( isset($_POST['submit'])){
											$selected = $_POST[$directory_url.'-category'];
										}
											//directories
											$taxonomy = $directory_url.'-category';
											$args = array(
												'orderby'           => 'name',
												'order'             => 'ASC',
												'hide_empty'        => true,
												'exclude'           => array(),
												'exclude_tree'      => array(),
												'include'           => array(),
												'number'            => '',
												'fields'            => 'all',
												'slug'              => '',
												'parent'            => '0',
												'hierarchical'      => true,
												'child_of'          => 0,
												'childless'         => false,
												'get'               => '',

											);
								$terms = get_terms($taxonomy,$args); // Get all terms of a taxonomy
								if ( $terms && !is_wp_error( $terms ) ) :
									$i=0;
									foreach ( $terms as $term_parent ) {  ?>
            <?php

											echo '<option  value="'.$term_parent->slug.'" '.($selected==$term_parent->slug?'selected':'' ).'><strong>'.$term_parent->name.'<strong></option>';
											?>
            <?php

												$args2 = array(
													'type'                     => $directory_url,
													'parent'                   => $term_parent->term_id,
													'orderby'                  => 'name',
													'order'                    => 'ASC',
													'hide_empty'               => 1,
													'hierarchical'             => 1,
													'exclude'                  => '',
													'include'                  => '',
													'number'                   => '',
													'taxonomy'                 => $directory_url.'-category',
													'pad_counts'               => false

												);
												$categories = get_categories( $args2 );
												if ( $categories && !is_wp_error( $categories ) ) :


													foreach ( $categories as $term ) {
														echo '<option  value="'.$term->slug.'" '.($selected==$term->slug?'selected':'' ).'>-'.$term->name.'</option>';
													}

												endif;
												?>
            <?php
										$i++;
									}
								endif;
									echo '</select>';
								?>
          </div>
          <?php
						$dir_search_location=get_option('_dir_search_location');
						if($dir_search_location==""){$dir_search_location='yes';}
						if($dir_search_location=='yes'){
						?>
          <div class="form-group" >
            <input type="text" class="form-control " id="address" name="address"  placeholder="<?php _e( 'Location', 'ivdirectories' ); ?>"
								value="<?php echo trim($address); ?>">
            <input type="hidden" id="latitude" name="latitude" placeholder="Latitude" value="<?php echo $lat; ?>" >
            <input type="hidden" id="longitude" name="longitude" placeholder="Longitude"  value="<?php echo $long; ?>">
          </div>
          <?php
						}
						$dir_f_search_radius=get_option('_dir_search_location');
							if($dir_f_search_radius==""){$dir_f_search_radius='yes';}
							if($dir_f_search_radius=='yes'){
					  ?>
          <?php
							$dir_search_redius=get_option('_dir_search_redius');
							if($dir_search_redius==""){$dir_search_redius='Km';}
							?>
          <div class="form-group range-txt">
            <?php _e( 'Radius', 'ivdirectories' ); ?>
            : <span id="rvalue"><?php echo $radius;?></span><?php echo ' '.$dir_search_redius; ?> </div>
          <div class="form-group range-filter">
            <div class="range range-success">
              <input type="range" name="range" id="range" min="1" max="1000" value="<?php echo $radius;?>" onchange="range.value=value">
              <input type="hidden" name="range_value" id="range_value" value="<?php echo $radius; ?>" >
            </div>
          </div>
          <?php
						}
						?>
			<button type="submit" id="submit" name="submit"  class="btn">
          <?php _e('Search','ivdirectories'); ?>
          </button>
        </form>
      </div>
    </div>
  </div>

  <div class="clearfix" style="margin-top:20px">
   <div id="js-sort-juicy-projects" class="cbp-l-sort cbp-l-filters-right">
		<div class="cbp-l-dropdown">
			<div class="cbp-l-dropdown-wrap">
				<div class="cbp-l-dropdown-header"><?php _e( 'Date', 'ivdirectories' ); ?></div>
				<div class="cbp-l-dropdown-list">
					<div class="cbp-l-dropdown-item cbp-sort-item cbp-l-dropdown-item--active" data-sort=".cbp-l-grid-projects-date" data-sortBy="int:desc"   ><?php _e( 'Date', 'ivdirectories' ); ?></div>

					<div class="cbp-l-dropdown-item cbp-sort-item" data-sort=".cbp-l-grid-projects-title" data-sortBy="string:asc" ><?php _e( 'Title', 'ivdirectories' ); ?></div>
					 <?php
					$dir_single_review_show=get_option('_dir_single_review_show');
					if($dir_single_review_show==""){$dir_single_review_show='yes';}
					if($dir_single_review_show=='yes'){
					?>
					<div class="cbp-l-dropdown-item cbp-sort-item" data-sort=".cbp-l-grid-projects-review" data-sortBy="string:desc"><?php _e( 'Review', 'ivdirectories' ); ?></div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
		<div class="cbp-l-direction cbp-l-direction--second">
			<div class="cbp-l-direction-item cbp-sort-item" data-sortBy="string:asc"></div>
			<div class="cbp-l-direction-item cbp-sort-item" data-sortBy="string:desc"></div>
		</div>
	</div>


    <div id="js-filters-meet-the-team" class="cbp-l-filters-button  cbp-l-filters-alignLeft" >

    <?php
	$active_filter=get_option('active_filter');
	if($active_filter==""){$active_filter='category';}


	if($active_filter=="category"){
		if($postcats==''){	?>
      <div data-filter="*" class="cbp-filter-item">
        <?php _e('Show All', 'ivdirectories' ); ?>
      </div>
      <?php

					$argscat = array(
						'child_of'            => 0,
						'parent'              => 0,
						'current_category'    => 0,
						'depth'               => 0,
						'type'                     => $directory_url,
						'orderby'                  => 'name',
						'order'                    => 'ASC',
						'hide_empty'               => true,
						'hierarchical'             => 1,
						'exclude'                  => '',
						'include'                  => '',
						'number'                   => '',
						'taxonomy'                 => $directory_url.'-category',
						'pad_counts'               => false

					);
					$categories = get_categories( $argscat );

					if ( $categories && !is_wp_error( $categories ) ) :
						foreach ( $categories as $term ) {
							echo '<div data-filter=".'.$term->slug.'" class="cbp-filter-item"> '.$term->name.' <div class="cbp-filter-counter"></div></div>';

						}
					endif;
					?>


			<?php
			}
			if($postcats!=''){ ?>
      <div data-filter="" class="cbp-filter-item"><a href="<?php echo get_post_type_archive_link( $directory_url) ; ?>">
        <?php _e('Show All', 'ivdirectories' ); ?>
        </a> </div>
      <?php
				 $term = get_term_by('slug', $postcats, $directory_url.'-category');
				 $name = (isset($term->name)? $term->name: $postcats);
				echo '<div data-filter=".'.$postcats.'"  class="cbp-filter-item-active cbp-filter-item"> '.$name.' <div class="cbp-filter-counter"></div></div>';


			}
	}
if($active_filter=="tag"){
	?>
	<div data-filter="*" class="cbp-filter-item">
	<?php _e('Show All', 'ivdirectories' ); ?>
	</div>
	<?php
	$args2 = array(
		'type'                     => $directory_url,
		//'parent'                   => $term_parent->term_id,
		'orderby'                  => 'name',
		'order'                    => 'ASC',
		'hide_empty'               => true,
		'hierarchical'             => 1,
		'exclude'                  => '',
		'include'                  => '',
		'number'                   => '',
		'taxonomy'                 => $directory_url.'_tag',
		'pad_counts'               => false

	);

	$main_tag = get_categories( $args2 );
	//$tags_all= wp_get_object_terms( $post_edit->ID,  $post_type.'_tag');

	if ( $main_tag && !is_wp_error( $main_tag ) ) :
		foreach ( $main_tag as $term_m ) {
			$checked='';
			echo '<div data-filter=".'.$term_m->slug.'" class="cbp-filter-item"> '.$term_m->name.' <div class="cbp-filter-counter"></div></div>';
	}
	endif;
}

			?>

    </div>
  </div>

  <!-- Item Filter Section -->
  <div class="direc-item">
    <div id="js-grid-meet-the-team" class="cbp cbp-l-grid-team" >

      <?php
		include( wp_iv_directories_template. 'directories/archive_feature_listing.php');

	$i=1;
	$dir_popup=get_option('_dir_popup');
	if($dir_popup==""){$dir_popup='yes';}
	 if ( $the_query->have_posts() ) :

	while ( $the_query->have_posts() ) : $the_query->the_post();
				$id = get_the_ID();
		if(get_post_meta($id, 'dirpro_featured', true)!='featured'){

				$gallery_ids=get_post_meta($id ,'image_gallery_ids',true);
				$gallery_ids_array = array_filter(explode(",", $gallery_ids));

				$dir_data['link']=get_post_permalink();
				$dir_data['title']=$post->post_title;
				$dir_data['lat']=get_post_meta($id,'latitude',true);
				$dir_data['lng']=get_post_meta($id,'longitude',true);
				if(get_post_meta($id,'latitude',true)!=''){$ins_lat=get_post_meta($id,'latitude',true);}
				if(get_post_meta($id,'longitude',true)!=''){$ins_lng=get_post_meta($id,'longitude',true);}
				$dir_data['address']=get_post_meta($id,'address',true);
				$dir_data['image']= '';
				$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'thumbnail' );
				if($feature_image[0]!=""){
					$dir_data['image']=  $feature_image[0];
				}
				$dir_data['marker_icon']=wp_iv_directories_URLPATH."/assets/images/map-marker/map-marker.png";
				$currentCategoryId='';
				$terms =get_the_terms($id, $directory_url."-category");
				if($terms!=""){
					foreach ($terms as $termid) {
						if(isset($termid->term_id)){
							 $currentCategoryId= $termid->term_id;
						}
					}
				}
				$marker = get_option('_cat_map_marker_'.$currentCategoryId,true);
				if($marker!=''){
					$image_attributes = wp_get_attachment_image_src( $marker ); // returns an array
					if( $image_attributes ) {

						$dir_data['marker_icon']= $image_attributes[0];
					}
				}
				if($dir_data['lat']!='' AND $dir_data['lng']!='' ){
					array_push( $dirs_data, $dir_data );
				}



					$feature_img='';
					if(has_post_thumbnail()){
						$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' );
						if($feature_image[0]!=""){
							$feature_img =$feature_image[0];
						}
					}else{
						$feature_img= wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";

					}
					if($active_filter=="tag"){
						$currentCategory=wp_get_object_terms( $id, $directory_url.'_tag');
					}else{
						$currentCategory=wp_get_object_terms( $id, $directory_url.'-category');
					}

					$cat_link='';$cat_name='';$cat_slug='';
					if(isset($currentCategory[0]->slug)){
						$cat_slug = $currentCategory[0]->slug;
						$cat_name = $currentCategory[0]->name;
						$cc=0;
						foreach($currentCategory as $c){
								if($cc==0){
									$cat_name =$c->name;
									$cat_slug =$c->slug;
								}else{
									$cat_name = $cat_name .', '.$c->name;
									$cat_slug = $cat_slug .' '.$c->slug;
								}
							$cc++;
						}
					}

					$currentCategory=wp_get_object_terms( $id, $directory_url.'-category');
					$cat_name2='';
					if(isset($currentCategory[0]->slug)){
						$cat_name2 = $currentCategory[0]->name;
						$cc=0;
						foreach($currentCategory as $c){
								if($cc==0){
									$cat_name2 =$c->name;
								}else{
									$cat_name2 = $cat_name2 .', '.$c->name;
								}
							$cc++;
						}
					}


					// VIP image***************
					$vip_image='';	$have_vip_badge='';
					$post_author_id= $post->post_author;
					$author_package_id=get_user_meta($post_author_id, 'iv_directories_package_id', true);
					$have_vip_badge= get_post_meta($author_package_id,'iv_directories_package_vip_badge',true);
					$exprie_date= strtotime (get_user_meta($post_author_id, 'iv_directories_exprie_date', true));
					$current_date=time();
					if($have_vip_badge=='yes'){
						if($exprie_date >= $current_date){
							if(get_option('vip_image_attachment_id')!=""){
									$vip_img= wp_get_attachment_image_src(get_option('vip_image_attachment_id'));
									if(isset($vip_img[0])){
										$vip_image='<img style="width:30px;"   src="'.$vip_img[0] .'">';
									}
							}else{
								$vip_image='<img style="width:35px;"   src="'. wp_iv_directories_URLPATH."/assets/images/vipicon.png".'">';
							}

						}
					}



			if ( is_user_logged_in() ) {

			    global $current_user;
			    $current_user = wp_get_current_user();
			    
			    if(get_user_meta($current_user->ID,'address',true)!='') {

			      if(get_post_meta($id,'address',true)!='') {

			          $dir_map_api=get_option('_dir_map_api');
			          if($dir_map_api=="") {
			            $dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';
			          }

			          //Post
			          $post_add = get_post_meta($id,'address',true);

			          if(get_post_meta($id,'latitude',true)!=''){$ins_lat=get_post_meta($id,'latitude',true);}
			          if(get_post_meta($id,'longitude',true)!=''){$ins_lng=get_post_meta($id,'longitude',true);}

			          if( get_user_meta($current_user->ID,'latitude',true)!=''){ $user_lat = get_user_meta($current_user->ID,'latitude',true);}
			          if( get_user_meta($current_user->ID,'longitude',true)!=''){ $user_lng = get_user_meta($current_user->ID,'longitude',true);}

			          if(($ins_lat == $user_lat) && ($ins_lng == $user_lng)) {
			              $discal = "0 Km";

			          } else {
			              $theta = $ins_lng - $user_lng;
			              $dist = sin(deg2rad($ins_lat)) * sin(deg2rad($user_lat)) +  cos(deg2rad($ins_lat)) * cos(deg2rad($user_lat)) * cos(deg2rad($theta));
			              $dist = acos($dist);
			              $dist = rad2deg($dist);
			              $miles = $dist * 60 * 1.1515;
			              $kmdistance = $miles * 1.609344;
			              $kmdistance = round($kmdistance,2);
			              $discal = $kmdistance." Km";
			          }

			      } else {
			        $discal = " ";
			      }
			    } else {
			      $discal = " ";
			    }
			  } else {
			      $discal = " ";
			  }




		?>


					<div class="PropertyBox item-inside cbp-item <?php echo esc_attr($cat_slug); ?>">
                        <figure>
                            <span class="verified"><img src="<?php echo get_template_directory_uri();?>/images/Verfied.png">verified</span>
                            <!--<span class="Premium">Premium</span>-->
                            <a href="<?php echo get_the_permalink($id); ?>"><img src="<?php echo $feature_img;?>">
                            <aside>
                                <h4><?php echo $post->post_title;?></h4>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo get_post_meta($id,'city',true);?> <span> <?php echo $discal;?> </span></p>
                            </aside>
                            </a>
                        </figure>
                        <figcaption>
                    	<?php
							$dir_single_review_show=get_option('_dir_single_review_show');
							if($dir_single_review_show==""){$dir_single_review_show='yes';}
							if($dir_single_review_show=='yes'){

							$total_reviews_point = $wpdb->get_col("SELECT SUM(pm.meta_value) FROM {$wpdb->postmeta} pm
							 INNER JOIN {$wpdb->posts} p ON p.ID = pm.post_id
							 WHERE pm.meta_key = 'review_value'
							 AND p.post_status = 'publish'
							 AND p.post_type = 'dirpro_review' AND p.post_author = '".$id."'");


							$argsreviw = array( 'post_type' => 'dirpro_review','author'=>$id,'post_status'=>'publish' );
							$ratings = new WP_Query( $argsreviw );

							$total_review_post = $ratings->post_count;

							$avg_review=0;
							
							if(isset($total_reviews_point[0])){
								$avg_review= (int)$total_reviews_point[0]/(int)$total_review_post;
								$avg_review = (int)$avg_review;
							}
						?>
                            <h5>
                                <i class="fa fa-star <?php echo ($avg_review>0?'black-star': 'white-star ');?>"></i>
								<i class="fa fa-star <?php echo ($avg_review>=2?'black-star': 'white-star');?>"></i>
								<i class="fa fa-star <?php echo ($avg_review>=3?'black-star': 'white-star');?>"></i>
								<i class="fa fa-star <?php echo ($avg_review>=4?'black-star': 'white-star');?>"></i>
								<i class="fa fa-star <?php echo ($avg_review>=5?'black-star': 'white-star');?>"></i>
                                <?php echo $total_review_post;?>
                                <a href="#">(<?php echo $avg_review;?> review)</a>
                            </h5>
                        <?php
							}
						?>
                            <p><?php the_excerpt();?></p>
                            <ul>
                                <li><a href="<?php echo get_the_permalink($id); ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                                <li><a href="<?php echo get_the_permalink($id); ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                                <li><a href="<?php echo get_the_permalink($id); ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                <li><a href="<?php echo get_the_permalink($id); ?>"><i class="fa fa-picture-o" aria-hidden="true"></i></a></li>
                                <li><a href="<?php echo get_the_permalink($id); ?>"><i class="fa fa-globe" aria-hidden="true"></i></a></li>
                            </ul>
                                <div class="clear"></div>
                        </figcaption>
                    </div>


			<?php
			$i++;
		}

	endwhile;

	?>

	<?php
				$dirs_json ='';
				if(!empty($dirs_data)){
					$dirs_json =json_encode($dirs_data);
				}
				?>


      <?php else :
			$dirs_json=''; ?>
      <?php _e( 'Sorry, no posts matched your criteria.','ivdirectories' ); ?>
      <?php endif; ?>


    </div>
  </div>
  <?php
    if ( !$the_query->have_posts() ){
	_e( 'Sorry, no posts matched your criteria.','ivdirectories' );

	} ?>
	<!--
	<div class="row ">
		<div id="dirpro_loadmore"></div>
		<div id="loadmore_button">
			<button type="button" onclick="wpdirp_loadmore();"  class="btn"><?php _e('Load More','ivdirectories'); ?></button>
		</div>
	</div>
	-->
	<?php
	if(isset($atts['category']) and $atts['category']!="" ){

		}else{ ?>
			<div id="loadmore_button" onclick="wpdirp_loadmore();"  class="cbp-l-loadMore-button" >
				<div id="dirpro_loadmore"></div>
				<a  class="cbp-l-loadMore-link" rel="nofollow">
					<span class="cbp-l-loadMore-defaultText " id="">
						<?php _e('Load More','ivdirectories'); ?>
					</span>
				</a>
			</div>

	<?php
		}
	?>


  <!--END .navigation-links-->
</div>

<?php
wp_enqueue_script('iv_directories-ar-script-23', wp_iv_directories_URLPATH . 'assets/cube/js/jquery.cubeportfolio.min.js');
wp_enqueue_script('iv_directories-ar-script-102', wp_iv_directories_URLPATH . 'assets/cube/js/meet-team.js');
?>
<?php

$dir_map_zoom=get_option('_dir_map_zoom');
if($dir_map_zoom==""){$dir_map_zoom='7';}
?>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $dir_map_api;?>'></script>
<script type="text/javascript">
var paged =1;
var center = new google.maps.LatLng('<?php echo $ins_lat; ?>', '<?php echo $ins_lng; ?>');
var map = new google.maps.Map(document.getElementById('map'), {
	zoom: <?php echo $dir_map_zoom; ?>,
	center: center,
	mapTypeId: google.maps.MapTypeId.ROADMAP
});
				function initialize() {
					//var center = new google.maps.LatLng('<?php echo $ins_lat; ?>', '<?php echo $ins_lng; ?>');
					//var center = new google.maps.LatLng(49, 2.56);
					/*
					var map = new google.maps.Map(document.getElementById('map'), {
						zoom: <?php echo $dir_map_zoom; ?>,
						center: center,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					*/
					var markers = [];
					var infowindow = new google.maps.InfoWindow();
					var dirs ='';
					var min = .999999;
					var max = 1.000002;
					<?php echo ($dirs_json!=''? 'var dirs ='.$dirs_json:''); ?>;
					if(dirs!=''){
					 for (i = 0; i < dirs.length; i++) {
						//for(var i=0;i<5;i++){
							//console.log(dirs[i);
							var new_lat= dirs[i].lat  * (Math.random() * (max - min) + min);
							var new_lng= dirs[i].lng  * (Math.random() * (max - min) + min);
							var latLng = new google.maps.LatLng(new_lat,new_lng);
							//var latLng = new google.maps.LatLng(dirs[i].lat,dirs[i].lng);
							var marker = new google.maps.Marker({
								position: latLng,
								map: map,
								icon: dirs[i].marker_icon,
							});
							markers.push(marker);
								 google.maps.event.addListener(marker, 'click', (function(marker, i) {
									return function() {
											//infowindow.setContent('<div id="map-marker-info " ><a href="'+dirs[i].link +'">'+dirs[i].image+'<h5>'+ dirs[i].title //+'</h5><span class="address">'+dirs[i].address+'</span></a></div>');
											infowindow.setContent('<div id="map-marker-info" style="overflow: auto; cursor: default; clear: both; position: relative; border-radius: 4px; padding: 15px; border-color: rgb(255, 255, 255); border-style: solid; background-color: rgb(255, 255, 255); border-width: 1px; width: 275px; height: 130px;"><div style="overflow: hidden;" class="map-marker-info"><a  style="text-decoration: none;" href="'+dirs[i].link +'">	<span style="background-image: url('+dirs[i].image+')" class="list-cover has-image"></span><span class="address"><strong>'+dirs[i].title +'</strong></span> <span class="address" style="margin-top:15px">'+dirs[i].address+'</span></a></div></div>');
										infowindow.open(map, marker);
									}
								})(marker, i));
						}
					}
					var markerCluster = new MarkerClusterer(map, markers);

				}
				function cs_toggle_street_view(btn) {
				  var toggle = panorama.getVisible();
				  if (toggle == false) {
					if(btn == 'streetview'){
					  panorama.setVisible(true);
					}
				  } else {
					if(btn == 'mapview'){
					  panorama.setVisible(false);
					}
				  }
				}
                google.maps.event.addDomListener(window, 'load', initialize);

				//google.maps.event.trigger(map, 'resize');
					jQuery("#search_toggle_div").on('click', function(e) {
						setTimeout(function(){
								initialize();
								google.maps.event.trigger(map, 'resize');
						},500)
					});

	function wpdirp_loadmore(){

		paged = paged+1;
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var loader_image = '<img src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
		jQuery('#dirpro_loadmore').html(loader_image);
		var search_params={
			"action"  : 	"iv_directories_loadmore",
			"form_data":	jQuery("#dirprosearch").serialize(),
			"paged": paged,
		};

		jQuery.ajax({
			url : ajaxurl,
			dataType : "json",
			type : "post",
			data : search_params,
			success : function(response){
				jQuery("#js-grid-meet-the-team").cubeportfolio('append', response.data);
				jQuery('#dirpro_loadmore').html('');

					var markers = [];
					var infowindow = new google.maps.InfoWindow();
					var dirs ='';
					var min = .999999;
					var max = 1.000002;
					if(response.dirs_json!=''){
					 for (i = 0; i < response.dirs_json.length; i++) {
							var new_lat= response.dirs_json[i].lat  * (Math.random() * (max - min) + min);
							var new_lng= response.dirs_json[i].lng  * (Math.random() * (max - min) + min);
							var latLng = new google.maps.LatLng(new_lat,new_lng);
							var marker = new google.maps.Marker({
								position: latLng,
								animation: google.maps.Animation.DROP,
								map: map,
								icon: response.dirs_json[i].marker_icon,
							});
							markers.push(marker);
								 google.maps.event.addListener(marker, 'click', (function(marker, i) {
									return function() {
											infowindow.setContent('<div id="map-marker-info" style="overflow: auto; cursor: default; clear: both; position: relative; border-radius: 4px; padding: 15px; border-color: rgb(255, 255, 255); border-style: solid; background-color: rgb(255, 255, 255); border-width: 1px; width: 275px; height: 130px;"><div style="overflow: hidden;" class="map-marker-info"><a  style="text-decoration: none;" href="'+response.dirs_json[i].link +'">	<span style="background-image: url('+response.dirs_json[i].image+')" class="list-cover has-image"></span><span class="address"><strong>'+response.dirs_json[i].title +'</strong></span> <span class="address" style="margin-top:15px">'+response.dirs_json[i].address+'</span></a></div></div>');
										infowindow.open(map, marker);
									}
								})(marker, i));
						}
					}
					var markerCluster = new MarkerClusterer(map, markers);


				if(response.loadmore=='hide'){
					jQuery('#loadmore_button').html('<h3></h3>');
				}
			}
		});

	}
	function save_favorite(id) {

		  var isLogged ="<?php echo get_current_user_id();?>";

                if (isLogged=="0") {
                     alert("<?php _e('Please login to add favorite','ivdirectories'); ?>");
                } else {

						var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
						var loader_image = '<img src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
						//jQuery('#fav_message').html(loader_image);
						var search_params={
							"action"  : 	"iv_directories_save_favorite",
							"data": "id=" + id,
						};

						jQuery.ajax({
							url : ajaxurl,
							dataType : "json",
							type : "post",
							data : search_params,
							success : function(response){
								jQuery("#fav_dir"+id).html('<a href="javascript:;" onclick="save_unfavorite('+id+')" ><span class="hide-sm"><i class="fa fa-heart fa-lg red-heart"></i>&nbsp;&nbsp; </span></a>');
							}
						});
				}
    }
	function save_unfavorite(id) {
		  var isLogged ="<?php echo get_current_user_id();?>";
                if (isLogged=="0") {
                     alert("<?php _e('Please login to remove favorite','ivdirectories'); ?>");
                } else {
						var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
						var loader_image = '<img src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
						//jQuery('#fav_message'+id).html(loader_image);
						var search_params={
							"action"  : 	"iv_directories_save_un_favorite",
							"data": "id=" + id,
						};
						jQuery.ajax({
							url : ajaxurl,
							dataType : "json",
							type : "post",
							data : search_params,
							success : function(response){
								jQuery("#fav_dir"+id).html('<a href="javascript:;" onclick="save_favorite('+id+')" ><span class="hide-sm"><i class="fa fa-heart fa-lg "></i>&nbsp;&nbsp; </span></a>');
							}
						});
				}
    }
function initialize_address() {
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
			google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            //document.getElementById('city2').value = place.name;
            document.getElementById('latitude').value = place.geometry.location.lat();
            document.getElementById('longitude').value = place.geometry.location.lng();
        });
    }
google.maps.event.addDomListener(window, 'load', initialize_address);
jQuery('input[name="range"]').on("change", function() {
		//jQuery(this).next().html(jQuery(this).val() + '%');
		jQuery('#rvalue').html(jQuery(this).val());
		jQuery('#range_value').val(jQuery(this).val());
		//console.log(jQuery(this).val());
});


</script>
