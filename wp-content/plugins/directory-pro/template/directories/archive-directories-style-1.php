	
<?php
global $post,$wpdb,$tag;
$directory_url=get_option('_iv_directory_url');					
if($directory_url==""){$directory_url='directories';}

$current_post_type=$directory_url;
$form_action='';
if ( is_front_page() ) {
  $form_action='action="'.get_post_type_archive_link($current_post_type).'"';

}

wp_enqueue_style('iv_directories-style-1109', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap.css');
wp_enqueue_script('iv_directories-ar-script-21', wp_iv_directories_URLPATH . 'admin/files/js/bootstrap.min.js');
wp_enqueue_script('iv_directories-script-12', wp_iv_directories_URLPATH . 'admin/files/js/markerclusterer.js');

wp_enqueue_style('iv_directories-css-queryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css');
wp_enqueue_script('iv_directories-jqueryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');

?>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<style>
.ui-autocomplete { position: absolute; cursor: default;z-index:990 !important;}

 .red-heart { color: red; }
 .iv-top-buffer { margin-top:10px!important; }



.range {   
    position: relative;
    height: 20px;
    
    background-color: rgb(245, 245, 245);
    border-radius: 5px;
    -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
    cursor: pointer;
}
.range-value {   
    position: relative;
  
    
}
.range input[type="range"] {
    -webkit-appearance: none !important;
    -moz-appearance: none !important;
    -ms-appearance: none !important;
    -o-appearance: none !important;
    appearance: none !important;

    display: table-cell;
    width: 100%;
    background-color: transparent;
    height: 20px;
    cursor: pointer;
}
.range input[type="range"]::-webkit-slider-thumb {
    -webkit-appearance: none !important;
    -moz-appearance: none !important;
    -ms-appearance: none !important;
    -o-appearance: none !important;
    appearance: none !important;

    width: 11px;
    height: 20px;
    color: rgb(255, 255, 255);
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
    background-color: rgb(153, 153, 153);
}
.range-success input[type="range"]::-webkit-slider-thumb {
    background-color: rgb(92, 184, 92);
}
.range-success input[type="range"]::-moz-slider-thumb {
    background-color: rgb(92, 184, 92);
}



.range input[type="range"] {
    outline: none;
}

.dir-box {
	
	min-height:395px;
	
	
	
}




#map-marker-info .address, .map-marker-info .rating, .map-marker-info h5 {
    margin: 0.5em 70px 0.5em 0px;
    display: block;
}
#map-marker-info .map-marker-info .list-cover {
    width: 60px;
    height: 60px;
    border-radius: 50%;
    position: absolute;
    right: 5px;
}
#map-marker-info .list-cover {
    background-size: cover;
    background-position: center center;
    width: 60px;
    height: 60px;
}
.thumbnail {
    position: relative;
    height: 135px;
    overflow: hidden;
	border: 0px !important;
 
    img {
        width:920px
        max-width: 920px; //only set this if using Twitter Bootstrap
        position: absolute;
        left:50%;
        margin-left: -460px; //half of the image size
    }
}
.panel-title {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 16px;
  color: inherit;
}
.dpro-igl-title {
  display: table;
  table-layout: fixed;
  width: 100%;
  padding-bottom: 15px;
}

.dpro-grid-listing-excerpt {
  height: 105px;
  overflow: hidden;
}
.text-excerpt {
  border-top: dashed 1px #bbb;
  padding: 15px 0;
  line-height: 1.8em;
}
</style>

		
<?php


if(isset($_POST['latitude'])){
	$ins_lat=$_POST['latitude'];
}else{
	$ins_lat='37.4419';
}	
if(isset($_POST['longitude'])){
	$ins_lng=$_POST['longitude'];
}else{
	$ins_lng='-122.1419';
}	
	
	$dirs_data =array();
	$tag_arr= array();
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array(
		'post_type' => $directory_url, // enter your custom post type
		'paged' => $paged, 
		'post_status' => 'publish',
		//'fields' => 'all',
		//'orderby' => 'ASC',
		//'posts_per_page'=> '2',  // overrides posts per page in theme settings
	);
	//$args['orderby']='title';
	//$args['order']='ASC';
	
	$lat='';$long='';$keyword_post='';$address='';$postcats ='';$selected='';
	
	if(get_query_var($directory_url.'-category')!=''){			
			$postcats = get_query_var($directory_url.'-category');
			$args[$directory_url.'-category']=$postcats;
			$selected=$postcats;	
	}
	
	if( isset($_POST['directories-category'])){
		if($_POST['directories-category']!=''){
			$postcats = $_POST['directories-category'];
			$args[$directory_url.'-category']=$postcats;
			$selected=$postcats;	
			$args['posts_per_page']='9999';
		}		
	}
	
	
	$radius=get_option('_iv_radius');
	if( isset($_POST['range_value'])){
		$radius = $_POST['range_value'];
	}	
	if($radius==''){$radius='50';}
	
	if( isset($_POST['address'])){
		if($_POST['address']!=""){
			$lat =  $_POST['latitude'];
			$long = $_POST['longitude'];
			$address=trim($_POST['address']);
			if($lat=='' || $long==''){
					$latitude='';$longitude='';
					
					$prepAddr = str_replace(' ','+',$address);
					$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
					$output= json_decode($geocode);
					if(isset( $output->results[0]->geometry->location->lat)){
						$latitude = $output->results[0]->geometry->location->lat;
					}
					if(isset($output->results[0]->geometry->location->lng)){
						$longitude = $output->results[0]->geometry->location->lng;
					}					
					$lat=$latitude;
					$long=$longitude;	
					$args['distance']='50';
				}else{
					$args['distance']=$radius;
				}						
			
			$args['lat']=$lat;
			$args['lng']=$long;			
			$args['posts_per_page']='9999';			
		}		
	}
	if( isset($_POST['keyword'])){
		if($_POST['keyword']!=""){
			$args['s']= $_POST['keyword'];
			$keyword_post=$_POST['keyword'];
			$args['posts_per_page']='9999';
		}
	}
	
	
	if( isset($tag)){
		if($tag!=""){
			if(!isset($_POST['keyword'])){
				$args['tag']= $tag;						
			}
		}
	}	
	if( isset($_POST['tag_arr'])){  
		if($_POST['tag_arr']!=""){  
			
			$tag_arr= $_POST['tag_arr'];	
			//$tag_arr= get_query_var('tag_arr');	
				
			$tags_string= implode("+", $tag_arr);
			$args['tag']= $tags_string;
			
		}
	}		
	// Meta Query***********************
$city_mq ='';
if(isset($_REQUEST['dir_city']) AND $_REQUEST['dir_city']!=''){							
		$city_mq = array(
		'relation' => 'AND',
			array(
				'key'     => 'city',
				'value'   => $_REQUEST['dir_city'],
				'compare' => 'LIKE'
			),
		);
}
$country_mq='';
if(isset($_REQUEST['dir_country']) AND $_REQUEST['dir_country']!=''){	
	$country_mq = array(
		'relation' => 'AND',
			array(
				'key'     => 'country',
				'value'   => $_REQUEST['dir_country'],
				'compare' => 'LIKE'
			),
		);
}
$zip_mq='';
if(isset($_REQUEST['zipcode']) AND $_REQUEST['zipcode']!=''){	
	$zip_mq = array(
		'relation' => 'AND',
			array(
				'key'     => 'postcode',
				'value'   => $_REQUEST['zipcode'],
				'compare' => 'LIKE'
			),
		);
}

$args['meta_query'] = array(
	$city_mq, $country_mq, $zip_mq,
);
	
    $args_bidding =$args ;
		
		$args_bidding['posts_per_page']='999999';
		$args_bidding['paged']='1';
		$the_query_bidding = new WP_GeoQuery( $args_bidding ); 
		// Bidding -- Search Paid listing*****
		$i=0;
		$bump_exp_date = '';//get_post_meta($row->ID,'_bump_exp_date',true);
		$bump_amount  = '';//get_post_meta($row->ID,'_bump_amount',true); 
		$bump_create_date='';// get_post_meta($row->ID,'_bump_create_date',true);
		$paid_area_count=0;
		$paid_ids = array();
		$paid_id_amount = array();
		if ( $the_query_bidding->have_posts() ) : 
			while ( $the_query_bidding->have_posts() ) : $the_query_bidding->the_post();
				$id = get_the_ID();
					$near_bump_exp_date=get_post_meta($id,'_bump_exp_date',true);
					$near_bump_create_date= get_post_meta($id,'_bump_create_date',true);
					$near_bump_amount= get_post_meta($id,'_bump_amount',true);												
					if(strtotime($near_bump_exp_date)>=time()){
						
							$paid_id_amount[$id]=$near_bump_amount;
							$paid_ids[$i]=$id;							
							$i++;																	
					}
			endwhile; 
		endif;		
		arsort($paid_id_amount); // sort TOP listing 
		
	// End Bidding Search Paid listing****
	
	   $the_query = new WP_GeoQuery( $args ); 
	  
	
	?>
	<!-- pagination here -->

	<!-- the loop -->
		
			<!-- Map**************-->
<?php
$search_show=0;		
$map_show=0;
$dir_searchbar_show=get_option('_dir_searchbar_show');	
if($dir_searchbar_show=="yes"){$search_show=1;}
$dir_map_show=get_option('_dir_map_show');	
if($dir_map_show=="yes"){$map_show=1;}


$main_class = new wp_iv_directories;

?>				
			<div class="" style="width:100%;<?php echo ($map_show==1 ? '': 'display: none;'); ?>" >								
				<div id="map"  style="height: 380px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden;"> </div>											 
			</div>		
				
<div class="bootstrap-wrapper ">
		<div class="container" id="directory-temp"> 					
		 <div class=" navbar-default navbar" style="<?php echo ($search_show==1 ? '': 'display: none;'); ?>">
			<div class=" navbar-collapse text-center" >		
					<form class="form-inline" method="POST"  <?php echo $form_action;?> onkeypress="return event.keyCode != 13;">
						<?php
						$dir_search_keyword=get_option('_dir_search_keyword');	
						if($dir_search_keyword==""){$dir_search_keyword='yes';}	
						if($dir_search_keyword=='yes'){
						?>	
					  <div class="form-group" >						
							<input type="text" class="form-control " id="keyword" name="keyword"  placeholder="<?php _e( 'Keyword', 'ivdirectories' ); ?>" value="<?php echo $keyword_post; ?>">
							<?php $pos = $main_class->get_unique_keyword_values('keyword',$current_post_type);
									
								?>
								<script>									
									jQuery(function() {
									var availableTags = [ "<?php echo  implode('","',$pos); ?>" ];
									jQuery( "#keyword" ).autocomplete({source: availableTags});
								  });
								  
								</script>
					  </div>
					  <?php
						}
					  $dir_search_city=get_option('_dir_search_city');	
						if($dir_search_city==""){$dir_search_city='yes';}	
						if($dir_search_city=='yes'){
						// City
						$args_citys = array(
							'post_type'  => $current_post_type,
							'posts_per_page' => -1,
							'meta_query' => array(
								array(
									'key'     => 'city',	
									'orderby' => 'meta_value', 
									'order' => 'ASC',		
								),
								
							),
						);
						$citys = new WP_Query( $args_citys );	
						$citys_all = $citys->posts;
						$get_cityies =array();
						foreach ( $citys_all as $term ) {
							$new_city="";
							$new_city=get_post_meta($term->ID,'city',true);
							if (!in_array($new_city, $get_cityies)) {
								$get_cityies[]=$new_city;
							
							}	
						}	

					// City
					
						
						?>	
						
						<div class="form-group" >
							<select name="dir_city"  id="dir_city" class="form-control" >
								<option   value=""><?php esc_html_e('Choose a City','ivdirectories'); ?></option>	
								<?php	
										$selected_city= (isset($_REQUEST['dir_city'])?$_REQUEST['dir_city']:'' );					
										if(count($get_cityies)) {									  
										  foreach($get_cityies as $row1) {
											  if($row1!=''){													  
											  ?>
											<option   value="<?php echo $row1; ?>" <?php echo ($selected_city==$row1?'selected':''); ?>><?php echo $row1; ?></option>
											<?php
											}
												
											}
										  
										} 
											
										?>												
							</select>
						</div>
						<?php
							}
						?>
						
						<?php
						$dir_search_country=get_option('_dir_search_country');	
						if($dir_search_country==""){$dir_search_country='yes';}	
						if($dir_search_country=='yes'){
						// Country
							$args_country = array(
								'post_type'  => $current_post_type,
								'posts_per_page' => -1,
								'meta_query' => array(
									array(
										'key'     => 'country',	
										'orderby' => 'meta_value', 
										'order' => 'ASC',		
									),
									
								),
							);
							$country = new WP_Query( $args_country );	
							$country_all = $country->posts;
							$get_country =array();
							foreach ( $country_all as $term ) {
								$new_country="";
								$new_country=get_post_meta($term->ID,'country',true);
								if (!in_array($new_country, $get_country)) {
									$get_country[]=$new_country;
								
								}	
							}	

							//Country
							?>
						<div class="form-group" >
							<select name="dir_country"  id="dir_country" class="form-control" >
								<option   value=""><?php esc_html_e('Choose a Country','ivdirectories'); ?></option>	
								<?php	$selected_country= (isset($_REQUEST['dir_country'])?$_REQUEST['dir_country']:'' );						
										if(count($get_country)) {									  
										  foreach($get_country as $row1) {
											  if($row1!=''){													  
											  ?>
											<option   value="<?php echo $row1; ?>" <?php echo ($selected_country==$row1?'selected':''); ?> ><?php echo $row1; ?></option>
											<?php
											}
												
											}
										  
										} 
											
										?>												
							</select>
						</div>
						<?php
							}
						?>
						<?php
						$dir_search_category=get_option('_dir_search_category');	
						if($dir_search_category==""){$dir_search_category='yes';}	
						if($dir_search_category=='yes'){
						?>		
					  <div class="form-group" style="margin-top:8px">
									<?php
								echo '<select name="directories-category" class="form-control">';
								echo'	<option selected="'.$selected.'" value="">'.__('Any Category','ivdirectories').'</option>';
								
										
										if( isset($_POST['submit'])){
											$selected = $_POST['directories-category'];
										}
											//directories
											$taxonomy = $directory_url.'-category';
											$args = array(
												'orderby'           => 'name', 
												'order'             => 'ASC',
												'hide_empty'        => true, 
												'exclude'           => array(), 
												'exclude_tree'      => array(), 
												'include'           => array(),
												'number'            => '', 
												'fields'            => 'all', 
												'slug'              => '',
												'parent'            => '0',
												'hierarchical'      => true, 
												'child_of'          => 0,
												'childless'         => false,
												'get'               => '', 
												
											);
								$terms = get_terms($taxonomy,$args); // Get all terms of a taxonomy
								if ( $terms && !is_wp_error( $terms ) ) :
									$i=0;
									foreach ( $terms as $term_parent ) {  ?>												
										
										
											<?php  
											
											echo '<option  value="'.$term_parent->slug.'" '.($selected==$term_parent->slug?'selected':'' ).'><strong>'.$term_parent->name.'<strong></option>';
											?>	
												<?php
												
												$args2 = array(
													'type'                     => $directory_url,						
													'parent'                   => $term_parent->term_id,
													'orderby'                  => 'name',
													'order'                    => 'ASC',
													'hide_empty'               => 1,
													'hierarchical'             => 1,
													'exclude'                  => '',
													'include'                  => '',
													'number'                   => '',
													'taxonomy'                 => $directory_url.'-category',
													'pad_counts'               => false 

												); 											
												$categories = get_categories( $args2 );	
												if ( $categories && !is_wp_error( $categories ) ) :
														
														
													foreach ( $categories as $term ) { 
														echo '<option  value="'.$term->slug.'" '.($selected==$term->slug?'selected':'' ).'>-'.$term->name.'</option>';
													} 	
																				
												endif;		
												
												?>
																			
	  
									<?php
										$i++;
									} 								
								endif;	
									echo '</select>';	
								?>		
						</div>	
						
						<?php
						}
						?>
					<?php
						 $_dir_search_zipcode=get_option('_dir_search_zipcode');	
						if($_dir_search_zipcode==""){$dir_search_location='yes';}	
						if($_dir_search_zipcode=='yes'){
							$zipcode=(isset($_REQUEST['zipcode'])?$_REQUEST['zipcode']:'' )
						?>		
						<div class="form-group" >
								<input type="text" class="form-control " id="zipcode" name="zipcode"  placeholder="<?php _e( 'Zipcode', 'ivdirectories' ); ?>"
								value="<?php echo trim($zipcode); ?>">
								
								<?php $pos = $main_class->get_unique_post_meta_values('postcode',$current_post_type);
									//print_r($pos);
								?>
								<script>
								  jQuery(function() {
									var availableTags = [ "<?php echo  implode('","',$pos); ?>" ];
									jQuery( "#zipcode" ).autocomplete({source: availableTags});
								  });
								</script>
								
						</div>	
						<?php
						}
						?>
					<?php
						$dir_search_location=get_option('_dir_search_location');	
						if($dir_search_location==""){$dir_search_location='yes';}	
						if($dir_search_location=='yes'){
						?>			
					 <div class="form-group" >							
								<input type="text" class="form-control " id="address" name="address"  placeholder="<?php _e( 'Location', 'ivdirectories' ); ?>" 
								value="<?php echo trim($address); ?>">
								<input type="hidden" id="latitude" name="latitude" placeholder="Latitude" value="<?php echo $lat; ?>" >
								<input type="hidden" id="longitude" name="longitude" placeholder="Longitude"  value="<?php echo $long; ?>">
					  </div>
					  <?php
						}
						$dir_f_search_radius=get_option('_dir_search_location');	
							if($dir_f_search_radius==""){$dir_f_search_radius='yes';}	
							if($dir_f_search_radius=='yes'){
					  ?>
						<div class="form-group" style="margin-top:8px">
							<?php
							
								$dir_search_redius=get_option('_dir_search_redius');	
								if($dir_search_redius==""){$dir_search_redius='Km';}	
							?>
							<?php _e( 'Radius', 'ivdirectories' ); ?>: <span id="rvalue"><?php echo $radius;?></span><?php echo ' '.$dir_search_redius; ?>
						</div>
						
					 	<div class="form-group" >
								<div class="range range-success">					
									<input type="range" name="range" id="range" min="1" max="1000" value="<?php echo $radius;?>" onchange="range.value=value">	
									<input type="hidden" name="range_value" id="range_value" value="<?php echo $radius; ?>" >							
								</div>	
						</div>	
						<?php
						}
						?>
						<div class="form-group" >
							<button type="submit" id="submit" name="submit"  class="btn btn-default "><?php _e('Search','ivdirectories'); ?> </button>	
						</div>	
						
						
					 
					</form>				
		
		 </div>
		</div>	
	
	
	<div class="" id="directory-temp" style= "margin-top:10px">	
		
	
	<?php 
		// For Bidding Loop*******************
		$ii=1;
	if($paged==1){
		foreach ($paid_id_amount as $key => $val) { 
			$id=$key; 
			$post = get_post($id);			
			?>	
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pull-left  " style= "margin-top:10px;">
			<div class="panel panel-default panel-relative dir-box">							
				<div class="" style="overflow: hidden; position: relative;height:200px;">
					<a href="<?php echo get_post_permalink($id);?>"> 
					<?php 
						if(has_post_thumbnail()){ 
							$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' ); 
							if($feature_image[0]!=""){ 
								?>
							<img width="490" height="" src="<?php  echo $feature_image[0]; ?>" class="img-responsive" alt="" draggable="false">
								
							<?php
							}	
							?>																					
							
						<?php
						}else{ ?>
							<img width="400" height="" src="<?php echo  wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";?>" class="img-responsive" alt="" draggable="false">
						<?php	
						}	
						?>
					</a>
				</div>
								
					<div class="panel-body">
						<div class="col-md-12" style="overflow: hidden;height:70px;">										
							<a href="<?php echo the_permalink();?>" class="dpro-tooltip dpro-igl-title" style="text-decoration: none;"> 										
								<div class="panel-title text-center">
									<strong> 
										<?php 
											if (strlen(the_title('','',FALSE)) > 30) {
										   $title_short = substr(the_title('','',FALSE), 0, 30);
											   preg_match('/^(.*)\s/s', $title_short, $matches);
										   if (isset($matches[1])) $title_short = $matches[1];
											   $title_short = $title_short.'..';
										   } else {
											   $title_short = the_title('','',FALSE);
											}		
											echo $title_short;	
												//the_title();
											?>
									 </strong>
									</div>
									<?php
									$currentCategory=wp_get_object_terms( $id, $directory_url.'-category');
									$cat_link='';$cat_name='';$cat_slug='';
									if(isset($currentCategory[0]->slug)){										
										$cat_slug = $currentCategory[0]->slug;
										$cat_name = $currentCategory[0]->name;
										$cc=0;
										foreach($currentCategory as $c){		
												if($cc==0){
													$cat_name =$c->slug;
													$cat_slug =$c->slug;
												}else{
													$cat_name = $cat_name .', '.$c->name;
													$cat_slug = $cat_slug .' '.$c->slug;
												}															
											$cc++;
										}														
										$cat_link= get_term_link($currentCategory[0], $directory_url.'-category');
									}
									?>
									<div class="text-center"><?php echo $cat_name.'&nbsp;'; ?></div>											
							</a>
						</div>
					
						<div class="col-md-12 text-center text-excerpt" style="overflow: hidden;height:100px;">
							<?php									
							if(strlen(substr(get_the_excerpt(), 0,70))>=70){
								echo substr(get_the_excerpt(), 0,70).'...';
							}else{
								echo get_the_excerpt();
							}
							?>	
						</div>										
						<div class="col-md-12 text-center " style="margin-top:10px;overflow: hidden;height:25px;">						
									
									<?php
									$now = time();
									$new_badge_day=get_option('_iv_new_badge_day');
									if($new_badge_day==''){$new_badge_day=7;}
									 $post_date = strtotime($post->post_date);
									 $datediff = $now - $post_date;
									 $total_day =  floor($datediff/(60*60*24));
									 if($total_day<=$new_badge_day ){ ?>
										
											<img  style="width:40px;"  src="<?php echo  wp_iv_directories_URLPATH."/assets/images/newicon-big.png";?>">
										
									<?php	
									 }
									$post_author_id= $post->post_author;
									$author_package_id=get_user_meta($post_author_id, 'iv_directories_package_id', true); 
									$have_vip_badge= get_post_meta($author_package_id,'iv_directories_package_vip_badge',true);
									$exprie_date= strtotime (get_user_meta($post_author_id, 'iv_directories_exprie_date', true));	
									$current_date=time();
									if($have_vip_badge=='yes'){
										if($exprie_date >= $current_date){ ?>
																
												<img style="width:30px;"  src="<?php echo  wp_iv_directories_URLPATH."/assets/images/vipicon.png";?>">
											
										<?php
										}	
									}								
									?>
									
									
									<span id="fav_dir<?php echo $id; ?>" >					
										<?php
											$user_ID = get_current_user_id();
											if($user_ID>0){
												$my_favorite = get_post_meta($id,'_favorites',true);
												$all_users = explode(",", $my_favorite);
												if (in_array($user_ID, $all_users)) { ?>
													<a  data-toggle="tooltip" data-placement="bottom" title="<?php _e('Added to Favorites','ivdirectories'); ?>" href="javascript:;" style="text-decoration: none;" onclick="save_unfavorite('<?php echo $id; ?>')" >   
													<span class="hide-sm"><i class="fa fa-heart fa-lg red-heart"></i>&nbsp;&nbsp; </span></a> 
												<?php								
												}else{ ?>
													<a data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" style="text-decoration: none;" onclick="save_favorite('<?php echo $id; ?>')" >
													<span class="hide-sm"><i class="fa fa-heart fa-lg"></i>&nbsp;&nbsp; </span>
													</a> 
												<?php	
												}
												
											}else{ ?>
													
													<a data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" style="text-decoration: none;" onclick="save_favorite('<?php echo $id; ?>')" >
													<span class="hide-sm"><i class="fa fa-heart fa-lg "></i>&nbsp;&nbsp; </span>
													</a> 
											
										<?php							
											}
											 
										?>
									</span>	
									
						</div>	
													
								
					</div>	 <!-- /.panel-body -->
				</div> <!-- /.panel -->
			</div><!-- /.col-md-4  -->
				
				
			
				
			<?php				
			$ii++;	
		}
	}	
		// END Bidding top loop End**************
		
	?>
	
	<?php	
	$i=1;
	 if ( $the_query->have_posts() ) : 
	
	while ( $the_query->have_posts() ) : $the_query->the_post();
				$id = get_the_ID();
				
				$gallery_ids=get_post_meta($id ,'image_gallery_ids',true);
				$gallery_ids_array = array_filter(explode(",", $gallery_ids));
				
				$dir_data['link']=get_post_permalink();
				$dir_data['title']=$post->post_title; 				
				$dir_data['lat']=get_post_meta($id,'latitude',true);
				$dir_data['lng']=get_post_meta($id,'longitude',true);
				$ins_lat=get_post_meta($id,'latitude',true);
				$ins_lng=get_post_meta($id,'longitude',true);
				$dir_data['address']=get_post_meta($id,'address',true); 
				$dir_data['image']= '';
				$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'thumbnail' ); 
				if($feature_image[0]!=""){ 
					//$dir_data['image']= '<img class=" img-responsive" src="'. $feature_image[0].'">';
					$dir_data['image']=  $feature_image[0];
				}
				$dir_data['marker_icon']=wp_iv_directories_URLPATH."/assets/images/map-marker/map-marker.png";				
				$currentCategoryId='';
				$terms =get_the_terms($id, $directory_url."-category");				
				if($terms!=""){
					foreach ($terms as $termid) {  
						if(isset($termid->term_id)){
							 $currentCategoryId= $termid->term_id; 
						}					  
					} 
				}
				$marker = get_option('_cat_map_marker_'.$currentCategoryId,true);
				if($marker!=''){
					$image_attributes = wp_get_attachment_image_src( $marker ); // returns an array
					if( $image_attributes ) {
					
						$dir_data['marker_icon']= $image_attributes[0];
					}							
				}
				
				
				if($dir_data['lat']!='' AND $dir_data['lng']!='' ){				
					array_push( $dirs_data, $dir_data );
				}
				
				if (in_array($id, $paid_ids)) {
					continue;
				}
				?>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pull-left  " style= "margin-top:10px;">
						<div class="panel panel-default panel-relative dir-box">							
							<div class="" style="overflow: hidden; position: relative;height:200px;" >
								<a href="<?php echo get_post_permalink($id);?>"> 
								<?php 
									if(has_post_thumbnail()){ 
										$feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'large' ); 
										if($feature_image[0]!=""){ 
											?>
										<img width="490" height="" src="<?php  echo $feature_image[0]; ?>" class="img-responsive" alt="hotel6" draggable="false">
											
										<?php
										}	
										?>																					
										
									<?php
									}else{ ?>
										<img width="400" height="" src="<?php echo  wp_iv_directories_URLPATH."/assets/images/default-directory.jpg";?>" class="img-responsive" alt="hotel6" draggable="false">
									<?php	
									}	
									?>
								</a>
							</div>
								
								<div class="panel-body">
									<div class="col-md-12" style="overflow: hidden;height:70px;">										
										<a href="<?php echo the_permalink();?>" class="dpro-tooltip dpro-igl-title" style="text-decoration: none;"> 										
											<div class="panel-title text-center">
												<strong> 
													<?php 
														if (strlen(the_title('','',FALSE)) > 30) {
													   $title_short = substr(the_title('','',FALSE), 0, 30);
														   preg_match('/^(.*)\s/s', $title_short, $matches);
														if (isset($matches[1])) $title_short = $matches[1];
														   $title_short = $title_short.'..';
													   } else {
														   $title_short = the_title('','',FALSE);
														}		
														echo $title_short;	
															//the_title();
														?>
												 </strong>
												</div>
												<?php
												$currentCategory=wp_get_object_terms( $id, $directory_url.'-category');
												$cat_link='';$cat_name='';$cat_slug='';
												if(isset($currentCategory[0]->slug)){										
													$cat_slug = $currentCategory[0]->slug;
													$cat_name = $currentCategory[0]->name;	
													$cc=0;
													foreach($currentCategory as $c){		
															if($cc==0){
																$cat_name =$c->name;
																$cat_slug =$c->slug;
															}else{
																$cat_name = $cat_name .', '.$c->name;
																$cat_slug = $cat_slug .' '.$c->slug;
															}															
														$cc++;
													}													
													$cat_link= get_term_link($currentCategory[0], $directory_url.'-category');
												}
												?>
												<div class="text-center"><?php echo $cat_name.'&nbsp;'; ?></div>											
										</a>
									</div>
								
									<div class="col-md-12 text-center text-excerpt" style="overflow: hidden;height:100px;">
										<?php									
										if(strlen(substr(get_the_excerpt(), 0,70))>=70){
											echo substr(get_the_excerpt(), 0,70).'...';
										}else{
											echo get_the_excerpt();
										}
										?>	
									</div>										
									<div class="col-md-12 text-center " style="margin-top:10px;overflow: hidden;height:25px;">						
												
												<?php
												$now = time();
												$new_badge_day=get_option('_iv_new_badge_day');
												if($new_badge_day==''){$new_badge_day=7;}
												 $post_date = strtotime($post->post_date);
												 $datediff = $now - $post_date;
												 $total_day =  floor($datediff/(60*60*24));
												 if($total_day<=$new_badge_day ){ ?>
													
														<img  style="width:40px;"  src="<?php echo  wp_iv_directories_URLPATH."/assets/images/newicon-big.png";?>">
													
												<?php	
												 }
												$post_author_id= $post->post_author;
												$author_package_id=get_user_meta($post_author_id, 'iv_directories_package_id', true); 
												$have_vip_badge= get_post_meta($author_package_id,'iv_directories_package_vip_badge',true);
												$exprie_date= strtotime (get_user_meta($post_author_id, 'iv_directories_exprie_date', true));	
												$current_date=time();
												if($have_vip_badge=='yes'){
													if($exprie_date >= $current_date){ ?>
																			
															<img style="width:30px;"  src="<?php echo  wp_iv_directories_URLPATH."/assets/images/vipicon.png";?>">
														
													<?php
													}	
												}								
												?>
												
												<span id="fav_dir<?php echo $id; ?>" >					
													<?php
														$user_ID = get_current_user_id();
														if($user_ID>0){
															$my_favorite = get_post_meta($id,'_favorites',true);
															$all_users = explode(",", $my_favorite);
															if (in_array($user_ID, $all_users)) { ?>
																<a  data-toggle="tooltip" data-placement="bottom" title="<?php _e('Added to Favorites','ivdirectories'); ?>" href="javascript:;" style="text-decoration: none;" onclick="save_unfavorite('<?php echo $id; ?>')" >   
																<span class="hide-sm"><i class="fa fa-heart fa-lg red-heart"></i>&nbsp;&nbsp; </span></a> 
															<?php								
															}else{ ?>
																<a data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" style="text-decoration: none;" onclick="save_favorite('<?php echo $id; ?>')" >
																<span class="hide-sm"><i class="fa fa-heart fa-lg"></i>&nbsp;&nbsp; </span>
																</a> 
															<?php	
															}
															
														}else{ ?>
																
																<a data-toggle="tooltip" data-placement="bottom" title="<?php _e('Add to Favorites','ivdirectories'); ?>" href="javascript:;" style="text-decoration: none;" onclick="save_favorite('<?php echo $id; ?>')" >
																<span class="hide-sm"><i class="fa fa-heart fa-lg "></i>&nbsp;&nbsp; </span>
																</a> 
														
													<?php							
														}
														 
													?>
												</span>	
												
									</div>	
													
								
							</div>	 <!-- /.panel-body -->
						</div> <!-- /.panel -->
					</div><!-- /.col-md-4  -->
				
				
			
		<?php 
				if($ii>=3){ $ii=0; ?>
					<!--
					<div class="clearfix" ></div> 
					-->
				<?php
				}
			$ii++;	
		
		
		$i++;
		
	endwhile; 
		$dirs_json ='';
		if(!empty($dirs_data)){
			$dirs_json =json_encode($dirs_data);
		}
	 
	?>	
	<!-- end of the loop -->	
		<!--
		paging plugin
		https://wordpress.org/plugins/wp-pagenavi/screenshots/
		-->
	
		<?php if (function_exists('wp_pagenavi')) : ?>
				<div class="col-md-12 text-center">
				<?php wp_pagenavi( array( 'query' => $the_query ) ); ?>
				</div>
			<?php else: 
				?>
					
					<div class="">			
						<div class="col-sm-6 col-md-6 nav-next"><?php previous_posts_link( '<div class=""> <div class="fa fa-arrow-circle-left"></div>'.__( ' Newer Entries', 'ivdirectories' ).'</div>' ); ?></div>
							<div class="col-sm-6 col-md-6 nav-previous"><?php next_posts_link( '<div class="">'.__( ' Older Entries ', 'ivdirectories' ).'<div class="fa fa-arrow-circle-right"></div></div>' ); ?>
						</div>
					</div>
		  <?php endif; ?>
		<!--END .navigation-links-->
			
			<div class="clearfix"></div> 	
		<?php wp_reset_postdata(); ?>

		<?php else :
			$dirs_json='';
		 ?>
				<div class="col-md-12 iv-top-buffer">
					<?php _e( 'Sorry, no posts matched your criteria.' ); ?>
				</div>
		<?php endif; ?>
		</div>
		
		<div> <p>&nbsp;</p></div>
	</div>
	
</div>

<?php
$dir_map_api=get_option('_dir_map_api');	
if($dir_map_api==""){$dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';}
$dir_map_zoom=get_option('_dir_map_zoom');	
if($dir_map_zoom==""){$dir_map_zoom='7';}		
?>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $dir_map_api;?>'></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>



			
			<script type="text/javascript">								
                   
					
				function initialize() {
					var center = new google.maps.LatLng('<?php echo $ins_lat; ?>', '<?php echo $ins_lng; ?>');
					//var center = new google.maps.LatLng(49, 2.56);
					

					var map = new google.maps.Map(document.getElementById('map'), {
						zoom: <?php echo $dir_map_zoom; ?>,
						center: center,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});

					var markers = [];
					var infowindow = new google.maps.InfoWindow();
					var dirs ='';
					var min = .999999;
					var max = 1.000002;
					<?php echo ($dirs_json!=''? 'var dirs ='.$dirs_json:''); ?>;
					if(dirs!=''){
					 for (i = 0; i < dirs.length; i++) {
						//for(var i=0;i<5;i++){
							//console.log(dirs[i);
							
							var new_lat= dirs[i].lat  * (Math.random() * (max - min) + min);
							var new_lng= dirs[i].lng  * (Math.random() * (max - min) + min);
							var latLng = new google.maps.LatLng(new_lat,new_lng);	
							var marker = new google.maps.Marker({
								position: latLng,
								map: map,
								icon: dirs[i].marker_icon,
							});
							markers.push(marker);
								 google.maps.event.addListener(marker, 'click', (function(marker, i) {
									return function() {
										
										
											//infowindow.setContent('<div id="map-marker-info " ><a href="'+dirs[i].link +'">'+dirs[i].image+'<h5>'+ dirs[i].title //+'</h5><span class="address">'+dirs[i].address+'</span></a></div>');
											
											infowindow.setContent('<div id="map-marker-info" style="overflow: auto; cursor: default; clear: both; position: relative; border-radius: 4px; padding: 15px; border-color: rgb(255, 255, 255); border-style: solid; background-color: rgb(255, 255, 255); border-width: 1px; width: 275px; height: 130px;"><div style="overflow: hidden;" class="map-marker-info"><a  style="text-decoration: none;" href="'+dirs[i].link +'">	<span style="background-image: url('+dirs[i].image+')" class="list-cover has-image"></span><span class="address"><strong>'+dirs[i].title +'</strong></span> <span class="address" style="margin-top:15px">'+dirs[i].address+'</span></a></div></div>');
										infowindow.open(map, marker);
									}
								})(marker, i));

						}
					}

					var markerCluster = new MarkerClusterer(map, markers);
				}	
				function cs_toggle_street_view(btn) {
				  var toggle = panorama.getVisible();
				  if (toggle == false) {
					if(btn == 'streetview'){
					  panorama.setVisible(true);
					}
				  } else {
					if(btn == 'mapview'){
					  panorama.setVisible(false);
					}
				  }
				}

                google.maps.event.addDomListener(window, 'load', initialize);					
				
				//google.maps.event.trigger(map, 'resize');					
					jQuery('a[href="#locationmap"]').on('click', function(e) {
						setTimeout(function(){
								initialize();	
								google.maps.event.trigger(map, 'resize');
						},500)
							
					});
				
						
			</script>
<script>
	function save_favorite(id) {       
		
		  var isLogged ="<?php echo get_current_user_id();?>";
                               
                if (isLogged=="0") {                   
                     alert("<?php _e('Please login to add favorite','ivdirectories'); ?>");
                } else { 
						
						var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
						var loader_image = '<img src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
						//jQuery('#fav_message').html(loader_image); 
						var search_params={
							"action"  : 	"iv_directories_save_favorite",	
							"data": "id=" + id,
						};
						
						jQuery.ajax({					
							url : ajaxurl,					 
							dataType : "json",
							type : "post",
							data : search_params,
							success : function(response){ 								
								jQuery("#fav_dir"+id).html('<a href="javascript:;" onclick="save_unfavorite('+id+')" ><span class="hide-sm"><i class="fa fa-heart fa-lg red-heart"></i>&nbsp;&nbsp; </span></a>');
							
								
							}
						});
						
				}  
				
    }
	function save_unfavorite(id) {       
		
		  var isLogged ="<?php echo get_current_user_id();?>";
                               
                if (isLogged=="0") {                   
                     alert("<?php _e('Please login to remove favorite','ivdirectories'); ?>");
                } else { 
						
						var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
						var loader_image = '<img src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
						//jQuery('#fav_message'+id).html(loader_image); 
						var search_params={
							"action"  : 	"iv_directories_save_un_favorite",	
							"data": "id=" + id,
						};
						
						jQuery.ajax({					
							url : ajaxurl,					 
							dataType : "json",
							type : "post",
							data : search_params,
							success : function(response){								
								jQuery("#fav_dir"+id).html('<a href="javascript:;" onclick="save_favorite('+id+')" ><span class="hide-sm"><i class="fa fa-heart fa-lg "></i>&nbsp;&nbsp; </span></a>');
							
								
							}
						});
						
				}  
				
    }
	
function initialize_address() {
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
			google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            //document.getElementById('city2').value = place.name;
            document.getElementById('latitude').value = place.geometry.location.lat();
            document.getElementById('longitude').value = place.geometry.location.lng(); 
        });
    }
google.maps.event.addDomListener(window, 'load', initialize_address);

				jQuery(document).ready(function() { 
					var geocoder;
					geocoder = new google.maps.Geocoder();	 
				  initialize_address();
						  
				  jQuery(function() {
					jQuery("#address").autocomplete({
					  //This bit uses the geocoder to fetch address values
					  source: function(request, response) {
						geocoder.geocode( {'address': request.term }, function(results, status) {
						  response(jQuery.map(results, function(item) {
							return {
							  label:  item.formatted_address,
							  value: item.formatted_address,
							  latitude: item.geometry.location.lat(),
							  longitude: item.geometry.location.lng()
							}
						  }));
						})
					  },
					});
				  });							  
				  
				  
				});
	
jQuery('input[name="range"]').on("change", function() { 
		//jQuery(this).next().html(jQuery(this).val() + '%');
		jQuery('#rvalue').html(jQuery(this).val());
		jQuery('#range_value').val(jQuery(this).val());
		//console.log(jQuery(this).val());
});
jQuery(function () {
  jQuery('[data-toggle="tooltip"]').tooltip();
})
</script>
