<?php
$dir_map_api=get_option('_dir_map_api');	
if($dir_map_api==""){$dir_map_api='AIzaSyCIqlk2NLa535ojmnA7wsDh0AS8qp0-SdE';}	
?>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $dir_map_api;?>'></script>
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />

          <div class="profile-content">
            
              <div class="portlet light">
                <div class="portlet-title tabbable-line clearfix">
                    <div class="caption caption-md">
                      <span class="caption-subject"> <?php _e('New Listing','ivdirectories'); ?></span>
                    </div>
					
                  </div>
               
                  <div class="portlet-body">
                    <div class="tab-content">
                    
                      <div class="tab-pane active" id="tab_1_1">
					  <?php					
						global $wpdb;
						$directory_url=get_option('_iv_directory_url');					
						if($directory_url==""){$directory_url='directories';}
						// Check Max\
						$sql="SELECT * FROM $wpdb->posts WHERE post_type = 'iv_directories_pack'  and post_status='draft' ";
						$membership_pack = $wpdb->get_results($sql);
						$total_package = count($membership_pack);
							
						$package_id=get_user_meta($current_user->ID,'iv_directories_package_id',true);	
						
						if($package_id==""){ 
							global $wpdb; $user_role= $current_user->roles[0];
							$sql="SELECT * FROM $wpdb->posts WHERE post_type = 'iv_directories_pack'";
							$membership_pack = $wpdb->get_results($sql);
							$total_package=count($membership_pack);								
							if(sizeof($membership_pack)>0){
								$i=0;
								foreach ( $membership_pack as $row )
								{	
									if(get_post_meta($row->ID, 'iv_directories_package_user_role', true)==$user_role ){
										$package_id=$row->ID ;
									}
								}
								$max=get_post_meta($package_id, 'iv_directories_package_max_post_no', true);
								
							}else{
								$max=999999;
							}	
						}					
						
						$max=get_post_meta($package_id, 'iv_directories_package_max_post_no', true);
						
						$user_role= $current_user->roles[0];
						if(isset($current_user->roles[0]) and $current_user->roles[0]=='administrator'){
							$max=999999;
						}	
						if(sizeof($membership_pack)<1){ 
							$max=999999;
						
						}	
						
								
						
						$sql="SELECT count(*) as total FROM $wpdb->posts WHERE post_type ='".$directory_url."'  and post_author='".$current_user->ID."' ";									
						$all_post = $wpdb->get_row($sql);
						$my_post_count=$all_post->total;
						
						if ( $my_post_count>=$max or !current_user_can('edit_posts') )  {
								$iv_redirect = get_option( '_iv_directories_profile_page');							
								$reg_page= get_permalink( $iv_redirect); 							
							?>
							<?php _e('Please Upgrade Your Account','ivdirectories'); ?>
							 <a href="<?php echo $reg_page.'?&profile=level'; ?>" title="Upgarde"><b><?php _e('Here','ivdirectories'); ?> </b></a> 
							<?php _e('To Add More Post.','ivdirectories'); ?>	
							
							
						<?php
						}else{
					
					?>					
					
						<div class="row">
							<div class="col-md-12">	 
							
							 
							<form action="" id="new_post" name="new_post"  method="POST" role="form">
								<div class=" form-group">
									<label for="text" class=" control-label"><?php _e('Title','ivdirectories'); ?></label>
									<div class="  "> 
										<input type="text" class="form-control" name="title" id="title" value="" placeholder="<?php _e('Enter Title Here','ivdirectories'); ?>">
									</div>																		
								</div>
								
								<div class="form-group">
										
										<div class=" ">
												<?php
													$settings_a = array(															
														'textarea_rows' =>8,
														'editor_class' => 'form-control'															 
														);
													
													$editor_id = 'new_post_content';
													wp_editor( '', $editor_id,$settings_a );										
													?>
									
										</div>
									
								</div>
								<div class=" row form-group ">
									<label for="text" class=" col-md-5 control-label"><?php _e('Feature Image','ivdirectories'); ?>  </label>
									
										<div class="col-md-4" id="post_image_div">
											<a  href="javascript:void(0);" onclick="edit_post_image('post_image_div');"  >									
											<?php  echo '<img width="100px" src="'. wp_iv_directories_URLPATH.'assets/images/image-add-icon.png">'; ?>			
											</a>					
										</div>
										
										<input type="hidden" name="feature_image_id" id="feature_image_id" value="">
										
										<div class="col-md-3" id="post_image_edit">	
											<button type="button" onclick="edit_post_image('post_image_div');"  class="btn btn-xs green-haze"><?php _e('Add','ivdirectories'); ?> </button>
											
										</div>									
								</div>
								<div class=" row form-group ">
									<label for="text" class=" col-md-5 control-label"><?php _e('Image Gallery','ivdirectories'); ?> 
										<button type="button" onclick="edit_gallery_image('gallery_image_div');"  class="btn btn-xs green-haze"><?php _e('Add Images','ivdirectories'); ?></button>
									 </label>						
								</div>
								<div class=" row form-group ">	
											<!--
										<div class="col-md-12" id="gallery_image_div">
											
											<a  href="javascript:void(0);" onclick="edit_gallery_image('gallery_image_div');"  >									
											<?php  echo '<img src="'. wp_iv_directories_URLPATH.'assets/images/gallery_icon.png">'; ?>			
											</a>
															
										</div>
											-->
										<input type="hidden" name="gallery_image_ids" id="gallery_image_ids" value="">
										
										<div class="col-md-12" id="gallery_image_div">
										</div>									
								</div>
								
																
								
								<div class="clearfix"></div>
								<div class=" row form-group ">
									<label for="text" class=" col-md-12 control-label"><?php _e('Post Status','ivdirectories'); ?>  </label>
									
										<div class="col-md-12" id="">										
										<select name="post_status" id="post_status"  class="form-control">
											<?php
												$dir_approve_publish =get_option('_dir_approve_publish');
												if($dir_approve_publish!='yes'){?>
													<option value="publish"><?php _e('Publish','ivdirectories'); ?></option>
												<?php	
												}else{ 
													if(isset($current_user->roles[0]) and $current_user->roles[0]=='administrator'){?>
														<option value="publish"><?php _e('Publish','ivdirectories'); ?></option>
													<?php
													}	
													?>
													
													<option value="pending"><?php _e('Pending Review','ivdirectories'); ?></option>
												<?php
												}
											?>	
											<option value="draft"><?php _e('Draft','ivdirectories'); ?></option>
											
										</select>										
											
											
										</div>				
																		
								</div>
								
								
								<div class="clearfix"></div>
								<div class=" row form-group">
									<label for="text" class=" col-md-12 control-label"><?php _e('Category','ivdirectories'); ?></label>									
									<div class=" col-md-12 "> 
								
								<?php
									echo '<select name="postcats[]" class="form-control " multiple="multiple">';
									//echo'	<option selected="'.$selected.'" value="">'.__('Choose a category','ivdirectories').'</option>';
								
										$selected='';
										if( isset($_POST['submit'])){
											$selected = $_POST['postcats'];
										}
											//directories
											$taxonomy = $directory_url.'-category';
											$args = array(
												'orderby'           => 'name', 
												'order'             => 'ASC',
												'hide_empty'        => false, 
												'exclude'           => array(), 
												'exclude_tree'      => array(), 
												'include'           => array(),
												'number'            => '', 
												'fields'            => 'all', 
												'slug'              => '',
												'parent'            => '0',
												'hierarchical'      => true, 
												'child_of'          => 0,
												'childless'         => false,
												'get'               => '', 
												
											);
								$terms = get_terms($taxonomy,$args); // Get all terms of a taxonomy
								if ( $terms && !is_wp_error( $terms ) ) :
									$i=0;
									foreach ( $terms as $term_parent ) {  ?>												
										
										
											<?php //echo'<br/> '. $term_parent->name.' ..ID..'.$term_parent->term_id;  
											
											echo '<option  value="'.$term_parent->slug.'" '.($selected==$term_parent->slug?'selected':'' ).'><strong>'.$term_parent->name.'<strong></option>';
											?>	
												<?php
												
												$args2 = array(
													'type'                     => $directory_url,						
													'parent'                   => $term_parent->term_id,
													'orderby'                  => 'name',
													'order'                    => 'ASC',
													'hide_empty'               => 0,
													'hierarchical'             => 1,
													'exclude'                  => '',
													'include'                  => '',
													'number'                   => '',
													'taxonomy'                 => $directory_url.'-category',
													'pad_counts'               => false 

												); 											
												$categories = get_categories( $args2 );	
												if ( $categories && !is_wp_error( $categories ) ) :
														
														
													foreach ( $categories as $term ) { 
														echo '<option  value="'.$term->slug.'" '.($selected==$term->slug?'selected':'' ).'>--'.$term->name.'</option>';
													} 	
																				
												endif;		
												
												?>
																			
	  
									<?php
										$i++;
									} 								
								endif;	
									echo '</select>';	
								?>		
									</div>
																		
								</div>
								
								
								


						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('Address','ivdirectories'); ?></label>							
							<div class=" "> 
								<input type="text" class="form-control" name="address" id="address" value="" placeholder="<?php _e('Enter here Here','ivdirectories'); ?>">
							</div>
							
								
                                
                                								
						</div>
						
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('City','ivdirectories'); ?></label>							
							
								<input type="text" class="form-control" name="city" id="city" value="" placeholder="<?php _e('Enter city ','ivdirectories'); ?>">
						</div>
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('Zipcode','ivdirectories'); ?></label>							
							
								<input type="text" class="form-control" name="postcode" id="postcode" value="" placeholder="<?php _e('Enter Zipcode ','ivdirectories'); ?>">
						</div>
						
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('State','ivdirectories'); ?></label>							
							
								<input type="text" class="form-control" name="state" id="state" value="" placeholder="<?php _e('Enter State ','ivdirectories'); ?>">
						</div>
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('Country','ivdirectories'); ?></label>							
							
								<input type="text" class="form-control" name="country" id="country" value="" placeholder="<?php _e('Enter Country ','ivdirectories'); ?>">
						</div>
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('Latitude','ivdirectories'); ?></label>							
							
								<input type="text" class="form-control" name="latitude" id="latitude" value="" placeholder="<?php _e('Enter latitude ','ivdirectories'); ?>">
						</div>
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('Longitude','ivdirectories'); ?></label>							
							
								<input type="text" class="form-control" name="longitude" id="longitude" value="" placeholder="<?php _e('Enter longitude ','ivdirectories'); ?>">
						</div>
						
						
						
						
						<div class=" form-group">
							<label for="text" class=" control-label"><?php _e('Address Map','ivdirectories'); ?></label>							
							<div class=" "> 
									<div  id="map-canvas"  style="width:100%;height:300px;"></div>
										
									<script type="text/javascript">
								var geocoder;
								jQuery(document).ready(function($) {									
									var map;
									var marker;

									 geocoder = new google.maps.Geocoder();
									

									function geocodePosition(pos) {
									  geocoder.geocode({
									    latLng: pos
									  }, function(responses) {
									    if (responses && responses.length > 0) {
									      updateMarkerAddress(responses[0].formatted_address);
									    } else {
									      updateMarkerAddress('Cannot determine address at this location.');
									    }
									  });
									}

									function updateMarkerPosition(latLng) {
									  jQuery('#latitude').val(latLng.lat());
									  jQuery('#longitude').val(latLng.lng());	
										//console.log(latLng);	
										codeLatLng(latLng.lat(), latLng.lng());
									}

									function updateMarkerAddress(str) {
									  jQuery('#address').val(str);
									}

									function initialize() {
									  
										 
										  var latlng = new google.maps.LatLng(40.748817, -73.985428);
									  	
									  
									  var mapOptions = {
									    zoom: 2,
									    center: latlng
									  }

									  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
										
									  geocoder = new google.maps.Geocoder();

									  marker = new google.maps.Marker({
									  	position: latlng,
									    map: map,
									    draggable: true
									  });

									  // Add dragging event listeners.
									  google.maps.event.addListener(marker, 'dragstart', function() {
									    updateMarkerAddress('Please Wait Dragging...');
									  });
									  
									  google.maps.event.addListener(marker, 'drag', function() {
									    updateMarkerPosition(marker.getPosition());
									  });
									  
									  google.maps.event.addListener(marker, 'dragend', function() {
									    geocodePosition(marker.getPosition());
									  });

									}

									google.maps.event.addDomListener(window, 'load', initialize);
									
									

									jQuery(document).ready(function() { 
									         
									  initialize();
									          
									  jQuery(function() {
										  
										  
										  
										   var input = document.getElementById('address');
											var autocomplete = new google.maps.places.Autocomplete(input);
												google.maps.event.addListener(autocomplete, 'place_changed', function () {
												var place = autocomplete.getPlace();
												//document.getElementById('city2').value = place.name;
												document.getElementById('latitude').value = place.geometry.location.lat();
												document.getElementById('longitude').value = place.geometry.location.lng(); 
												
												var location = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
												codeLatLng(place.geometry.location.lat(), place.geometry.location.lng());
												
												marker.setPosition(location);
												map.setZoom(16);
												map.setCenter(location);
												
											});
											
											
																							  

									    
									  });
									  
									  //Add listener to marker for reverse geocoding
									  google.maps.event.addListener(marker, 'drag', function() {
									    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
									      if (status == google.maps.GeocoderStatus.OK) {
									        if (results[0]) {
												
									          jQuery('#address').val(results[0].formatted_address);
									          jQuery('#latitude').val(marker.getPosition().lat());
									          jQuery('#longitude').val(marker.getPosition().lng());
									        }
									      }
									    });
									  });
									  
									});

								});
								// For city country , zip
								function codeLatLng(lat, lng) {
									var city;
									var postcode;
									var state;
									var country;	
									var latlng = new google.maps.LatLng(lat, lng);
									geocoder.geocode({'latLng': latlng}, function(results, status) {
									  if (status == google.maps.GeocoderStatus.OK) {
									  //console.log(results)
										if (results[1]) {
									
										//find country name
											 for (var i=0; i<results[0].address_components.length; i++) {
											for (var b=0;b<results[0].address_components[i].types.length;b++) {
											
												//there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
												if (results[0].address_components[i].types[b] == "locality") {
													//this is the object you are looking for
													city= results[0].address_components[i];		
													//break;
												}
												if (results[0].address_components[i].types[b] == "country") {
													country= results[0].address_components[i];
												}
												if (results[0].address_components[i].types[b] == "postal_code") {													
													postcode= results[0].address_components[i];													
												}	
												
											}
										}
										//city data
										jQuery('#city').val('');
										jQuery('#postcode').val('');
										
										jQuery('#address').val(results[0].formatted_address); 
										jQuery('#city').val(city.long_name);
										jQuery('#postcode').val(postcode.long_name);
										jQuery('#country').val(country.long_name);
										//alert(city.short_name + " " + city.long_name)
										


										} else {
										  
										}
									  } else {
										
									  }
									});
								  }

						    </script>
							
							</div>																
						</div>
						<div class="clearfix"></div>	
					
					
					<div class="clearfix"></div>	
					
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									  <?php _e('Contact Info','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									  <?php _e('[ Edit ]','ivdirectories'); ?> 
									</a>
								  </h4>
								</div>
								<div id="collapseFour" class="panel-collapse collapse">
								  <div class="panel-body">											
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Phone','ivdirectories'); ?></label>						
											<div class="  "> 
												<input type="text" class="form-control" name="phone" id="phone" value="" placeholder="<?php _e('Enter Phone Number','ivdirectories'); ?>">
											</div>																
									</div>
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Fax','ivdirectories'); ?></label>
											
											<div class="  "> 
												<input type="text" class="form-control" name="fax" id="fax" value="" placeholder="<?php _e('Enter Fax Number','ivdirectories'); ?>">
											</div>																
									</div>	
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Email Address','ivdirectories'); ?></label>
											
											<div class="  "> 
												<input type="text" class="form-control" name="contact-email" id="contact-email" value="" placeholder="<?php _e('Enter Email Address','ivdirectories'); ?>">
											</div>																
									</div>
									<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Web Site','ivdirectories'); ?></label>
											
											<div class="  "> 
												<input type="text" class="form-control" name="contact_web" id="contact_web" value="" placeholder="<?php _e('Enter Web Site','ivdirectories'); ?>">
											</div>																
									</div>
									
									
								  </div>
								</div>
					  </div>
					
					
					
					<div class="clearfix"></div>	
					
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									  <?php _e('Videos','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									   <?php _e('[ Edit ]','ivdirectories'); ?> 
									</a>
								  </h4>
								</div>
								<div id="collapseThree" class="panel-collapse collapse">
								  <div class="panel-body">	
									  <?php
											// video, event , coupon , vip_badge
										 if($this->check_write_access('video')){
											
										?>										
										<div class=" form-group">
											
												<label for="text" class=" control-label"><?php _e('Youtube','ivdirectories'); ?></label>
												
												<div class="  "> 
													<input type="text" class="form-control" name="youtube" id="youtube" value="" placeholder="<?php _e('Enter Youtube video ID, e.g : bU1QPtOZQZU ','ivdirectories'); ?>">
												</div>																
										</div>
										<div class=" form-group">
												<label for="text" class=" control-label"><?php _e('vimeo','ivdirectories'); ?></label>
												
												<div class="  "> 
													<input type="text" class="form-control" name="vimeo" id="vimeo" value="" placeholder="<?php _e('Enter vimeo ID, e.g : 134173961','ivdirectories'); ?>">
												</div>																
										</div>
										<?php
										}else{
												_e('Please upgrade your account to add video ','ivdirectories');
										}
										?>
									
								  </div>
								</div>
					  </div>

					<div class="clearfix"></div>	
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
									  <?php _e('Social Profiles','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
									   <?php _e('[ Edit ]','ivdirectories'); ?> 
									</a>
								  </h4>
								</div>
								<div id="collapseFive" class="panel-collapse collapse">
								  <div class="panel-body">											
										
										<div class="form-group">
										<label class="control-label">FaceBook</label>
										<input type="text" name="facebook" id="facebook" value="" class="form-control"/>
									  </div>
									  <div class="form-group">
										<label class="control-label">Linkedin</label>
										<input type="text" name="linkedin" id="linkedin" value="" class="form-control"/>
									  </div>
									  <div class="form-group">
										<label class="control-label">Twitter</label>
										<input type="text" name="twitter" id="twitter" value="" class="form-control"/>
									  </div>
									  <div class="form-group">
										<label class="control-label">Google+ </label>
										<input type="text" name="gplus" id="gplus" value=""  class="form-control"/>
									  </div>
									   <div class="form-group">
										<label class="control-label">Instagram </label>
										<input type="text" name="instagram" id="instagram" value=""  class="form-control"/>
									  </div>
									  <div class="form-group">
										<label class="control-label">Youtube </label>
										<input type="text" name="youtube_social" id="youtube_social" value=""  class="form-control"/>
									  </div>
									  
									  
						  									
										
								  </div>
								</div>
					  </div>
					
					
					<div class="clearfix"></div>	
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									  <?php _e('Additional Info','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									 <?php _e('[ Edit ]','ivdirectories'); ?> 
									</a>
								  </h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse">
								  <div class="panel-body">											
										<?php							

											$default_fields = array();
												$field_set=get_option('iv_directories_fields' );
											if($field_set!=""){ 
													$default_fields=get_option('iv_directories_fields' );
											}else{															
													$default_fields['business_type']='Business Type';
													$default_fields['main_products']='Main Products';
													$default_fields['number_of_employees']='Number Of Employees';
													$default_fields['main_markets']='Main Markets';
													$default_fields['total_annual_sales_volume']='Total Annual Sales Volume';	
											}
													
										$i=1;							
										foreach ( $default_fields as $field_key => $field_value ) { ?>	
												 <div class="form-group">
													<label class="control-label"><?php echo _e($field_value, 'wp_iv_directories'); ?></label>
													<input type="text" placeholder="<?php echo 'Enter '.$field_value;?>" name="<?php echo $field_key;?>" id="<?php echo $field_key;?>"  class="form-control" value=""/>
												  </div>
										
										<?php
										}
										?>			
										
								  </div>
								</div>
					  </div>
					
					
						 
					<div class="clearfix"></div>	
					<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title col-lg-10">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
									  <?php _e('Event','ivdirectories'); ?>
									</a>
								  </h4>
									<h4 class="panel-title" style="text-align:right;color:blue;font-size:12px;">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
									  <?php _e('[ Edit ]','ivdirectories'); ?> 
									</a>
								  </h4>
								</div>
								<div id="collapseSix" class="panel-collapse collapse">
								  <div class="panel-body">											
										<?php
											// video, event , coupon , vip_badge , booking
										 if($this->check_write_access('event')){
											
										?>		
										<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Event Title','ivdirectories'); ?></label>
												<input type="text" class="form-control" name="event-title" id="event-title" value="" placeholder="<?php _e('Enter Title Here','ivdirectories'); ?>">							
										</div>	
										<div class=" form-group">
											<label for="text" class=" control-label"><?php _e('Event Detail','ivdirectories'); ?></label>
												<?php
													$settings_a = array(															
														'textarea_rows' =>10,	
														'editor_class' => 'form-control'															 
														);
													$content_client ='';//$content;//get_option( 'iv_directories_signup_email');
													$editor_id = 'event-detail';
													//wp_editor( $content_client, $editor_id,$settings_a );	
																						
													?>
													
												<textarea id="event-detail" name="event-detail"  rows="4" class="form-control" >  </textarea>
												
												
										</div>
										<div class=" row form-group " style="margin-top:10px">
											<label for="text" class=" col-md-5 control-label"><?php _e('Event Image','ivdirectories'); ?>  </label>
										
											<div class="col-md-4" id="event_image_div">
												<a  href="javascript:void(0);" onclick="event_post_image('event_image_div');"  >									
												<?php  echo '<img width="100px" src="'. wp_iv_directories_URLPATH.'assets/images/image-add-icon.png">'; ?>			
												</a>					
											</div>
											
											<input type="hidden" name="event_image_id" id="event_image_id" value="">
											
											<div class="col-md-3" id="event_image_edit">	
												<button type="button" onclick="event_post_image('event_image_div');"  class="btn btn-xs green-haze"><?php _e('Add','ivdirectories'); ?></button>
												
											</div>									
										</div>	
										<?php
										}else{
												_e('Please upgrade your account to add event ','ivdirectories');
										}
										?>
								  </div>
								
								</div>
					  </div>
					<div class="clearfix"></div>	
					
						
								</div>
					  </div>
						
						
						
								<div class="margiv-top-10">
								    <div class="" id="update_message"></div>
									<input type="hidden" name="user_post_id" id="user_post_id" value="">
								    <button type="button" onclick="iv_save_post();"  class="btn green-haze"><?php _e('Save Post','ivdirectories'); ?></button>
		                          
		                        </div>	
									 
							</form>
						  </div>
						</div>
			<?php
			
				} // for Role
			
		
				
			?>
					
			

                      
					 </div>
                     
                  </div>
                </div>
              </div>
            </div>
          <!-- END PROFILE CONTENT -->

          
	 <script>
				 
		 
		 
function iv_save_post (){
	tinyMCE.triggerSave();	
	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	var loader_image = '<img src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
				jQuery('#update_message').html(loader_image);
				var search_params={
					"action"  : 	"iv_directories_save_wp_post",	
					"form_data":	jQuery("#new_post").serialize(), 
				};
				jQuery.ajax({					
					url : ajaxurl,					 
					dataType : "json",
					type : "post",
					data : search_params,
					success : function(response){
						if(response.code=='success'){
								var url = "<?php echo get_permalink(); ?>?&profile=all-post";    						
								jQuery(location).attr('href',url);	
						}
						//jQuery('#update_message').html('<div class="alert alert-info alert-dismissable"><a class="panel-close close" data-dismiss="alert">x</a>'+response.msg +'.</div>');
						
					}
				});
	
	}
function add_day_field(){
	var main_opening_div =jQuery('#day-row1').html(); 
	jQuery('#day_field_div').append('<div class="clearfix"></div><div class=" row form-group" >'+main_opening_div+'</div>');

}
function  remove_post_image	(profile_image_id){
	jQuery('#'+profile_image_id).html('');
	jQuery('#feature_image_id').val(''); 
	jQuery('#post_image_edit').html('<button type="button" onclick="edit_post_image(\'post_image_div\');"  class="btn btn-xs green-haze">Add</button>');  

}
function  remove_event_image	(profile_image_id){
	jQuery('#'+profile_image_id).html('');
	jQuery('#event_image_id').val(''); 
	jQuery('#event_image_edit').html('<button type="button" onclick="event_post_image(\'event_image_div\');"  class="btn btn-xs green-haze">Add</button>');  

}
function  remove_deal_image	(profile_image_id){
	jQuery('#'+profile_image_id).html('');
	jQuery('#deal_image_id').val(''); 
	jQuery('#deal_image_edit').html('<button type="button" onclick="deal_post_image(\'deal_image_div\');"  class="btn btn-xs green-haze">Add</button>');  

}	
 function edit_post_image(profile_image_id){	
				var image_gallery_frame;

               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: "<?php _e( 'Set Feature Image ', 'ivdirectories' ); ?>",
                    button: {
                        text: "<?php _e( 'Set Feature Image', 'ivdirectories' ); ?>",
                    },
                    multiple: false,
                    displayUserSettings: true,
                });                
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {
							jQuery('#'+profile_image_id).html('<img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'">');
							jQuery('#feature_image_id').val(attachment.id ); 
							jQuery('#post_image_edit').html('<button type="button" onclick="edit_post_image(\'post_image_div\');"  class="btn btn-xs green-haze">Edit</button> &nbsp;<button type="button" onclick="remove_post_image(\'post_image_div\');"  class="btn btn-xs green-haze">Remove</button>');  
						   
						}
					});
                   
                });               
				image_gallery_frame.open(); 
				
	}
function event_post_image(profile_image_id){	
				var image_gallery_frame;

               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: "<?php _e( 'Set Event Image ', 'ivdirectories' ); ?>",
                    button: {
                        text: "<?php _e( 'Set Event Image', 'ivdirectories' ); ?>",
                    },
                    multiple: false,
                    displayUserSettings: true,
                });                
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {
							jQuery('#'+profile_image_id).html('<img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'">');
							jQuery('#event_image_id').val(attachment.id ); 
							jQuery('#event_image_edit').html('<button type="button" onclick="event_post_image(\'event_image_div\');"  class="btn btn-xs green-haze">Edit</button> &nbsp;<button type="button" onclick="remove_event_image(\'event_image_div\');"  class="btn btn-xs green-haze">Remove</button>');  
						   
						}
					});
                   
                });               
				image_gallery_frame.open(); 
				
	}
function deal_post_image(profile_image_id){	
				var image_gallery_frame;

               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: "<?php _e( 'Set Deal/Coupon Image ', 'ivdirectories' ); ?>",
                    button: {
                        text: "<?php _e( 'Set Deal/Coupon Image', 'ivdirectories' ); ?>",
                    },
                    multiple: false,
                    displayUserSettings: true,
                });                
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {
							jQuery('#'+profile_image_id).html('<img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'">');
							jQuery('#deal_image_id').val(attachment.id ); 
							jQuery('#deal_image_edit').html('<button type="button" onclick="deal_post_image(\'deal_image_div\');"  class="btn btn-xs green-haze">Edit</button> &nbsp;<button type="button" onclick="remove_deal_image(\'deal_image_div\');"  class="btn btn-xs green-haze">Remove</button>');  
						   
						}
					});
                   
                });               
				image_gallery_frame.open(); 
				
	}			
 function edit_gallery_image(profile_image_id){
				
				var image_gallery_frame;
				var hidden_field_image_ids = jQuery('#gallery_image_ids').val();
               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: "<?php _e( 'Gallery Images ', 'ivdirectories' ); ?>",
                    button: {
                        text: "<?php _e( 'Gallery Images', 'ivdirectories' ); ?>",
                    },
                    multiple: true,
                    displayUserSettings: true,
                });                
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        console.log(attachment);
                        if ( attachment.id ) {
							jQuery('#'+profile_image_id).append('<div id="gallery_image_div'+attachment.id+'" class="col-md-3"><img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'"><button type="button" onclick="remove_gallery_image(\'gallery_image_div'+attachment.id+'\', '+attachment.id+');"  class="btn btn-xs btn-danger">Remove</button> </div>');
							
							hidden_field_image_ids=hidden_field_image_ids+','+attachment.id ;
							jQuery('#gallery_image_ids').val(hidden_field_image_ids); 
							
							//jQuery('#gallery_image_edit').html('');  
						   
						}
					});
                   
                });               
				image_gallery_frame.open(); 

 }			

function  remove_gallery_image(img_remove_div,rid){	
	var hidden_field_image_ids = jQuery('#gallery_image_ids').val();	
	hidden_field_image_ids =hidden_field_image_ids.replace(rid, '');	
	jQuery('#'+img_remove_div).remove();
	jQuery('#gallery_image_ids').val(hidden_field_image_ids); 
	//jQuery('#gallery_gallery_edit').html('');  

}		
function add_award_field(){
	var main_award_div =jQuery('#award').html(); 
	jQuery('#awards').append('<div class="clearfix"></div><hr>'+main_award_div+'');
}
function award_post_image(awardthis){	
				var image_gallery_frame;
               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: "<?php _e( 'Set award Image ', 'ivdirectories' ); ?>",
                    button: {
                        text: "<?php _e( 'Set award Image', 'ivdirectories' ); ?>",
                    },
                    multiple: false,
                    displayUserSettings: true,
                });                
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {		
													
							jQuery(awardthis).html('<img  class="img-responsive"  src="'+attachment.sizes.thumbnail.url+'"><input type="hidden" name="award_image_id[]" id="award_image_id[]" value="'+attachment.id+'">');
							
							
						}
					});                   
                });               
				image_gallery_frame.open(); 				
	}		
 </script>	  
<!-- for divi theme
<script>
jQuery( document ).ready(function() {
   jQuery('.collapse').collapse()
});	
</script>	
style="min-height: 400px;"

  -->              
