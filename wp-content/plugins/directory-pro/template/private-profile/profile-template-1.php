<?php
wp_enqueue_style('wp-iv_directories-myaccount-style-11', wp_iv_directories_URLPATH . 'admin/files/css/iv-bootstrap.css');
wp_enqueue_script('iv_directories-myaccount-style-12', wp_iv_directories_URLPATH . 'admin/files/js/bootstrap.min.js');


wp_enqueue_media(); 
global $current_user;

$directory_url=get_option('_iv_directory_url');					
if($directory_url==""){$directory_url='directories';}  

$user = new WP_User( $current_user->ID );									
if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
	foreach ( $user->roles as $role ){
		$crole= ucfirst($role); 
		break;
	}	
}	
if(strtoupper($crole)!=strtoupper('administrator')){ 
	include(wp_iv_directories_template.'/private-profile/check_status.php');
}



?>
<!--
<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
-->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,600italic,400,700' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <?php
      global $current_user;
      $current_user = wp_get_current_user();
	  //print_r($current_user);
	  $currencies = array();
	$currencies['AUD'] ='$';$currencies['CAD'] ='$';
	$currencies['EUR'] ='€';$currencies['GBP'] ='£';
	$currencies['JPY'] ='¥';$currencies['USD'] ='$';
	$currencies['NZD'] ='$';$currencies['CHF'] ='Fr';
	$currencies['HKD'] ='$';$currencies['SGD'] ='$';
	$currencies['SEK'] ='kr';$currencies['DKK'] ='kr';
	$currencies['PLN'] ='zł';$currencies['NOK'] ='kr';
	$currencies['HUF'] ='Ft';$currencies['CZK'] ='Kč';
	$currencies['ILS'] ='₪';$currencies['MXN'] ='$';
	$currencies['BRL'] ='R$';$currencies['PHP'] ='₱';
	$currencies['MYR'] ='RM';$currencies['AUD'] ='$';
	$currencies['TWD'] ='NT$';$currencies['THB'] ='฿';	
	$currencies['TRY'] ='TRY';	$currencies['CNY'] ='¥';	
	$currency= get_option('_iv_directories_api_currency');
	
	$currency_symbol=(isset($currencies[$currency]) ? $currencies[$currency] :$currency );
      ?>
    <div id="profile-account2" class="bootstrap-wrapper around-separetor">
      <div class="row margin-top-10">
        <div class="col-md-3 col-sm-3 col-xs-12">
          <!-- BEGIN PROFILE SIDEBAR -->

          <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet portlet0 light profile-sidebar-portlet">
              <!-- SIDEBAR USERPIC -->
              <div class="profile-userpic text-center" id="profile_image_main">
				  <?php
				  	$iv_profile_pic_url=get_user_meta($current_user->ID, 'iv_profile_pic_thum',true);
				  	if($iv_profile_pic_url!=''){ ?>
					 <img src="<?php echo $iv_profile_pic_url; ?>">
					<?php
					}else{
					 echo'	 <img src="'. wp_iv_directories_URLPATH.'assets/images/Blank-Profile.jpg">';
					}
				  	?>
                   
              </div>
              <!-- END SIDEBAR USERPIC -->
              <!-- SIDEBAR USER TITLE -->
              <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                   <?php 
				   $name_display=get_user_meta($current_user->ID,'first_name',true).' '.get_user_meta($current_user->ID,'last_name',true);
				   echo (trim($name_display)!=""? $name_display : $current_user->display_name );?>
                </div>
                <div class="profile-usertitle-job">
                   <?php echo get_user_meta($current_user->ID,'occupation',true); ?>
                </div>
				
              </div>
              <!-- END SIDEBAR USER TITLE -->
              <!-- SIDEBAR BUTTONS -->
              <div class="profile-userbuttons">
                <button type="button" onclick="edit_profile_image('profile_image_main');"  class="btn"><?php _e('Change Image','ivdirectories'); ?> </button>
              </div>
              <!-- END SIDEBAR BUTTONS -->
              <!-- SIDEBAR MENU -->
              <div class="profile-usermenu">
			  <?php
			  $active='all-post';
			  
			  if(isset($_GET['profile']) AND $_GET['profile']=='setting' ){
				 $active='setting';
			  }
			  if(isset($_GET['profile']) AND $_GET['profile']=='level' ){
				 $active='level';
			  }
			  if(isset($_GET['profile']) AND $_GET['profile']=='all-post' ){
				 $active='all-post';
			  }
			  if(isset($_GET['profile']) AND $_GET['profile']=='new-post' ){
				 $active='new-post';
			  }
			  if(isset($_GET['profile']) AND $_GET['profile']=='new-post' ){
				 $active='new-post';
			  }
			  if(isset($_GET['profile']) AND $_GET['profile']=='bidding' ){
				 $active='bidding';
			  }
			  if(isset($_GET['profile']) AND $_GET['profile']=='favorites' ){
				 $active='favorites';
			  }
			  if(isset($_GET['profile']) AND $_GET['profile']=='who-is-interested' ){
				 $active='who-is-interested';
			  }
			  if(isset($_GET['profile']) AND $_GET['profile']=='balance' ){
				 $active='balance';
			  }
			   if(isset($_GET['profile']) AND $_GET['profile']=='post-edit' ){
				$active='all-post';
			  }
			  
			  
			  

				
				$post_type=  'directories';
						
			  ?>
                <ul class="nav">
					<?php
					// $account_menu_check= '';
					// if( get_option( '_iv_directories_menu_listinghome' ) ) {
					// 	 $account_menu_check= get_option('_iv_directories_menu_listinghome');
					// }
					// if($account_menu_check!='yes'){					
					?>
					<!-- <li class="">
                    <a href="<?php //echo get_post_type_archive_link( $directory_url) ; ?>">
                    <i class="fa fa-cog"></i> -->
                    <?php //_e('Listing Home','ivdirectories');	 ?> <!-- </a> -->
					<!-- </li> -->
					<?php
						//}
					?>

					<?php
					$account_menu_check= '';
					if( get_option( '_iv_directories_menusetting' ) ) {
						 $account_menu_check= get_option('_iv_directories_menusetting');
					}
					if($account_menu_check!='yes'){					
					?>
                  <li class="<?php echo ($active=='setting'? 'active':''); ?> ">
                    <a href="<?php echo get_permalink(); ?>?&profile=setting">
                    <i class="fa fa-cog"></i>
                    <?php _e('Profile','ivdirectories');?> </a>
                  </li>
				  <?php
					}
				  ?>

				  <?php
					$account_menu_check= '';
					if( get_option( '_iv_directories_menunewlisting' ) ) {
						 $account_menu_check= get_option('_iv_directories_menunewlisting');
					}
					if($account_menu_check!='yes'){					
					?>
				    <li class="<?php echo ($active=='new-post'? 'active':''); ?> ">
                    <a href="<?php echo get_permalink(); ?>?&profile=new-post">
                    <i class="fa fa-cog"></i>
                    <?php  _e('Add Business','ivdirectories');?> </a>
                  </li>
				  <?php
					}
				  ?>


				  <?php
					$account_menu_check= '';
					if( get_option( '_iv_directories_menuallpost' ) ) {
						 $account_menu_check= get_option('_iv_directories_menuallpost');
					}
					if($account_menu_check!='yes'){					
					?>
				  <li class="<?php echo ($active=='all-post'? 'active':''); ?> ">
                    <a href="<?php echo get_permalink(); ?>?&profile=all-post">
                    <i class="fa fa-cog"></i>
                    <?php _e('Business Listing','ivdirectories');?>  </a>
                  </li>
				  <?php
					}
				  ?>


				  <?php
					$account_menu_check= '';
					if( get_option( '_iv_directories_menufavorites' ) ) {
						 $account_menu_check= get_option('_iv_directories_menufavorites');
					}
					if($account_menu_check!='yes'){					
					?>
					  <li class="<?php echo ($active=='favorites'? 'active':''); ?> ">
                    <a href="<?php echo get_permalink(); ?>?&profile=favorites">
                    <i class="fa fa-cog"></i>
                    <?php  _e('My Favorites','ivdirectories');?> </a>
                  </li>
				  <?php
					}
				  ?>

                  <?php
					$account_menu_check= '';
					if( get_option( '_iv_directories_mylevel' ) ) {
						 $account_menu_check= get_option('_iv_directories_mylevel');
					}
					if($account_menu_check!='yes'){					
					?>
					  <li class="<?php echo ($active=='level'? 'active':''); ?> ">
						<a href="<?php echo get_permalink(); ?>?&profile=level">
						<i class="fa fa-cog"></i>
						<?php _e('Subscription','ivdirectories');	 ?> </a>
					  </li>
					 <?php
					}
					?>
					
				  <?php
					// $account_menu_check= '';
					// if( get_option( '_iv_directories_menuinterested' ) ) {
					// 	 $account_menu_check= get_option('_iv_directories_menuinterested');
					// }
					// if($account_menu_check!='yes'){					
					?>
				    <!-- <li class="<?php //echo ($active=='who-is-interested'? 'active':''); ?> ">
                    <a href="<?php //echo get_permalink(); ?>?&profile=who-is-interested">
                    <i class="fa fa-cog"></i> -->
                    <?php  //_e('Who is Interested','ivdirectories');?> <!-- </a> -->
                  <!-- </li> -->
				  <?php
					//}
				  ?>

			  <?php     
			  		$old_custom_menu = array();
					if(get_option('iv_directories_profile_menu')){
						$old_custom_menu=get_option('iv_directories_profile_menu' );
					}
					$ii=1;		
					if($old_custom_menu!=''){
						foreach ( $old_custom_menu as $field_key => $field_value ) { ?>
							
							  <li class="<?php echo ($active=='new-post'? 'active':''); ?> ">
									<a href="<?php echo $field_value; ?>">
										<i class="fa fa-cog"></i>
									 <?php echo $field_key;?> </a>
							  </li>
						
						<?php
						}
					}    
			  ?>                  
					<li class="<?php echo ($active=='log-out'? 'active':''); ?> ">
						<a href="<?php echo wp_logout_url( home_url() ); ?>" >
						<i class="fa fa-sign-out"></i>
						  <?php _e('Sign out','ivdirectories');?> 
						 </a>
					 </li>

					 

                </ul>
              </div>
              <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            
            <!-- END PORTLET MAIN -->
          </div>
          </div>
          <!-- END BEGIN PROFILE SIDEBAR -->
          <!-- BEGIN PROFILE CONTENT -->
		  <?php ?>
		  
          <div class="col-md-9 col-sm-9 col-xs-12">
          <div class="listng-acc">
		  <?php
		  if(isset($_GET['profile']) AND $_GET['profile']=='all-post' ){
			include(  wp_iv_directories_template. 'private-profile/profile-all-post-1.php');
		  } elseif(isset($_GET['profile']) AND $_GET['profile']=='bidding' ){
			include( wp_iv_directories_template. 'private-profile/bidding-1.php');
		  } elseif(isset($_GET['profile']) AND $_GET['profile']=='new-post' ){
			include( wp_iv_directories_template. 'private-profile/profile-new-post-1.php');		
		  }elseif(isset($_GET['profile']) AND $_GET['profile']=='level' ){
			include(  wp_iv_directories_template. 'private-profile/profile-level-1.php');
		  }elseif(isset($_GET['profile']) AND $_GET['profile']=='post-edit' ){ 		    
			include(  wp_iv_directories_template. 'private-profile/profile-edit-post-1.php');
		  }elseif(isset($_GET['profile']) AND $_GET['profile']=='favorites' ){ 		    
			include(  wp_iv_directories_template. 'private-profile/my-favorites-1.php');
		  }elseif(isset($_GET['profile']) AND $_GET['profile']=='who-is-interested' ){ 		    
			include(  wp_iv_directories_template. 'private-profile/interested-1.php');
		  }elseif(isset($_GET['profile']) AND $_GET['profile']=='balance' ){ 		    
			include(  wp_iv_directories_template. 'private-profile/balance.php');
		  }elseif(isset($_GET['profile']) AND $_GET['profile']=='setting' ){ 		    
			include(  wp_iv_directories_template. 'private-profile/profile-setting-1.php');
		  }
		  else{ 
			include(  wp_iv_directories_template. 'private-profile/profile-all-post-1.php');
		  }
		  ?>
          </div>
        </div>
      </div>
    </div>




 <script>

			  
			  function edit_profile_image(profile_image_id){	
				var image_gallery_frame;

               // event.preventDefault();
                image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: '<?php _e( 'Profile Image', 'easy-image-gallery' ); ?>',
                    button: {
                        text: '<?php _e( 'Profile Image', 'easy-image-gallery' ); ?>',
                    },
                    multiple: false,
                    displayUserSettings: true,
                });                
                image_gallery_frame.on( 'select', function() {
                    var selection = image_gallery_frame.state().get('selection');
                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.id ) {
							//console.log(attachment.sizes.thumbnail.url);
							var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
							var search_params = {
								"action": 	"iv_directories_update_profile_pic",
								"attachment_thum": attachment.sizes.thumbnail.url,
								"profile_pic_url_1": attachment.url,
							};
                             jQuery.ajax({
										url: ajaxurl,
										dataType: "json",
										type: "post",
										data: search_params,
										success: function(response) {   
											if(response=='success'){					
												
												jQuery('#'+profile_image_id).html('<img  class="img-circle img-responsive"  src="'+attachment.sizes.thumbnail.url+'">');                              
						

											}
											
										}
									});									
                              
						}
					});
                   
                });               
				image_gallery_frame.open(); 
				
			}
				
	function update_profile_setting (){
	
	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	var loader_image = '<img src="<?php echo wp_iv_directories_URLPATH. "admin/files/images/loader.gif"; ?>" />';
				jQuery('#update_message').html(loader_image); 
				var search_params={
					"action"  : 	"iv_directories_update_profile_setting",	
					"form_data":	jQuery("#profile_setting_form").serialize(), 
				};
				jQuery.ajax({					
					url : ajaxurl,					 
					dataType : "json",
					type : "post",
					data : search_params,
					success : function(response){
						jQuery('#update_message').html('<div class="alert alert-info alert-dismissable"><a class="panel-close close" data-dismiss="alert">x</a>'+response.msg +'.</div>');
						
					}
				});
	
	}		  
			  
			  
		  </script>	  

		
