<div class="profile-content">
	<div class="portlet light">
		<div class="portlet-title tabbable-line clearfix">
			<div class="caption caption-md">
			  	<span class="caption-subject">
			  		<?php											
						_e('Delete Account','ivdirectories')	;	
					?>
				</span>
			</div>
		</div>

		<div class="portlet-body">
			<?php echo do_shortcode('[plugin_delete_me /]');?>
		</div>
	</div>
</div>